

var compareList = [];

function addRemoveFromCompareArray(item,state){
    if(state){
        compareList.push(item);
    }
    else{
        compareList = jQuery.grep(compareList, function(value) {
            return value != item;
        });
    }
    return false;
}



/**
 * Created by Davor on 30.6.2016..
 */
jQuery(document).ready(function() {

    /**
     * Get counts
     */
    //countTicketsUnreadByUser();

    jQuery('[data-action="search-form"]').bind('click',function(e){
        e.preventDefault();
        window.location.href = jQuery(this).parent().attr('action')+"?q=" + escape( jQuery(".search-query").val());
        return false;
    });

    /**
     * TASKS LIST
     */
    function getTasksList(){

        jQuery.post(jQuery('[data-action="tasks-wrapper"]').data('url'), function(result) {
            if(result.error == true){
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
            else if(result.error == false){

                if ( $.fn.DataTable.fnIsDataTable( jQuery('#tasks_table') ) ) {
                    jQuery('#tasks_table').DataTable().destroy();
                }

                jQuery('#tasks_table').html(result.html);
                jQuery('#tasks_table').find('[data-toggle="tooltip"]').tooltip();

                /**
                 * search, export
                 */
                createDatatable(jQuery('#tasks_table'), 50, true, false, true, false, 0, 'desc', false, false);
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: translations.there_has_been_an_error_please_try_again
                });
            }
        }, 'json');

        return true;
    }

    if(jQuery('#page-tasks-list').length > 0) {
        getTasksList();

        /**
         * Add task
         */
        jQuery('[data-action="add-task"]').bind('click', function(){
            jQuery.post(jQuery(this).data("url"), { }, function(result) {
                if(result.error == false){
                    jQuery('#modal-container').html(result.html);
                    var modal = jQuery('#modal-container').find('.modal');
                    modal.modal('show');

                    var form = modal.find('[data-action="tasks-form"]');

                    form.find('textarea.autosize').autosize({append: "\n"});
                    form.find('[name="date_due"]').datepicker({
                        dateFormat: 'dd/mm/yy',
                        defaultDate: "+1d",
                        changeMonth: true,
                        changeYear: false,
                        minDate: "0d",
                        numberOfMonths: 1
                    });

                    /**
                     * Validate form
                     */
                    form.on('init.field.fv', function(e, data) {
                        // data.fv      --> The BootstrapValidator instance
                        // data.field   --> The field name
                        // data.element --> The field element

                        var $parent    = data.element.parents('.form-group'),
                            $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                            options    = data.fv.getOptions(),                      // Entire options
                            validators = data.fv.getOptions(data.field).validators; // The field validators

                        if (validators.notEmpty && options.icon && options.icon.required) {
                            // The field uses notEmpty validator
                            // Add required icon
                            $icon.addClass(options.icon.required).show();
                        }
                    });

                    form.formValidation({
                        framework: 'bootstrap',
                        excluded: [':disabled'], /*':disabled' , ':hidden' , ':not(:visible)'*/
                        icon: {
                            required: 'glyphicon glyphicon-asterisk',
                            valid: 'glyphicon glyphicon-ok',
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        live: 'enabled',
                        message: translations.please_fill_in_the_field_correctly,
                        button: {
                            selector: 'button[type="submit"]'
                        },
                        trigger: null
                    }).on('status.field.fv', function(e, data) {
                        // Remove the required icon when the field updates its status
                        var $parent    = data.element.parents('.form-group'),
                            $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                            options    = data.fv.getOptions(),                      // Entire options
                            validators = data.fv.getOptions(data.field).validators; // The field validators

                        if (validators.notEmpty && options.icon && options.icon.required) {
                            $icon.removeClass(options.icon.required).addClass('glyphicon');
                        }
                    }).on('success.field.fv', function(e, data) {
                        // e, data parameters are the same as in err.field.fv event handler
                        // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
                        // - The submit button is clicked
                        // - The form is invalid
                        data.fv.disableSubmitButtons(false);
                    }).on('success.form.fv', function(e, data) {
                        // Prevent form submission
                        e.preventDefault();

                        // Get the form instance
                        var $form = jQuery(e.target);

                        // Get the BootstrapValidator instance
                        $form.formValidation('disableSubmitButtons', true);
                        var fv = $form.data('formValidation');

                        jQuery.post($form.attr('action'), $form.serialize(), function(result) {
                            if(result.error == true){
                                jQuery.growl.error({
                                    title: translations.error_message,
                                    message: result.message
                                });
                            }
                            else if(result.error == false){
                                jQuery.growl.notice({
                                    title: result.title,
                                    message: result.message
                                });
                                jQuery('#modal-container').find('.modal').modal('hide');

                                getTasksList();
                            }
                            else{
                                jQuery.growl.error({
                                    title: translations.error_message,
                                    message: translations.there_has_been_an_error_please_try_again
                                });
                            }
                        }, 'json');
                    });
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
        });
    }

    /**
     * Bind popover
     */
    jQuery('#tasks_table').on('mouseenter','[data-action="popover"]',function() {
        jQuery(this).popover({
            html: true,
            trigger: 'hover',
            content: function() {
                return jQuery(this).data('html');
            },
            title: function() {
                return "";
            },
            placement: "top"
        });
        jQuery(this).popover('show');
    });

    /**
     * Edit task
     */
    jQuery('#tasks_table').on('click', '[data-action="edit-task"]', function(){
        jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id') }, function(result) {
            if(result.error == false){
                jQuery('#modal-container').html(result.html);
                var modal = jQuery('#modal-container').find('.modal');
                modal.modal('show');

                var form = modal.find('[data-action="tasks-form"]');

                form.find('textarea.autosize').autosize({append: "\n"});
                form.find('[name="date_due"]').datepicker({
                    dateFormat: 'dd/mm/yy',
                    defaultDate: "+1d",
                    changeMonth: true,
                    changeYear: false,
                    minDate: "0d",
                    numberOfMonths: 1
                });

                /**
                 * Validate form
                 */
                form.on('init.field.fv', function(e, data) {
                    // data.fv      --> The BootstrapValidator instance
                    // data.field   --> The field name
                    // data.element --> The field element

                    var $parent    = data.element.parents('.form-group'),
                        $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                        options    = data.fv.getOptions(),                      // Entire options
                        validators = data.fv.getOptions(data.field).validators; // The field validators

                    if (validators.notEmpty && options.icon && options.icon.required) {
                        // The field uses notEmpty validator
                        // Add required icon
                        $icon.addClass(options.icon.required).show();
                    }
                });

                form.formValidation({
                    framework: 'bootstrap',
                    excluded: [':disabled'], /*':disabled' , ':hidden' , ':not(:visible)'*/
                    icon: {
                        required: 'glyphicon glyphicon-asterisk',
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    live: 'enabled',
                    message: translations.please_fill_in_the_field_correctly,
                    button: {
                        selector: 'button[type="submit"]'
                    },
                    trigger: null
                }).on('status.field.fv', function(e, data) {
                    // Remove the required icon when the field updates its status
                    var $parent    = data.element.parents('.form-group'),
                        $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                        options    = data.fv.getOptions(),                      // Entire options
                        validators = data.fv.getOptions(data.field).validators; // The field validators

                    if (validators.notEmpty && options.icon && options.icon.required) {
                        $icon.removeClass(options.icon.required).addClass('glyphicon');
                    }
                }).on('success.field.fv', function(e, data) {
                    // e, data parameters are the same as in err.field.fv event handler
                    // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
                    // - The submit button is clicked
                    // - The form is invalid
                    data.fv.disableSubmitButtons(false);
                }).on('success.form.fv', function(e, data) {
                    // Prevent form submission
                    e.preventDefault();

                    // Get the form instance
                    var $form = jQuery(e.target);

                    // Get the BootstrapValidator instance
                    $form.formValidation('disableSubmitButtons', true);
                    var fv = $form.data('formValidation');

                    jQuery.post($form.attr('action'), $form.serialize(), function(result) {
                        if(result.error == true){
                            jQuery.growl.error({
                                title: translations.error_message,
                                message: result.message
                            });
                        }
                        else if(result.error == false){
                            jQuery.growl.notice({
                                title: result.title,
                                message: result.message
                            });
                            jQuery('#modal-container').find('.modal').modal('hide');

                            if(jQuery('#tasks_table').data('function') == 'getDashboardTasks'){
                                getDashboardTasks();
                            }
                            else{
                                getTasksList();
                            }
                        }
                        else{
                            jQuery.growl.error({
                                title: translations.error_message,
                                message: translations.there_has_been_an_error_please_try_again
                            });
                        }
                    }, 'json');
                });
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");
    });

    /**
     * Remove task
     */
    jQuery('#tasks_table').on('click', '[data-action="remove-task"]', function(){
        jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id') }, function(result) {
            if(result.error == false){
                jQuery.growl.notice({
                    title: result.title,
                    message: result.message
                });

                if(jQuery('#tasks_table').data('function') == 'getDashboardTasks'){
                    getDashboardTasks();
                }
                else{
                    getTasksList();
                }
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");
    });

    /**
     * Set task completed
     */
    jQuery('#tasks_table').on('click', '[data-action="set-task-completed"]', function(){
        jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id'), task_state: 3 }, function(result) {
            if(result.error == false){
                jQuery.growl.notice({
                    title: result.title,
                    message: result.message
                });

                if(jQuery('#tasks_table').data('function') == 'getDashboardTasks'){
                    getDashboardTasks();
                }
                else{
                    getTasksList();
                }
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");
    });

    /**
     * TICKETS LIST
     */
    function getTicketsList(){

        jQuery.post(jQuery('[data-action="tickets-wrapper"]').data('url'), function(result) {
            if(result.error == true){
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
            else if(result.error == false){

                if ( $.fn.DataTable.fnIsDataTable( jQuery('#tickets_table') ) ) {
                    jQuery('#tickets_table').DataTable().destroy();
                }

                jQuery('#tickets_table').html(result.html);
                jQuery('#tickets_table').find('[data-toggle="tooltip"]').tooltip();

                /**
                 * search, export
                 */
                createDatatable(jQuery('#tickets_table'), 50, true, false, true, false, 2, 'desc', false, false);
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: translations.there_has_been_an_error_please_try_again
                });
            }
        }, 'json');

        return true;
    }

    if(jQuery('#page-tickets-list').length > 0) {

        getTicketsList();

        /**
         * Create ticket
         */
        jQuery('[data-action="add-ticket"]').bind('click', function(){
            jQuery.post(jQuery(this).data("url"), { }, function(result) {
                if(result.error == false){
                    jQuery('#modal-container').html(result.html);
                    var modal = jQuery('#modal-container').find('.modal');
                    modal.modal('show');

                    var form = modal.find('[data-action="ticket-form"]');

                    form.find('textarea.autosize').autosize({append: "\n"});
                    form.find('[data-action="multi-select2"]').multiSelect();

                    /**
                     * Validate form
                     */
                    form.on('init.field.fv', function(e, data) {
                        // data.fv      --> The BootstrapValidator instance
                        // data.field   --> The field name
                        // data.element --> The field element

                        var $parent    = data.element.parents('.form-group'),
                            $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                            options    = data.fv.getOptions(),                      // Entire options
                            validators = data.fv.getOptions(data.field).validators; // The field validators

                        if (validators.notEmpty && options.icon && options.icon.required) {
                            // The field uses notEmpty validator
                            // Add required icon
                            $icon.addClass(options.icon.required).show();
                        }
                    });

                    form.formValidation({
                        framework: 'bootstrap',
                        excluded: [':disabled'], /*':disabled' , ':hidden' , ':not(:visible)'*/
                        icon: {
                            required: 'glyphicon glyphicon-asterisk',
                            valid: 'glyphicon glyphicon-ok',
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        live: 'enabled',
                        message: translations.please_fill_in_the_field_correctly,
                        button: {
                            selector: 'button[type="submit"]'
                        },
                        trigger: null
                    }).on('status.field.fv', function(e, data) {
                        // Remove the required icon when the field updates its status
                        var $parent    = data.element.parents('.form-group'),
                            $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                            options    = data.fv.getOptions(),                      // Entire options
                            validators = data.fv.getOptions(data.field).validators; // The field validators

                        if (validators.notEmpty && options.icon && options.icon.required) {
                            $icon.removeClass(options.icon.required).addClass('glyphicon');
                        }
                    }).on('success.field.fv', function(e, data) {
                        // e, data parameters are the same as in err.field.fv event handler
                        // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
                        // - The submit button is clicked
                        // - The form is invalid
                        data.fv.disableSubmitButtons(false);
                    }).on('success.form.fv', function(e, data) {
                        // Prevent form submission
                        e.preventDefault();

                        // Get the form instance
                        var $form = jQuery(e.target);

                        // Get the BootstrapValidator instance
                        $form.formValidation('disableSubmitButtons', true);
                        var fv = $form.data('formValidation');

                        var user_ids = serealizeSelects(jQuery('[name="users"]'));

                        jQuery.post($form.attr('action'), $form.serialize()+'&'+jQuery.param({ 'user_ids': user_ids }), function(result) {
                            if(result.error == true){
                                jQuery.growl.error({
                                    title: translations.error_message,
                                    message: result.message
                                });
                            }
                            else if(result.error == false){
                                jQuery.growl.notice({
                                    title: result.title,
                                    message: result.message
                                });
                                jQuery('#modal-container').find('.modal').modal('hide');

                                getTicketsList();
                            }
                            else{
                                jQuery.growl.error({
                                    title: translations.error_message,
                                    message: translations.there_has_been_an_error_please_try_again
                                });
                            }
                        }, 'json');
                    });
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
        });

        /**
         * Edit ticket
         */
        jQuery('#tickets_table').on('click', '[data-action="edit-ticket"]', function(){
            jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id') }, function(result) {
                if(result.error == false){
                    jQuery('#modal-container').html(result.html);
                    var modal = jQuery('#modal-container').find('.modal');
                    modal.modal('show');

                    var form = modal.find('[data-action="ticket-form"]');

                    form.find('textarea.autosize').autosize({append: "\n"});
                    form.find('[data-action="multi-select2"]').multiSelect();

                    /**
                     * Validate form
                     */
                    form.on('init.field.fv', function(e, data) {
                        // data.fv      --> The BootstrapValidator instance
                        // data.field   --> The field name
                        // data.element --> The field element

                        var $parent    = data.element.parents('.form-group'),
                            $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                            options    = data.fv.getOptions(),                      // Entire options
                            validators = data.fv.getOptions(data.field).validators; // The field validators

                        if (validators.notEmpty && options.icon && options.icon.required) {
                            // The field uses notEmpty validator
                            // Add required icon
                            $icon.addClass(options.icon.required).show();
                        }
                    });

                    form.formValidation({
                        framework: 'bootstrap',
                        excluded: [':disabled'], /*':disabled' , ':hidden' , ':not(:visible)'*/
                        icon: {
                            required: 'glyphicon glyphicon-asterisk',
                            valid: 'glyphicon glyphicon-ok',
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        live: 'enabled',
                        message: translations.please_fill_in_the_field_correctly,
                        button: {
                            selector: 'button[type="submit"]'
                        },
                        trigger: null
                    }).on('status.field.fv', function(e, data) {
                        // Remove the required icon when the field updates its status
                        var $parent    = data.element.parents('.form-group'),
                            $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                            options    = data.fv.getOptions(),                      // Entire options
                            validators = data.fv.getOptions(data.field).validators; // The field validators

                        if (validators.notEmpty && options.icon && options.icon.required) {
                            $icon.removeClass(options.icon.required).addClass('glyphicon');
                        }
                    }).on('success.field.fv', function(e, data) {
                        // e, data parameters are the same as in err.field.fv event handler
                        // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
                        // - The submit button is clicked
                        // - The form is invalid
                        data.fv.disableSubmitButtons(false);
                    }).on('success.form.fv', function(e, data) {
                        // Prevent form submission
                        e.preventDefault();

                        // Get the form instance
                        var $form = jQuery(e.target);

                        // Get the BootstrapValidator instance
                        $form.formValidation('disableSubmitButtons', true);
                        var fv = $form.data('formValidation');

                        var user_ids = serealizeSelects(jQuery('[name="users"]'));

                        jQuery.post($form.attr('action'), $form.serialize()+'&'+jQuery.param({ 'user_ids': user_ids }), function(result) {
                            if(result.error == true){
                                jQuery.growl.error({
                                    title: translations.error_message,
                                    message: result.message
                                });
                            }
                            else if(result.error == false){
                                jQuery.growl.notice({
                                    title: result.title,
                                    message: result.message
                                });
                                jQuery('#modal-container').find('.modal').modal('hide');

                                getTicketsList();
                            }
                            else{
                                jQuery.growl.error({
                                    title: translations.error_message,
                                    message: translations.there_has_been_an_error_please_try_again
                                });
                            }
                        }, 'json');
                    });
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
        });

        /**
         * Remove ticket
         */
        jQuery('#tickets_table').on('click', '[data-action="remove-ticket"]', function(){
            jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id') }, function(result) {
                if(result.error == false){
                    jQuery.growl.notice({
                        title: result.title,
                        message: result.message
                    });

                    getTicketsList();
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
        });

        /**
         * Mark as read
         */
        jQuery('#tickets_table').on('click', '[data-action="mark-as-read"]', function(){
            jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id'), is_read: jQuery(this).data('is-read') }, function(result) {
                if(result.error == false){
                    jQuery.growl.notice({
                        title: result.title,
                        message: result.message
                    });

                    getTicketsList();
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
        });

        /**
         * Open messages
         */
        jQuery('#tickets_table').on('click', '[data-action="open-messages"]', function(){
            jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id') }, function(result) {

                if(result.error == false){
                    jQuery('#modal-container').html(result.html);
                    var modal = jQuery('#modal-container').find('.modal');
                    modal.modal('show');

                    var form = modal.find('[data-action="add-message-form"]');

                    form.find('textarea.autosize').autosize({append: "\n"});

                    /**
                     * Validate form
                     */
                    form.on('init.field.fv', function(e, data) {
                        // data.fv      --> The BootstrapValidator instance
                        // data.field   --> The field name
                        // data.element --> The field element

                        var $parent    = data.element.parents('.form-group'),
                            $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                            options    = data.fv.getOptions(),                      // Entire options
                            validators = data.fv.getOptions(data.field).validators; // The field validators

                        if (validators.notEmpty && options.icon && options.icon.required) {
                            // The field uses notEmpty validator
                            // Add required icon
                            $icon.addClass(options.icon.required).show();
                        }
                    });

                    form.formValidation({
                        framework: 'bootstrap',
                        excluded: [':disabled'], /*':disabled' , ':hidden' , ':not(:visible)'*/
                        icon: {
                            required: 'glyphicon glyphicon-asterisk',
                            valid: 'glyphicon glyphicon-ok',
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        live: 'enabled',
                        message: translations.please_fill_in_the_field_correctly,
                        button: {
                            selector: 'button[type="submit"]'
                        },
                        trigger: null
                    }).on('status.field.fv', function(e, data) {
                        // Remove the required icon when the field updates its status
                        var $parent    = data.element.parents('.form-group'),
                            $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                            options    = data.fv.getOptions(),                      // Entire options
                            validators = data.fv.getOptions(data.field).validators; // The field validators

                        if (validators.notEmpty && options.icon && options.icon.required) {
                            $icon.removeClass(options.icon.required).addClass('glyphicon');
                        }
                    }).on('success.field.fv', function(e, data) {
                        // e, data parameters are the same as in err.field.fv event handler
                        // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
                        // - The submit button is clicked
                        // - The form is invalid
                        data.fv.disableSubmitButtons(false);
                    }).on('success.form.fv', function(e, data) {
                        // Prevent form submission
                        e.preventDefault();

                        // Get the form instance
                        var $form = jQuery(e.target);

                        // Get the BootstrapValidator instance
                        $form.formValidation('disableSubmitButtons', true);
                        var fv = $form.data('formValidation');

                        jQuery.post($form.attr('action'), $form.serialize(), function(result) {
                            if(result.error == true){
                                jQuery.growl.error({
                                    title: translations.error_message,
                                    message: result.message
                                });
                            }
                            else if(result.error == false){
                                jQuery.growl.notice({
                                    title: result.title,
                                    message: result.message
                                });
                                jQuery('#modal-container').find('.modal').modal('hide');

                                getTicketsList();
                            }
                            else{
                                jQuery.growl.error({
                                    title: translations.error_message,
                                    message: translations.there_has_been_an_error_please_try_again
                                });
                            }
                        }, 'json');
                    });
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
        });
    }

    /**
     * MESSAGES LIST
     */
    function getMessagesList(){

        jQuery.post(jQuery('[data-action="messages-wrapper"]').data('url'), function(result) {
            if(result.error == true){
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
            else if(result.error == false){

                if ( $.fn.DataTable.fnIsDataTable( jQuery('#messages_table') ) ) {
                    jQuery('#messages_table').DataTable().destroy();
                }

                jQuery('#messages_table').html(result.html);
                jQuery('#messages_table').find('[data-toggle="tooltip"]').tooltip();

                /**
                 * search, export
                 */
                createDatatable(jQuery('#messages_table'), 50, true, false, false, false, 2, 'desc', false, false);
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: translations.there_has_been_an_error_please_try_again
                });
            }
        }, 'json');

        return true;
    }

    if(jQuery('#page-messages-list').length > 0) {

        getMessagesList();

        /**
         * Create conversation
         */
        jQuery('[data-action="add-conversation"]').bind('click', function(){
            jQuery.post(jQuery(this).data("url"), { }, function(result) {
                if(result.error == false){
                    jQuery('#modal-container').html(result.html);
                    var modal = jQuery('#modal-container').find('.modal');
                    modal.modal('show');

                    var form = modal.find('[data-action="conversation-form"]');

                    form.find('textarea.autosize').autosize({append: "\n"});
                    form.find('[data-action="multi-select2"]').multiSelect();

                    /**
                     * Validate form
                     */
                    form.on('init.field.fv', function(e, data) {
                        // data.fv      --> The BootstrapValidator instance
                        // data.field   --> The field name
                        // data.element --> The field element

                        var $parent    = data.element.parents('.form-group'),
                            $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                            options    = data.fv.getOptions(),                      // Entire options
                            validators = data.fv.getOptions(data.field).validators; // The field validators

                        if (validators.notEmpty && options.icon && options.icon.required) {
                            // The field uses notEmpty validator
                            // Add required icon
                            $icon.addClass(options.icon.required).show();
                        }
                    });

                    form.formValidation({
                        framework: 'bootstrap',
                        excluded: [':disabled'], /*':disabled' , ':hidden' , ':not(:visible)'*/
                        icon: {
                            required: 'glyphicon glyphicon-asterisk',
                            valid: 'glyphicon glyphicon-ok',
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        live: 'enabled',
                        message: translations.please_fill_in_the_field_correctly,
                        button: {
                            selector: 'button[type="submit"]'
                        },
                        trigger: null
                    }).on('status.field.fv', function(e, data) {
                        // Remove the required icon when the field updates its status
                        var $parent    = data.element.parents('.form-group'),
                            $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                            options    = data.fv.getOptions(),                      // Entire options
                            validators = data.fv.getOptions(data.field).validators; // The field validators

                        if (validators.notEmpty && options.icon && options.icon.required) {
                            $icon.removeClass(options.icon.required).addClass('glyphicon');
                        }
                    }).on('success.field.fv', function(e, data) {
                        // e, data parameters are the same as in err.field.fv event handler
                        // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
                        // - The submit button is clicked
                        // - The form is invalid
                        data.fv.disableSubmitButtons(false);
                    }).on('success.form.fv', function(e, data) {
                        // Prevent form submission
                        e.preventDefault();

                        // Get the form instance
                        var $form = jQuery(e.target);

                        // Get the BootstrapValidator instance
                        $form.formValidation('disableSubmitButtons', true);
                        var fv = $form.data('formValidation');

                        var user_ids = serealizeSelects(jQuery('[name="users"]'));

                        jQuery.post($form.attr('action'), $form.serialize()+'&'+jQuery.param({ 'user_ids': user_ids }), function(result) {
                            if(result.error == true){
                                jQuery.growl.error({
                                    title: translations.error_message,
                                    message: result.message
                                });
                            }
                            else if(result.error == false){
                                jQuery.growl.notice({
                                    title: result.title,
                                    message: result.message
                                });
                                jQuery('#modal-container').find('.modal').modal('hide');

                                getMessagesList();
                            }
                            else{
                                jQuery.growl.error({
                                    title: translations.error_message,
                                    message: translations.there_has_been_an_error_please_try_again
                                });
                            }
                        }, 'json');
                    });
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
        });

        /**
         * Edit conversation
         */
        jQuery('#messages_table').on('click', '[data-action="edit-conversation"]', function(){
            jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id') }, function(result) {
                if(result.error == false){
                    jQuery('#modal-container').html(result.html);
                    var modal = jQuery('#modal-container').find('.modal');
                    modal.modal('show');

                    var form = modal.find('[data-action="conversation-form"]');

                    form.find('textarea.autosize').autosize({append: "\n"});
                    form.find('[data-action="multi-select2"]').multiSelect();

                    /**
                     * Validate form
                     */
                    form.on('init.field.fv', function(e, data) {
                        // data.fv      --> The BootstrapValidator instance
                        // data.field   --> The field name
                        // data.element --> The field element

                        var $parent    = data.element.parents('.form-group'),
                            $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                            options    = data.fv.getOptions(),                      // Entire options
                            validators = data.fv.getOptions(data.field).validators; // The field validators

                        if (validators.notEmpty && options.icon && options.icon.required) {
                            // The field uses notEmpty validator
                            // Add required icon
                            $icon.addClass(options.icon.required).show();
                        }
                    });

                    form.formValidation({
                        framework: 'bootstrap',
                        excluded: [':disabled'], /*':disabled' , ':hidden' , ':not(:visible)'*/
                        icon: {
                            required: 'glyphicon glyphicon-asterisk',
                            valid: 'glyphicon glyphicon-ok',
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        live: 'enabled',
                        message: translations.please_fill_in_the_field_correctly,
                        button: {
                            selector: 'button[type="submit"]'
                        },
                        trigger: null
                    }).on('status.field.fv', function(e, data) {
                        // Remove the required icon when the field updates its status
                        var $parent    = data.element.parents('.form-group'),
                            $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                            options    = data.fv.getOptions(),                      // Entire options
                            validators = data.fv.getOptions(data.field).validators; // The field validators

                        if (validators.notEmpty && options.icon && options.icon.required) {
                            $icon.removeClass(options.icon.required).addClass('glyphicon');
                        }
                    }).on('success.field.fv', function(e, data) {
                        // e, data parameters are the same as in err.field.fv event handler
                        // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
                        // - The submit button is clicked
                        // - The form is invalid
                        data.fv.disableSubmitButtons(false);
                    }).on('success.form.fv', function(e, data) {
                        // Prevent form submission
                        e.preventDefault();

                        // Get the form instance
                        var $form = jQuery(e.target);

                        // Get the BootstrapValidator instance
                        $form.formValidation('disableSubmitButtons', true);
                        var fv = $form.data('formValidation');

                        var user_ids = serealizeSelects(jQuery('[name="users"]'));

                        jQuery.post($form.attr('action'), $form.serialize()+'&'+jQuery.param({ 'user_ids': user_ids }), function(result) {
                            if(result.error == true){
                                jQuery.growl.error({
                                    title: translations.error_message,
                                    message: result.message
                                });
                            }
                            else if(result.error == false){
                                jQuery.growl.notice({
                                    title: result.title,
                                    message: result.message
                                });
                                jQuery('#modal-container').find('.modal').modal('hide');

                                getMessagesList();
                            }
                            else{
                                jQuery.growl.error({
                                    title: translations.error_message,
                                    message: translations.there_has_been_an_error_please_try_again
                                });
                            }
                        }, 'json');
                    });
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
        });

        /**
         * Remove conversation
         */
        jQuery('#messages_table').on('click', '[data-action="remove-conversation"]', function(){
            jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id') }, function(result) {
                if(result.error == false){
                    jQuery.growl.notice({
                        title: result.title,
                        message: result.message
                    });

                    getMessagesList();
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
        });

        /**
         * Mark as read
         */
        jQuery('[data-action="mark-as-read"]').bind('click', function(){
            jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id'), is_read: jQuery(this).data('is-read') }, function(result) {
                if(result.error == false){
                    jQuery.growl.notice({
                        title: result.title,
                        message: result.message
                    });

                    getMessagesList();
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
        });

        /**
         * Open messages
         */
        jQuery('#messages_table').on('click', '[data-action="open-messages"]', function(){
            jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id') }, function(result) {

                if(result.error == false){
                    jQuery('#modal-container').html(result.html);
                    var modal = jQuery('#modal-container').find('.modal');
                    modal.modal('show');

                    var form = modal.find('[data-action="add-message-form"]');

                    form.find('textarea.autosize').autosize({append: "\n"});

                    /**
                     * Validate form
                     */
                    form.on('init.field.fv', function(e, data) {
                        // data.fv      --> The BootstrapValidator instance
                        // data.field   --> The field name
                        // data.element --> The field element

                        var $parent    = data.element.parents('.form-group'),
                            $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                            options    = data.fv.getOptions(),                      // Entire options
                            validators = data.fv.getOptions(data.field).validators; // The field validators

                        if (validators.notEmpty && options.icon && options.icon.required) {
                            // The field uses notEmpty validator
                            // Add required icon
                            $icon.addClass(options.icon.required).show();
                        }
                    });

                    form.formValidation({
                        framework: 'bootstrap',
                        excluded: [':disabled'], /*':disabled' , ':hidden' , ':not(:visible)'*/
                        icon: {
                            required: 'glyphicon glyphicon-asterisk',
                            valid: 'glyphicon glyphicon-ok',
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        live: 'enabled',
                        message: translations.please_fill_in_the_field_correctly,
                        button: {
                            selector: 'button[type="submit"]'
                        },
                        trigger: null
                    }).on('status.field.fv', function(e, data) {
                        // Remove the required icon when the field updates its status
                        var $parent    = data.element.parents('.form-group'),
                            $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                            options    = data.fv.getOptions(),                      // Entire options
                            validators = data.fv.getOptions(data.field).validators; // The field validators

                        if (validators.notEmpty && options.icon && options.icon.required) {
                            $icon.removeClass(options.icon.required).addClass('glyphicon');
                        }
                    }).on('success.field.fv', function(e, data) {
                        // e, data parameters are the same as in err.field.fv event handler
                        // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
                        // - The submit button is clicked
                        // - The form is invalid
                        data.fv.disableSubmitButtons(false);
                    }).on('success.form.fv', function(e, data) {
                        // Prevent form submission
                        e.preventDefault();

                        // Get the form instance
                        var $form = jQuery(e.target);

                        // Get the BootstrapValidator instance
                        $form.formValidation('disableSubmitButtons', true);
                        var fv = $form.data('formValidation');

                        jQuery.post($form.attr('action'), $form.serialize(), function(result) {
                            if(result.error == true){
                                jQuery.growl.error({
                                    title: translations.error_message,
                                    message: result.message
                                });
                            }
                            else if(result.error == false){
                                jQuery.growl.notice({
                                    title: result.title,
                                    message: result.message
                                });
                                jQuery('#modal-container').find('.modal').modal('hide');

                                getMessagesList();
                            }
                            else{
                                jQuery.growl.error({
                                    title: translations.error_message,
                                    message: translations.there_has_been_an_error_please_try_again
                                });
                            }
                        }, 'json');
                    });
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
        });
    }

    /**
     * ALERTS LIST
     */
    function getAlertsList(){

        jQuery.post(jQuery('[data-action="alerts-wrapper"]').data('url'), function(result) {
            if(result.error == true){
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
            else if(result.error == false){

                if ( $.fn.DataTable.fnIsDataTable( jQuery('#alerts_table') ) ) {
                    jQuery('#alerts_table').DataTable().destroy();
                }

                jQuery('#alerts_table').html(result.html);
                jQuery('#alerts_table').find('[data-toggle="tooltip"]').tooltip();

                /**
                 * search, export
                 */
                createDatatable(jQuery('#alerts_table'), 50, true, true, true, false, 3, 'desc', false, false);

            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: translations.there_has_been_an_error_please_try_again
                });
            }
        }, 'json');

        return true;
    }

    if(jQuery('#page-alerts-list').length > 0) {
        getAlertsList();
    }

    /**
     * Add alert to task
     */
    jQuery('#alerts_table').on('click', '[data-action="add-to-tasks"]', function(){
        jQuery.post(jQuery(this).data("url"), { alert_id: jQuery(this).data('id') }, function(result) {
            if(result.error == false){
                jQuery('#modal-container').html(result.html);
                var modal = jQuery('#modal-container').find('.modal');
                modal.modal('show');

                var form = modal.find('[data-action="tasks-form"]');

                form.find('textarea.autosize').autosize({append: "\n"});
                form.find('[name="date_due"]').datepicker({
                    dateFormat: 'dd/mm/yy',
                    defaultDate: "+1d",
                    changeMonth: true,
                    changeYear: false,
                    minDate: "0d",
                    numberOfMonths: 1
                });

                /**
                 * Validate form
                 */
                form.on('init.field.fv', function(e, data) {
                    // data.fv      --> The BootstrapValidator instance
                    // data.field   --> The field name
                    // data.element --> The field element

                    var $parent    = data.element.parents('.form-group'),
                        $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                        options    = data.fv.getOptions(),                      // Entire options
                        validators = data.fv.getOptions(data.field).validators; // The field validators

                    if (validators.notEmpty && options.icon && options.icon.required) {
                        // The field uses notEmpty validator
                        // Add required icon
                        $icon.addClass(options.icon.required).show();
                    }
                });

                form.formValidation({
                    framework: 'bootstrap',
                    excluded: [':disabled'], /*':disabled' , ':hidden' , ':not(:visible)'*/
                    icon: {
                        required: 'glyphicon glyphicon-asterisk',
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    live: 'enabled',
                    message: translations.please_fill_in_the_field_correctly,
                    button: {
                        selector: 'button[type="submit"]'
                    },
                    trigger: null
                }).on('status.field.fv', function(e, data) {
                    // Remove the required icon when the field updates its status
                    var $parent    = data.element.parents('.form-group'),
                        $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                        options    = data.fv.getOptions(),                      // Entire options
                        validators = data.fv.getOptions(data.field).validators; // The field validators

                    if (validators.notEmpty && options.icon && options.icon.required) {
                        $icon.removeClass(options.icon.required).addClass('glyphicon');
                    }
                }).on('success.field.fv', function(e, data) {
                    // e, data parameters are the same as in err.field.fv event handler
                    // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
                    // - The submit button is clicked
                    // - The form is invalid
                    data.fv.disableSubmitButtons(false);
                }).on('success.form.fv', function(e, data) {
                    // Prevent form submission
                    e.preventDefault();

                    // Get the form instance
                    var $form = jQuery(e.target);

                    // Get the BootstrapValidator instance
                    $form.formValidation('disableSubmitButtons', true);
                    var fv = $form.data('formValidation');

                    jQuery.post($form.attr('action'), $form.serialize(), function(result) {
                        if(result.error == true){
                            jQuery.growl.error({
                                title: translations.error_message,
                                message: result.message
                            });
                        }
                        else if(result.error == false){
                            jQuery.growl.notice({
                                title: result.title,
                                message: result.message
                            });
                            jQuery('#modal-container').find('.modal').modal('hide');

                            if(jQuery('#alerts_table').data('function') == 'getDashboardAlerts'){
                                getDashboardAlerts();
                                getDashboardTasks();
                            }
                            else{
                                getAlertsList();
                            }
                        }
                        else{
                            jQuery.growl.error({
                                title: translations.error_message,
                                message: translations.there_has_been_an_error_please_try_again
                            });
                        }
                    }, 'json');
                });
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");
    });

    /**
     * Remove alert
     */
    jQuery('#alerts_table').on('click', '[data-action="remove-alert"]', function(){
        jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id') }, function(result) {
            if(result.error == false){
                jQuery.growl.notice({
                    title: result.title,
                    message: result.message
                });

                if(jQuery('#alerts_table').data('function') == 'getDashboardAlerts'){
                    getDashboardAlerts();
                }
                else{
                    getAlertsList();
                }
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");
    });

    /**
     * TARGETS LIST
     */
    function getTargetsList(){

        jQuery.post(jQuery('[data-action="targets-wrapper"]').data('url'), function(result) {
            if(result.error == true){
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
            else if(result.error == false){

                if ( $.fn.DataTable.fnIsDataTable( jQuery('#targets_table') ) ) {
                    jQuery('#targets_table').DataTable().destroy();
                }

                jQuery('#targets_table').html(result.html);
                jQuery('#targets_table').find('[data-toggle="tooltip"]').tooltip();

                /**
                 * search, export
                 */
                createDatatable(jQuery('#targets_table'), 50, true, false, false, false, false, false, false, false);
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: translations.there_has_been_an_error_please_try_again
                });
            }
        }, 'json');

        return true;
    }

    /**
     * TARGETS VIEW
     */
    function getTargetsListView(){

        jQuery.post(jQuery('[data-action="targets-wrapper"]').data('url'), function(result) {
            if(result.error == true){
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
            else if(result.error == false){
                jQuery('[data-action="targets-wrapper"]').html(result.html);
                jQuery('[data-action="targets-wrapper"]').find('[data-toggle="tooltip"]').tooltip();

                /**
                 * Knob
                 */
                jQuery(".dial").knob({
                    'draw' : function () { $(this.i).val(this.cv + '%'); }
                });
                jQuery(".dial").parent("div").css("position", "absolute");
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: translations.there_has_been_an_error_please_try_again
                });
            }
        }, 'json');

        return true;
    }

    /**
     * Add task
     */
    function addTarget(element){

        jQuery.post(element.data("url"), { }, function(result) {
            if(result.error == false){
                jQuery('#modal-container').html(result.html);
                var modal = jQuery('#modal-container').find('.modal');
                modal.modal('show');

                /**
                 * Knob
                 */
                jQuery(".dial").knob();
                /**
                 * Bootstrap checkbox
                 */
                jQuery('[data-type="bchackbox"]').bootstrapSwitch();

                var form = modal.find('[data-action="target-form"]');

                /**
                 * Validate form
                 */
                form.on('init.field.fv', function(e, data) {
                    // data.fv      --> The BootstrapValidator instance
                    // data.field   --> The field name
                    // data.element --> The field element

                    var $parent    = data.element.parents('.form-group'),
                        $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                        options    = data.fv.getOptions(),                      // Entire options
                        validators = data.fv.getOptions(data.field).validators; // The field validators

                    if (validators.notEmpty && options.icon && options.icon.required) {
                        // The field uses notEmpty validator
                        // Add required icon
                        $icon.addClass(options.icon.required).show();
                    }
                });

                form.formValidation({
                    framework: 'bootstrap',
                    excluded: [':disabled'], /*':disabled' , ':hidden' , ':not(:visible)'*/
                    icon: {
                        required: 'glyphicon glyphicon-asterisk',
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    live: 'enabled',
                    message: translations.please_fill_in_the_field_correctly,
                    button: {
                        selector: 'button[type="submit"]'
                    },
                    trigger: null
                }).on('status.field.fv', function(e, data) {
                    // Remove the required icon when the field updates its status
                    var $parent    = data.element.parents('.form-group'),
                        $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                        options    = data.fv.getOptions(),                      // Entire options
                        validators = data.fv.getOptions(data.field).validators; // The field validators

                    if (validators.notEmpty && options.icon && options.icon.required) {
                        $icon.removeClass(options.icon.required).addClass('glyphicon');
                    }
                }).on('success.field.fv', function(e, data) {
                    // e, data parameters are the same as in err.field.fv event handler
                    // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
                    // - The submit button is clicked
                    // - The form is invalid
                    data.fv.disableSubmitButtons(false);
                }).on('success.form.fv', function(e, data) {
                    // Prevent form submission
                    e.preventDefault();

                    // Get the form instance
                    var $form = jQuery(e.target);

                    // Get the BootstrapValidator instance
                    $form.formValidation('disableSubmitButtons', true);
                    var fv = $form.data('formValidation');

                    jQuery.post($form.attr('action'), $form.serialize(), function(result) {
                        if(result.error == true){
                            jQuery.growl.error({
                                title: translations.error_message,
                                message: result.message
                            });
                        }
                        else if(result.error == false){
                            jQuery.growl.notice({
                                title: result.title,
                                message: result.message
                            });
                            jQuery('#modal-container').find('.modal').modal('hide');

                            getTargetsList();
                        }
                        else{
                            jQuery.growl.error({
                                title: translations.error_message,
                                message: translations.there_has_been_an_error_please_try_again
                            });
                        }
                    }, 'json');
                });
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");

        return false;
    }

    if(jQuery('#page-targets-list').length > 0) {
        getTargetsList();
        jQuery('[data-action="add-target"]').bind('click', function(){
            addTarget(jQuery(this));
        });
    }

    /**
     * Change brand function
     */
    function changeBrandSelect(element){
        jQuery.post(jQuery('#modal-container').find('[name="brand"]').data("self-url"), { id: element.val() }, function(result) {
            if(result.error == false){
                var select = jQuery('#modal-container').find('[name="brand"]');
                select.empty();
                select.html(result.html);
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");
    }

    /**
     * Change partner function
     */
    function changePartnerSelect(element){
        jQuery.post(jQuery('#modal-container').find('[name="partner"]').data("self-url"), { id: element.val() }, function(result) {
            if(result.error == false){
                var select = jQuery('#modal-container').find('[name="partner"]');
                select.empty();
                select.html(result.html);
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");
    }

    /**
     * Change product group
     */
    jQuery('#modal-container').on('change','[data-action="change-product-group"]', function(){
        changeBrandSelect(jQuery(this));
        changePartnerSelect(jQuery(this));
        /*jQuery.post(jQuery(this).data("url"), { id: jQuery(this).val() }, function(result) {
            if(result.error == false){
                var select = jQuery('#modal-container').find('[name="partner"]');
                select.empty();
                select.html(result.html);
                changeBrandSelect(select);
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");*/
    });

    /**
     * Change partner
     */
    /*jQuery('#modal-container').on('change','[data-action="change-partner"]', function(){
        changeBrandSelect(jQuery(this));
    });*/

    if(jQuery('#targets_table').length > 0) {

        /**
         * Delete target
         */
        jQuery('#targets_table').on('click', '[data-action="delete-target"]', function(){
            jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id') }, function(result) {
                if(result.error == false){
                    jQuery.growl.notice({
                        title: result.title,
                        message: result.message
                    });

                    getTargetsList();
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
        });

        /**
         * Edit target
         */
        jQuery('#targets_table').on('click', '[data-action="edit-target"]', function(){
            jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id') }, function(result) {
                if(result.error == false){
                    jQuery('#modal-container').html(result.html);
                    var modal = jQuery('#modal-container').find('.modal');
                    modal.modal('show');

                    /**
                     * Knob
                     */
                    jQuery(".dial").knob();

                    /**
                     * Bootstrap checkbox
                     */
                    jQuery('[data-type="bchackbox"]').bootstrapSwitch();

                    /*jQuery('.dial').val(33).trigger('change')
                     .trigger(
                     'configure',
                     {
                     "max":40,
                     }
                     );*/

                    var form = modal.find('[data-action="target-form"]');

                    /**
                     * Validate form
                     */
                    form.on('init.field.fv', function(e, data) {
                        // data.fv      --> The BootstrapValidator instance
                        // data.field   --> The field name
                        // data.element --> The field element

                        var $parent    = data.element.parents('.form-group'),
                            $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                            options    = data.fv.getOptions(),                      // Entire options
                            validators = data.fv.getOptions(data.field).validators; // The field validators

                        if (validators.notEmpty && options.icon && options.icon.required) {
                            // The field uses notEmpty validator
                            // Add required icon
                            $icon.addClass(options.icon.required).show();
                        }
                    });

                    form.formValidation({
                        framework: 'bootstrap',
                        excluded: [':disabled'], /*':disabled' , ':hidden' , ':not(:visible)'*/
                        icon: {
                            required: 'glyphicon glyphicon-asterisk',
                            valid: 'glyphicon glyphicon-ok',
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        live: 'enabled',
                        message: translations.please_fill_in_the_field_correctly,
                        button: {
                            selector: 'button[type="submit"]'
                        },
                        trigger: null
                    }).on('status.field.fv', function(e, data) {
                        // Remove the required icon when the field updates its status
                        var $parent    = data.element.parents('.form-group'),
                            $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                            options    = data.fv.getOptions(),                      // Entire options
                            validators = data.fv.getOptions(data.field).validators; // The field validators

                        if (validators.notEmpty && options.icon && options.icon.required) {
                            $icon.removeClass(options.icon.required).addClass('glyphicon');
                        }
                    }).on('success.field.fv', function(e, data) {
                        // e, data parameters are the same as in err.field.fv event handler
                        // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
                        // - The submit button is clicked
                        // - The form is invalid
                        data.fv.disableSubmitButtons(false);
                    }).on('success.form.fv', function(e, data) {
                        // Prevent form submission
                        e.preventDefault();

                        // Get the form instance
                        var $form = jQuery(e.target);

                        // Get the BootstrapValidator instance
                        $form.formValidation('disableSubmitButtons', true);
                        var fv = $form.data('formValidation');

                        jQuery.post($form.attr('action'), $form.serialize(), function(result) {
                            if(result.error == true){
                                jQuery.growl.error({
                                    title: translations.error_message,
                                    message: result.message
                                });
                            }
                            else if(result.error == false){
                                jQuery.growl.notice({
                                    title: result.title,
                                    message: result.message
                                });
                                jQuery('#modal-container').find('.modal').modal('hide');

                                getTargetsList();
                            }
                            else{
                                jQuery.growl.error({
                                    title: translations.error_message,
                                    message: translations.there_has_been_an_error_please_try_again
                                });
                            }
                        }, 'json');
                    });
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
        });
    }

    if(jQuery('#page-targets-view').length > 0) {
        getTargetsListView();
    }

    /**
     * USERS LIST
     */
    function getUsersList(){

        jQuery.post(jQuery('[data-action="users-wrapper"]').data('url'), function(result) {
            if(result.error == true){
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
            else if(result.error == false){

                if ( $.fn.DataTable.fnIsDataTable( jQuery('#users_table') ) ) {
                    jQuery('#users_table').DataTable().destroy();
                }

                jQuery('#users_table').html(result.html);
                jQuery('#users_table').find('[data-toggle="tooltip"]').tooltip();

                /**
                 * search, export
                 */
                createDatatable(jQuery('#users_table'), 50, true, false, false, false, 0, 'desc', false, false);
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: translations.there_has_been_an_error_please_try_again
                });
            }
        }, 'json');

        return true;
    }



    if(jQuery('#page-users-list').length > 0) {

        getUsersList();

        /**
         * Delete user
         */
        jQuery('#users_table').on('click', '[data-action="delete-user"]', function(){
            jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id') }, function(result) {
                if(result.error == false){
                    jQuery.growl.notice({
                        title: result.title,
                        message: result.message
                    });

                    getUsersList();
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
        });

        /**
         * Reset password
         */
        jQuery('#users_table').on('click', '[data-action="reset-password-user"]', function(){
            jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id') }, function(result) {
                if(result.error == false){
                    jQuery.growl.notice({
                        title: result.title,
                        message: result.message
                    });

                    getUsersList();
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
        });

        /**
         * Disable user
         */
        jQuery('#users_table').on('click', '[data-action="disable-user"]', function(){
            jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id') }, function(result) {
                if(result.error == false){
                    jQuery.growl.notice({
                        title: result.title,
                        message: result.message
                    });

                    getUsersList();
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
        });

        /**
         * Enable user
         */
        jQuery('#users_table').on('click', '[data-action="enable-user"]', function(){
            jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id') }, function(result) {
                if(result.error == false){
                    jQuery.growl.notice({
                        title: result.title,
                        message: result.message
                    });

                    getUsersList();
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
        });
    }



    /**
     * Account profile page
     */
    if(jQuery('#page-users-form').length > 0){
        /**
         * Validate form
         */
        jQuery('[data-action="users-form"]').on('init.field.fv', function(e, data) {
            // data.fv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent    = data.element.parents('.form-group'),
                $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                options    = data.fv.getOptions(),                      // Entire options
                validators = data.fv.getOptions(data.field).validators; // The field validators

            if (validators.notEmpty && options.icon && options.icon.required) {
                // The field uses notEmpty validator
                // Add required icon
                $icon.addClass(options.icon.required).show();
            }
        });

        jQuery('[data-action="users-form"]').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'], /*':disabled' , ':hidden' , ':not(:visible)'*/
            icon: {
                required: 'glyphicon glyphicon-asterisk',
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            live: 'enabled',
            message: translations.please_fill_in_the_field_correctly,
            button: {
                selector: 'button[type="submit"]'
            },
            trigger: null
        }).on('status.field.fv', function(e, data) {
            // Remove the required icon when the field updates its status
            var $parent    = data.element.parents('.form-group'),
                $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                options    = data.fv.getOptions(),                      // Entire options
                validators = data.fv.getOptions(data.field).validators; // The field validators

            if (validators.notEmpty && options.icon && options.icon.required) {
                $icon.removeClass(options.icon.required).addClass('glyphicon');
            }
        }).on('success.field.fv', function(e, data) {
            // e, data parameters are the same as in err.field.fv event handler
            // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
            // - The submit button is clicked
            // - The form is invalid
            data.fv.disableSubmitButtons(false);
        }).on('success.form.fv', function(e, data) {
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = jQuery(e.target);

            // Get the BootstrapValidator instance
            $form.formValidation('disableSubmitButtons', true);
            var fv = $form.data('formValidation');

            var partner_ids = serealizeSelects(jQuery('[name="partners_user"]'));
            var brand_ids = serealizeSelects(jQuery('[name="brands_user"]'));

            jQuery.post($form.attr('action'), $form.serialize()+'&'+jQuery.param({ 'partner_ids': partner_ids })+'&'+jQuery.param({ 'brand_ids': brand_ids }), function(result) {
                if(result.error == true){
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
                else if(result.error == false){
                    jQuery.growl.notice({
                        title: result.title,
                        message: result.message
                    });
                    window.location.replace($form.data('redirect-url'));
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: translations.there_has_been_an_error_please_try_again
                    });
                }
            }, 'json');
        });

        /**
         * Change company
         */
        jQuery('[data-action="change-company"]').bind('change', function(){
            jQuery.post(jQuery(this).data("url"), { id: jQuery(this).val() }, function(result) {
                if(result.error == false){
                    jQuery('[name="product_group"]').html(result.html);
                    jQuery('[data-action="users-form"]').data('formValidation').revalidateField(jQuery('[name="product_group"]'));
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
        });
    }

    /**
     * Account application settings page
      */
    if(jQuery('#page-user-settings').length > 0) {

        /**
         * Targets
         */
        getTargetsList();
        jQuery('[data-action="add-target"]').bind('click', function(){
            addTarget(jQuery(this));
        });

        /**
         * Validate basic settings form
         */
        jQuery('[data-action="basic-settings-form"]').on('init.field.fv', function(e, data) {
            // data.fv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent    = data.element.parents('.form-group'),
                $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                options    = data.fv.getOptions(),                      // Entire options
                validators = data.fv.getOptions(data.field).validators; // The field validators

            if (validators.notEmpty && options.icon && options.icon.required) {
                // The field uses notEmpty validator
                // Add required icon
                $icon.addClass(options.icon.required).show();
            }
        });

        jQuery('[data-action="basic-settings-form"]').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'], /*':disabled' , ':hidden' , ':not(:visible)'*/
            icon: {
                required: 'glyphicon glyphicon-asterisk',
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            live: 'enabled',
            message: translations.please_fill_in_the_field_correctly,
            button: {
                selector: 'button[type="submit"]'
            },
            trigger: null
        }).on('status.field.fv', function(e, data) {
            // Remove the required icon when the field updates its status
            var $parent    = data.element.parents('.form-group'),
                $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                options    = data.fv.getOptions(),                      // Entire options
                validators = data.fv.getOptions(data.field).validators; // The field validators

            if (validators.notEmpty && options.icon && options.icon.required) {
                $icon.removeClass(options.icon.required).addClass('glyphicon');
            }
        }).on('success.field.fv', function(e, data) {
            // e, data parameters are the same as in err.field.fv event handler
            // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
            // - The submit button is clicked
            // - The form is invalid
            data.fv.disableSubmitButtons(false);
        }).on('success.form.fv', function(e, data) {
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = jQuery(e.target);

            // Get the BootstrapValidator instance
            $form.formValidation('disableSubmitButtons', true);
            var fv = $form.data('formValidation');

            jQuery.post($form.attr('action'), $form.serialize(), function(result) {
                if(result.error == true){
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
                else if(result.error == false){
                    jQuery.growl.notice({
                        title: result.title,
                        message: result.message
                    });
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: translations.there_has_been_an_error_please_try_again
                    });
                }
            }, 'json');
        });

        /**
         * Validate partners brands form
         */
        jQuery('[data-action="partners-brands-form"]').on('init.field.fv', function(e, data) {
            // data.fv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent    = data.element.parents('.form-group'),
                $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                options    = data.fv.getOptions(),                      // Entire options
                validators = data.fv.getOptions(data.field).validators; // The field validators

            if (validators.notEmpty && options.icon && options.icon.required) {
                // The field uses notEmpty validator
                // Add required icon
                $icon.addClass(options.icon.required).show();
            }
        });

        jQuery('[data-action="partners-brands-form"]').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'], /*':disabled' , ':hidden' , ':not(:visible)'*/
            icon: {
                required: 'glyphicon glyphicon-asterisk',
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            live: 'enabled',
            message: translations.please_fill_in_the_field_correctly,
            button: {
                selector: 'button[type="submit"]'
            },
            trigger: null
        }).on('status.field.fv', function(e, data) {
            // Remove the required icon when the field updates its status
            var $parent    = data.element.parents('.form-group'),
                $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                options    = data.fv.getOptions(),                      // Entire options
                validators = data.fv.getOptions(data.field).validators; // The field validators

            if (validators.notEmpty && options.icon && options.icon.required) {
                $icon.removeClass(options.icon.required).addClass('glyphicon');
            }
        }).on('success.field.fv', function(e, data) {
            // e, data parameters are the same as in err.field.fv event handler
            // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
            // - The submit button is clicked
            // - The form is invalid
            data.fv.disableSubmitButtons(false);
        }).on('success.form.fv', function(e, data) {
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = jQuery(e.target);

            // Get the BootstrapValidator instance
            $form.formValidation('disableSubmitButtons', true);
            var fv = $form.data('formValidation');

            var partner_ids = serealizeSelects(jQuery('[name="partners_user"]'));
            var brand_ids = serealizeSelects(jQuery('[name="brands_user"]'));

            jQuery.post($form.attr('action'), jQuery.param({ 'partner_ids': partner_ids })+'&'+jQuery.param({ 'brand_ids': brand_ids }), function(result) {
                if(result.error == true){
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
                else if(result.error == false){
                    jQuery.growl.notice({
                        title: result.title,
                        message: result.message
                    });
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: translations.there_has_been_an_error_please_try_again
                    });
                }
            }, 'json');
        });
    }

    if(jQuery('#page-user-profile').length > 0) {
        /**
         * Validate form
         */
        jQuery('[data-action="profile-form"]').on('init.field.fv', function(e, data) {
            // data.fv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent    = data.element.parents('.form-group'),
                $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                options    = data.fv.getOptions(),                      // Entire options
                validators = data.fv.getOptions(data.field).validators; // The field validators

            if (validators.notEmpty && options.icon && options.icon.required) {
                // The field uses notEmpty validator
                // Add required icon
                $icon.addClass(options.icon.required).show();
            }
        });

        jQuery('[data-action="profile-form"]').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'], /*':disabled' , ':hidden' , ':not(:visible)'*/
            icon: {
                required: 'glyphicon glyphicon-asterisk',
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            live: 'enabled',
            message: translations.please_fill_in_the_field_correctly,
            button: {
                selector: 'button[type="submit"]'
            },
            trigger: null
        }).on('status.field.fv', function(e, data) {
            // Remove the required icon when the field updates its status
            var $parent    = data.element.parents('.form-group'),
                $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                options    = data.fv.getOptions(),                      // Entire options
                validators = data.fv.getOptions(data.field).validators; // The field validators

            if (validators.notEmpty && options.icon && options.icon.required) {
                $icon.removeClass(options.icon.required).addClass('glyphicon');
            }
        }).on('success.field.fv', function(e, data) {
            // e, data parameters are the same as in err.field.fv event handler
            // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
            // - The submit button is clicked
            // - The form is invalid
            data.fv.disableSubmitButtons(false);
        }).on('success.form.fv', function(e, data) {
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = jQuery(e.target);

            // Get the BootstrapValidator instance
            $form.formValidation('disableSubmitButtons', true);
            var fv = $form.data('formValidation');

            jQuery.post($form.attr('action'), $form.serialize(), function(result) {
                if(result.error == true){
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
                else if(result.error == false){
                    jQuery.growl.notice({
                        title: result.title,
                        message: result.message
                    });
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: translations.there_has_been_an_error_please_try_again
                    });
                }
            }, 'json');
        });

        /**
         * Validate form
         */
        jQuery('[data-action="password-change-form"]').on('init.field.fv', function(e, data) {
            // data.fv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent    = data.element.parents('.form-group'),
                $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                options    = data.fv.getOptions(),                      // Entire options
                validators = data.fv.getOptions(data.field).validators; // The field validators

            if (validators.notEmpty && options.icon && options.icon.required) {
                // The field uses notEmpty validator
                // Add required icon
                $icon.addClass(options.icon.required).show();
            }
        });

        jQuery('[data-action="password-change-form"]').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'], /*':disabled' , ':hidden' , ':not(:visible)'*/
            icon: {
                required: 'glyphicon glyphicon-asterisk',
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            live: 'enabled',
            message: translations.please_fill_in_the_field_correctly,
            button: {
                selector: 'button[type="submit"]'
            },
            trigger: null
        }).on('status.field.fv', function(e, data) {
            // Remove the required icon when the field updates its status
            var $parent    = data.element.parents('.form-group'),
                $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                options    = data.fv.getOptions(),                      // Entire options
                validators = data.fv.getOptions(data.field).validators; // The field validators

            if (validators.notEmpty && options.icon && options.icon.required) {
                $icon.removeClass(options.icon.required).addClass('glyphicon');
            }
        }).on('success.field.fv', function(e, data) {
            // e, data parameters are the same as in err.field.fv event handler
            // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
            // - The submit button is clicked
            // - The form is invalid
            data.fv.disableSubmitButtons(false);
        }).on('success.form.fv', function(e, data) {
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = jQuery(e.target);

            // Get the BootstrapValidator instance
            $form.formValidation('disableSubmitButtons', true);
            var fv = $form.data('formValidation');

            jQuery.post($form.attr('action'), $form.serialize(), function(result) {
                if(result.error == true){
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
                else if(result.error == false){
                    jQuery.growl.notice({
                        title: result.title,
                        message: result.message
                    });
                    window.location.replace($form.data('redirect-url'));
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: translations.there_has_been_an_error_please_try_again
                    });
                }
            }, 'json');
        });
    }



    /**
     * Get products
     * @param form_data
     * @param trigger_form
     */
    function productsSearchAction(form_data,trigger_form){

        if(!form_data){
            form_data = [];
        }

        jQuery.post(jQuery('[data-action="advanced-search-form"]').attr('action'), form_data, function(result) {
            if(result.error == true){
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
            else if(result.error == false){
                /*if(trigger_form){
                    jQuery('[data-action="advanced-search-form"]').parents('.panel').find('.panel-collapse').trigger('click');
                }*/

                if ( $.fn.DataTable.fnIsDataTable( jQuery('#products_table') ) ) {
                    jQuery('#products_table').DataTable().destroy();
                }

                jQuery('#products_table').html(result.html);
                jQuery('#products_table').find('[data-toggle="tooltip"]').tooltip();

                /**
                 * search, export
                 */
                createDatatable(jQuery('#products_table'), 50, false, true, true, true, false, false, false, 'checkVisibleImages');
                compareList = [];
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: translations.there_has_been_an_error_please_try_again
                });
            }
        }, 'json');

        return true;
    }

    /**
     * SEARCH
     */
    if(jQuery('#page-search').length > 0){

        var query = "";
        if(jQuery('[data-action="advanced-search-form"]').data('addon-type') && jQuery('[data-action="advanced-search-form"]').data('addon')){
            query = "?"+jQuery('[data-action="advanced-search-form"]').data('addon-type')+"="+jQuery('[data-action="advanced-search-form"]').data('addon');
        }

        /**
         * GENERIRA SEARCH
         */
        jQuery.ajax({
            type: "GET",
            url: jQuery('[data-action="advanced-search-form"]').data("url")+query,
            success: function(data, dataType)
            {
                if(data.error == false){
                    jQuery('[data-action="advanced-search-form"]').append(data.html);
                    jQuery('[data-action="advanced-search-form"]').find('[data-validate="true"]').each(function(){
                        jQuery('[data-action="advanced-search-form"]').formValidation('addField', jQuery(this).attr('name'));
                    });
                    jQuery('[data-action="advanced-search-form"]').parents('.panel').find('.panel-collapse').trigger('click');

                    /**
                     * Bootstrap checkbox
                     */
                    jQuery('[data-action="advanced-search-form"]').find('[data-type="bchackbox"]').bootstrapSwitch();

                    productsSearchAction(jQuery('[data-action="advanced-search-form"]').serialize(),false);

                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown)
            {
                jQuery.growl.error({
                    title: translations.error_message,
                    message: translations.there_has_been_an_error_please_try_again
                });
            }
        });

        //productsSearchAction(jQuery('[data-action="advanced-search-form"]').serialize(),false);

        /**
         * Bind popover
         */
        jQuery('#products_table').on('mouseenter','[data-action="popover"]',function() {
            jQuery(this).popover({
                html: true,
                trigger: 'hover',
                content: function() {
                    return jQuery(this).children('.sp-product-description').html();
                },
                title: function() {
                    return "";
                },
                placement: "top"
            });
            jQuery(this).popover('show');
        });
        /*.mouseleave(function() {
            //jQuery(this).popover('hide');
        });*/

        jQuery('#products_table').on('click','[data-action="toggle-bookmark"]',function() {
            var elem = jQuery(this);

            jQuery.post(elem.data("url"), { id: elem.data('id') }, function(result) {
                if(result.error == false){
                    elem.children().toggleClass('sp-bookmarked');
                    if(elem.children().hasClass('sp-bookmarked')){
                        elem.attr('title',elem.data('title-2'));
                    }
                    else{
                        elem.attr('title',elem.data('title-1'));
                    }
                    elem.tooltip('fixTitle');
                    elem.tooltip('show');

                    jQuery.growl.notice({
                        title: result.title,
                        message: result.message
                    });
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");

            return false;
        });

        /**
         * Toggle product compare
         */
        jQuery('#products_table').on('click','[data-action="toggle-compare"]',function() {
            if(jQuery(this).children().hasClass('glyphicon-unchecked')){
                if(compareList.length > 5){
                    jQuery.growl.error({
                        title: translations.compare,
                        message: translations.too_many_products
                    });
                    return false;
                }
                jQuery(this).attr('title',jQuery(this).data('title-2'));
                addRemoveFromCompareArray(jQuery(this).data('id'),true);
            }
            else{
                addRemoveFromCompareArray(jQuery(this).data('id'),false);
                jQuery(this).attr('title',jQuery(this).data('title-1'));
            }
            jQuery(this).children().toggleClass('glyphicon-check').toggleClass('glyphicon-unchecked');
            jQuery(this).tooltip('fixTitle');
            jQuery(this).tooltip('show');

            if(compareList.length > 1){
                jQuery('[data-action="compare-products"]').show();
            }
            else{
                jQuery('[data-action="compare-products"]').hide();
            }
        });

        /**
         * Find similar
         */
        jQuery('#products_table').on('click','[data-action="find-similar"]',function() {
            jQuery.each(jQuery(this).parent().parent().find('[data-action="similar"]'),function(){
                jQuery('[data-action="advanced-search-form"]').find('[name="'+jQuery(this).data('similar')+'"]').val(jQuery(this).data('value'));
            });
            jQuery('html, body').animate({
                scrollTop: jQuery('[data-action="advanced-search-form"]').offset().top - 120
            }, 500);

            return false;
        });

        /**
         * Compare product details
         */
        jQuery('#products_table').on('click','[data-action="compare-partners"]',function() {
            jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data("id") }, function(result) {
                if(result.error == false){
                    jQuery('#modal-container').html(result.html);
                    var modal = jQuery('#modal-container').find('.modal');
                    modal.modal('show');
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
            return false;
        });

        /**
         * Toggle images
         */
        jQuery('[data-action="show-images"]').on('switchChange.bootstrapSwitch', function(event, state) {
            checkVisibleImages();
            return false;
        });

        /**
         * Toggle web/regular price
         */
        jQuery('[data-action="show-regular-price"]').on('switchChange.bootstrapSwitch', function(event, state) {
            checkVisiblePrices();
            return false;
        });

        /**
         * Reset form
         */
        jQuery('[data-action="advanced-search-form"]').on('click','[data-action="reset-form"]',function() {
            resetFrom(jQuery('[data-action="advanced-search-form"]'));
            return false;
        });

        /**
         * Compare products
         */
        jQuery('[data-action="compare-products"]').bind('click', function(e) {
            jQuery.post(jQuery(this).data("url"), { product_ids: compareList }, function(result) {
                if(result.error == false){
                    jQuery('#modal-container').html(result.html);
                    var modal = jQuery('#modal-container').find('.modal');
                    modal.modal('show');
                    jQuery('[data-action="compare-products"]').hide();
                    compareList = [];

                    jQuery('#products_table').find('.glyphicon-check').each(function(e){
                        jQuery(this).toggleClass('glyphicon-check').toggleClass('glyphicon-unchecked');
                        jQuery(this).parent().attr('title',jQuery(this).parent().data('title-1'));
                        jQuery(this).parent().tooltip('fixTitle');
                        jQuery(this).parent().tooltip('show');
                        jQuery(this).parent().tooltip('hide');
                    });
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
            return false;
        });

        /**
         * Price history
         */
        jQuery('#products_table').on('click','[data-action="price-history"]', function(e) {
            jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data("id") }, function(result) {
                if(result.error == false){
                    jQuery('#modal-container').html(result.html);
                    var modal = jQuery('#modal-container').find('.modal');
                    modal.modal('show');

                    renderPriceHistoryChart(result.data_web_price,result.data_regular_price);
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
            return false;
        });

        /**
         * Create ticket from product
         */
        jQuery('#products_table').on('click','[data-action="add-ticket-product"]', function(e) {
            jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data("id") }, function(result) {
                if(result.error == false){
                    jQuery('#modal-container').html(result.html);
                    var modal = jQuery('#modal-container').find('.modal');
                    modal.modal('show');

                    var form = modal.find('[data-action="ticket-form"]');

                    form.find('textarea.autosize').autosize({append: "\n"});
                    form.find('[data-action="multi-select2"]').multiSelect();

                    /**
                     * Validate form
                     */
                    form.on('init.field.fv', function(e, data) {
                        // data.fv      --> The BootstrapValidator instance
                        // data.field   --> The field name
                        // data.element --> The field element

                        var $parent    = data.element.parents('.form-group'),
                            $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                            options    = data.fv.getOptions(),                      // Entire options
                            validators = data.fv.getOptions(data.field).validators; // The field validators

                        if (validators.notEmpty && options.icon && options.icon.required) {
                            // The field uses notEmpty validator
                            // Add required icon
                            $icon.addClass(options.icon.required).show();
                        }
                    });

                    form.formValidation({
                        framework: 'bootstrap',
                        excluded: [':disabled'], /*':disabled' , ':hidden' , ':not(:visible)'*/
                        icon: {
                            required: 'glyphicon glyphicon-asterisk',
                            valid: 'glyphicon glyphicon-ok',
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        live: 'enabled',
                        message: translations.please_fill_in_the_field_correctly,
                        button: {
                            selector: 'button[type="submit"]'
                        },
                        trigger: null
                    }).on('status.field.fv', function(e, data) {
                        // Remove the required icon when the field updates its status
                        var $parent    = data.element.parents('.form-group'),
                            $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                            options    = data.fv.getOptions(),                      // Entire options
                            validators = data.fv.getOptions(data.field).validators; // The field validators

                        if (validators.notEmpty && options.icon && options.icon.required) {
                            $icon.removeClass(options.icon.required).addClass('glyphicon');
                        }
                    }).on('success.field.fv', function(e, data) {
                        // e, data parameters are the same as in err.field.fv event handler
                        // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
                        // - The submit button is clicked
                        // - The form is invalid
                        data.fv.disableSubmitButtons(false);
                    }).on('success.form.fv', function(e, data) {
                        // Prevent form submission
                        e.preventDefault();

                        // Get the form instance
                        var $form = jQuery(e.target);

                        // Get the BootstrapValidator instance
                        $form.formValidation('disableSubmitButtons', true);
                        var fv = $form.data('formValidation');

                        var user_ids = serealizeSelects(jQuery('[name="users"]'));

                        jQuery.post($form.attr('action'), $form.serialize()+'&'+jQuery.param({ 'user_ids': user_ids }), function(result) {
                            if(result.error == true){
                                jQuery.growl.error({
                                    title: translations.error_message,
                                    message: result.message
                                });
                            }
                            else if(result.error == false){
                                jQuery.growl.notice({
                                    title: result.title,
                                    message: result.message
                                });
                                jQuery('#modal-container').find('.modal').modal('hide');
                            }
                            else{
                                jQuery.growl.error({
                                    title: translations.error_message,
                                    message: translations.there_has_been_an_error_please_try_again
                                });
                            }
                        }, 'json');
                    });
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
            }, "json");
            return false;
        });




        /**
         * Validate form
         */
        jQuery('[data-action="advanced-search-form"]').on('init.field.fv', function(e, data) {
            // data.fv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent    = data.element.parents('.form-group'),
                $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                options    = data.fv.getOptions(),                      // Entire options
                validators = data.fv.getOptions(data.field).validators; // The field validators

            if (validators.notEmpty && options.icon && options.icon.required) {
                // The field uses notEmpty validator
                // Add required icon
                $icon.addClass(options.icon.required).show();
            }
        });

        jQuery('[data-action="advanced-search-form"]').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'], /*':disabled' , ':hidden' , ':not(:visible)'*/
            icon: {
                required: 'glyphicon glyphicon-asterisk',
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            live: 'enabled',
            message: translations.please_fill_in_the_field_correctly,
            button: {
                selector: 'button[type="submit"]'
            },
            trigger: null
        }).on('status.field.fv', function(e, data) {
            // Remove the required icon when the field updates its status
            var $parent    = data.element.parents('.form-group'),
                $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                options    = data.fv.getOptions(),                      // Entire options
                validators = data.fv.getOptions(data.field).validators; // The field validators

            if (validators.notEmpty && options.icon && options.icon.required) {
                $icon.removeClass(options.icon.required).addClass('glyphicon');
            }
        }).on('success.field.fv', function(e, data) {
            // e, data parameters are the same as in err.field.fv event handler
            // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
            // - The submit button is clicked
            // - The form is invalid
            data.fv.disableSubmitButtons(false);
        }).on('success.form.fv', function(e, data) {
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = jQuery(e.target);

            // Get the BootstrapValidator instance
            //$form.formValidation('disableSubmitButtons', true);
            var fv = $form.data('formValidation');

            productsSearchAction($form.serialize(),true);

            // Use Ajax to submit form data

            return false;
        });

    }

    /**
     * DASHBOARD
     */
    if(jQuery('#page-dashboard').length > 0){



    }
});