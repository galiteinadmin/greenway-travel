/**
 * Custom callback after toner edit
 * @param form
 * @returns {boolean}
 */
function reloadTonerTable(form){
    if(jQuery('#page_toner_list').length > 0) {
        jQuery('#toner_table').DataTable().ajax.reload(null, false);
    }
    else{
        jQuery('#toner_no_printers_table').DataTable().ajax.reload(null, false);
        jQuery('#toner_latest_table').DataTable().ajax.reload(null, false);
    }
    return false;
}

/**
 * Initialize toner form
 * @param form
 */
function tonerEditInitialize(form){
    jQuery('[data-action="select2toner"]').select2({
        minimumInputLength: 3,
        width: '100%',
        closeOnSelect: false,
        ajax: {
            url: jQuery('[data-action="select2toner"]').data('search-url'),
            dataType: 'json',
            quietMillis: 100,
            data: function (term) {
                return {
                    q: term
                };
            },
            processResults: function (data, params) {
                return {
                    results: jQuery.map(data.entities, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            }
            //cache: true
        },
        templateResult: select2FormatResult,
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    jQuery('[data-action="select2printer"]').select2({
        minimumInputLength: 3,
        width: '100%',
        closeOnSelect: false,
        ajax: {
            url: jQuery('[data-action="select2printer"]').data('search-url'),
            dataType: 'json',
            quietMillis: 100,
            data: function (term) {
                return {
                    q: term
                };
            },
            processResults: function (data, params) {
                return {
                    results: jQuery.map(data.entities, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            }
            //cache: true
        },
        templateResult: select2FormatResult,
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    /**
     * Add printers from selected toner
     */
    jQuery('[name="toner_select"]').on('change', function(e) {
        var option = jQuery('[name="toner_select"] option:selected');
        var id = option.val();

        jQuery.post(jQuery('[data-action="add-printer-to-toner"]').data("similar-url"), { toner_similar_id: id,toner_id: jQuery('[name="id"]').val() }, function(result) {
            if(result.error == false){
                jQuery.growl.notice({
                    title: result.title,
                    message: result.message
                });

                option.remove();
                jQuery('[data-action="add-printer-to-toner"]').append(result.html);
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");

    });

    /**
     * Add selected printer
     */
    jQuery('[name="printer_select"]').on('change', function(e) {
        var option = jQuery('[name="printer_select"] option:selected');
        var id = option.val();

        if(jQuery('[data-action="add-printer-to-toner"]').find('[data-id="'+id+'"]').length > 0){
            return false;
        }

        jQuery.post(jQuery('[data-action="add-printer-to-toner"]').data("url"), { toner_id: jQuery('[name="id"]').val(), printer_id: id }, function(result) {
            if(result.error == false){
                jQuery.growl.notice({
                    title: result.title,
                    message: result.message
                });

                option.remove();
                jQuery('[data-action="add-printer-to-toner"]').append(result.html);
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");

    });

    /**
     * Remove printer from toner
     */
    jQuery('[data-action="add-printer-to-toner"]').on('click', '[data-action="remove"]', function(e) {

        var printer = jQuery(this);

        jQuery.post(jQuery('[data-action="add-printer-to-toner"]').data("remove-url"), { toner_id: jQuery('[name="id"]').val(), printer_id: printer.data('id') }, function(result) {
            if(result.error == false){
                jQuery.growl.notice({
                    title: result.title,
                    message: result.message
                });

                printer.remove();
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");

    });

    form.initializeValidation();

    return false;
}

/**
 * Initialize printer form
 * @param form
 */
function printerEditInitialize(form){

    jQuery('[data-action="select2toner"]').select2({
        minimumInputLength: 3,
        width: '100%',
        closeOnSelect: false,
        ajax: {
            url: jQuery('[data-action="select2toner"]').data('search-url'),
            dataType: 'json',
            quietMillis: 100,
            data: function (term) {
                return {
                    q: term
                };
            },
            processResults: function (data, params) {
                return {
                    results: jQuery.map(data.entities, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            }
            //cache: true
        },
        templateResult: select2FormatResult,
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    jQuery('[data-action="select2printer"]').select2({
        minimumInputLength: 3,
        width: '100%',
        closeOnSelect: false,
        ajax: {
            url: jQuery('[data-action="select2printer"]').data('search-url'),
            dataType: 'json',
            quietMillis: 100,
            data: function (term) {
                return {
                    q: term
                };
            },
            processResults: function (data, params) {
                return {
                    results: jQuery.map(data.entities, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            }
            //cache: true
        },
        templateResult: select2FormatResult,
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    /**
     * Add selected toner
     */
    jQuery('[name="toner_select"]').on('change', function(e) {
        var option = jQuery('[name="toner_select"] option:selected');
        var id = option.val();
        if(jQuery('[data-action="add-toner-to-printer"]').find('[data-id="'+id+'"]').length > 0){
            return false;
        }

        jQuery.post(jQuery('[data-action="add-toner-to-printer"]').data("url"), { toner_id: id, printer_id: jQuery('[name="id"]').val() }, function(result) {
            if(result.error == false){
                jQuery.growl.notice({
                    title: result.title,
                    message: result.message
                });

                option.remove();
                jQuery('[data-action="add-toner-to-printer"]').append(result.html);
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");

    });

    /**
     * Add toners from selected printers
     */
    jQuery('[name="printer_select"]').on('change', function(e) {
        var option = jQuery('[name="printer_select"] option:selected');
        var id = option.val();

        jQuery.post(jQuery('[data-action="add-toner-to-printer"]').data("similar-url"), { printer_similar_id: id, printer_id: jQuery('[name="id"]').val() }, function(result) {
            if(result.error == false){
                jQuery.growl.notice({
                    title: result.title,
                    message: result.message
                });

                option.remove();
                jQuery('[data-action="add-toner-to-printer"]').append(result.html);
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");

    });

    /**
     * Remove toner from printer
     */
    jQuery('[data-action="add-toner-to-printer"]').on('click', '[data-action="remove"]', function(e) {

        var toner = jQuery(this);

        jQuery.post(jQuery('[data-action="add-toner-to-printer"]').data("remove-url"), { toner_id: toner.data('id'), printer_id: jQuery('[name="id"]').val() }, function(result) {
            if(result.error == false){
                jQuery.growl.notice({
                    title: result.title,
                    message: result.message
                });

                toner.remove();
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");

    });

    form.initializeValidation();

    return false;
}

/**
 * Created by Davor on 30.6.2016..
 */
jQuery(document).ready(function() {

    if(jQuery('body').find('.datatables').length > 0){
        jQuery('body').find('.datatables').each(function(e){
            jQuery('#'+jQuery(this).attr('id')).createDatatable();
        });
    }

    /**
     * Account profile page
     */
    if(jQuery('#page-users-form').length > 0){
        jQuery('[data-action="form"]').initializeValidation();
    }

    /**
     * Account application settings page
     */
    if(jQuery('#page-user-settings').length > 0) {
        jQuery('[data-action="form"]').initializeValidation();
    }

    if(jQuery('#page-user-profile').length > 0) {
        jQuery('[data-action="form"]').initializeValidation();
    }


});