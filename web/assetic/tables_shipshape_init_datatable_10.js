/**
 * Create datatable
 * @returns {boolean}
 */
jQuery.fn.extend({
    insertDatatableData: function(data){
        jQuery(this).find('tbody').html(data.html);

        /**
         * Other methods
         */
        jQuery(this).find('[data-toggle="tooltip"]').tooltip();

        return false;
    },
    setFilterHtml: function(){
        var html = '<tr class="sp-filter-holder">';
        if(jQuery(this).find('.sp-table-header').length > 0){
            jQuery(this).find('.sp-table-header').children('th').each(function(index){
                html = html+'<th';
                if(jQuery(this).data("col-filter")){
                    html = html+' id="sp-filter-holder-'+index+'" class="sp-filter"';
                }
                html = html+'></th>';
            });
        }
        html = html+'</tr>';

        jQuery(this).find('.sp-table-header').before(html);
        jQuery(this).find('.sp-filter-holder').find('th:last-child').addClass('sp-actions-td sp-actions-td-filer').html('<span class="sp-actions-td-inner"></span>');

        return false;
    },
    setYFilterHtmlButtons: function(){
        jQuery('#sp-filter-buttons-template').children().insertBefore(jQuery(this));
    },
    createDatatable: function () {

        var table = jQuery(this);
        var datatable_configuration = {};

        if ( $.fn.DataTable.fnIsDataTable( table ) ) {
            table.DataTable().destroy();
        }

        var buttonCommon = {
            exportOptions: {
                format: {
                    body: function ( data, column, row, node ) {
                        var regex = /(<([^>]+)>)/ig
                        return data.replace(regex, "");
                    }
                }
            }
        };

        var datatable_header = "<'row'<'col-xs-6'l><'col-xs-6'f>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>";
        if(table.data('export') && table.data('search')){
            datatable_header = "<'row'<'col-xs-6'l><'col-xs-6'B><'col-xs-3'f>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>";
        }
        else if(table.data('export') && !table.data('search')){
            datatable_header = "<'row'<'col-xs-6'l><'col-xs-3'><'col-xs-6'f>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>";
        }
        else if(!table.data('export') && !table.data('search')){
            datatable_header = "<'row'<'col-xs-6'l><'col-xs-3'><'col-xs-6'>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>";
        }

        var aoColumns = [];
        if(table.find('.sp-table-header').length > 0){
            table.find('.sp-table-header').children('th').each(function(e){
                var prop = {};
                prop['mDataProp'] = jQuery(this).data("col-name");
                prop['bSearchable'] = jQuery(this).data("col-search");
                prop['bSortable'] = jQuery(this).data("col-sort");
                aoColumns.push(prop);
            });
        }

        if(table.data('empty')){
            datatable_configuration = {
                "sDom":  't',
            };
        }
        else{
            datatable_configuration = {
                "sDom": datatable_header,
                "sPaginationType": "bootstrap",
                "pageLength": table.data('limit'),
                "oLanguage": {
                    "sLengthMenu": "_MENU_ "+translations.records_per_page,
                    "sSearch": ""
                },
                "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]]
            };
            if(aoColumns.length > 0){
                datatable_configuration["aoColumns"] = aoColumns;
            }

            if(table.data('f-columns')){
                datatable_configuration["scrollY"] = "80vh";
                datatable_configuration["scrollX"] = true;
                datatable_configuration["scrollCollapse"] = true;
                datatable_configuration["fixedColumns"] = {};
                var lcolumn = table.data('fl-column');
                var rcolumn = table.data('fr-column');
                if(jQuery.isNumeric(lcolumn)){
                    datatable_configuration["fixedColumns"]["leftColumns"] = lcolumn;
                }
                if(jQuery.isNumeric(rcolumn)){
                    datatable_configuration["fixedColumns"]["rightColumns"] = rcolumn;
                }
            }

            /*table.on('click','[data-col-sort="true"]',function(){
             jQuery(this).toggleClass('ascending').toggleClass('descending');
             });*/
        }

        if(table.data('order-col') !== false){
            datatable_configuration["order"] = [[table.data('order-col'), table.data('order-dir')]];
        }

        if(table.data('export')){
            datatable_configuration["buttons"] = [
                jQuery.extend( true, {}, buttonCommon, {
                    extend:    'copyHtml5',
                    text:      '<i class="fa fa-files-o"></i> Copy',
                    titleAttr: 'Copy',
                    exportOptions: {
                        columns: ':not(.no-export)'
                    }
                }),
                jQuery.extend( true, {}, buttonCommon, {
                    extend:    'excelHtml5',
                    text:      '<i class="fa fa-file-excel-o"></i> Excel',
                    titleAttr: 'Excel',
                    title:  translations.data_export,
                    exportOptions: {
                        columns: ':not(.no-export)'
                    }
                }),
                jQuery.extend( true, {}, buttonCommon, {
                    extend:    'csvHtml5',
                    text:      '<i class="fa fa-file-text-o"></i> CSV',
                    titleAttr: 'CSV',
                    title:  translations.data_export,
                    exportOptions: {
                        columns: ':not(.no-export)'
                    }
                })
            ]
        }

        if(table.data('filter') && !table.data('server')){
            table.setFilterHtml();
            datatable_configuration["initComplete"] = function (e) {
                this.api().columns().every( function () {
                    var column = this;
                    var cellIndex = jQuery(column.header()).index();
                    var filterWrapper = jQuery(column.header()).closest('tr').prev().children().eq(cellIndex);
                    if(!filterWrapper.hasClass('sp-filter')){
                        return true;
                    }
                    var select = jQuery('<select><option value=""></option></select>')
                        .appendTo( filterWrapper )
                        .on( 'change', function () {
                            var val = jQuery.fn.dataTable.util.escapeRegex(
                                jQuery(this).val()
                            );
                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        });

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    });
                });
            }
        }

        if(table.data('server')){
            datatable_configuration["processing"] = true;
            datatable_configuration["serverSide"] = true;
            datatable_configuration["serverData"] = function ( source, request_data, fnCallback, settings )
            {
                var send_data = {};
                jQuery.each(request_data, function(d, val){
                    send_data[val.name] = val;
                    if(val.name == 'columns'){
                        jQuery.each(val.value, function(k, val){
                            send_data['columns']['value'][k]['search_type'] = table.find('[data-col-name="'+val.data+'"]').data('search-type');
                        });
                    }
                });

                send_data = JSON.stringify(send_data);

                var custom_data = {};
                if(table.data('before')){
                    var functions = table.data('before');
                    jQuery.each(functions, function(key,f) {
                        window[f](table,custom_data);
                    });
                }

                if(table.data('loader')){
                    jQuery.loader({
                        className:"blue-with-image-2",
                        content: translations.please_wait
                    });
                }

                settings.jqXHR = jQuery.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": table.data('url'),
                    "data": {data: send_data, custom_data: custom_data['custom_data']},
                    "success":function(data) {
                        fnCallback(data);
                        table.insertDatatableData(data);
                        jQuery("#jquery-loader-background").remove();
                        jQuery("#jquery-loader").remove();
                    },
                    "error":function() {
                        if(table.data('loader')){
                            jQuery("#jquery-loader-background").remove();
                            jQuery("#jquery-loader").remove();
                        }
                    }
                });
            }
        }

        var filter = table.DataTable(datatable_configuration);

        if(table.data('filter') && table.data('server') && table.find('.sp-table-header').length > 0){
            var filterData = [];

            table.setFilterHtml();

            table.find('.sp-table-header').children('th').each(function(index){
                var prop = {};
                if(jQuery(this).data("col-filter")){
                    prop['column_number'] = index;
                    prop['filter_type'] = jQuery(this).data('filter-type');
                    prop['filter_container_id'] = 'sp-filter-holder-'+index;
                    prop['style_class'] = 'form-control';
                    if(jQuery(this).data("filter-type") == 'multi_select'){
                        prop['select_type'] = 'select2';
                    }
                    else if(jQuery(this).data("filter-type") == 'range_date'){
                        prop['date_format'] = jQuery(this).data('date-format');
                    }
                    else if(jQuery(this).data("filter-type") == 'range_number'){
                        if(jQuery(this).data('ignore-char')){
                            prop['ignore_char'] = jQuery(this).data('ignore-char');
                        }
                        prop['column_data_type'] = 'rendered_html';
                        prop['html_data_type'] = 'text';
                        if(jQuery(this).data('filter-plugin-options')){
                            prop['filter_plugin_options'] = {step:jQuery(this).data('filter-plugin-options')};
                        }
                    }

                    if(jQuery(this).data("filter-type") == 'multi_select' || jQuery(this).data("filter-type") == 'select'){
                        prop['data'] = jQuery(this).data('list');
                        prop['filter_default_label'] = translations.select_value;
                    }
                    filterData.push(prop);
                }
            });

            if(table.data('filter-buttons')){

                yadcf.init(filter, filterData, {externally_triggered: true});

                table.setYFilterHtmlButtons();

                table.parent().parent().on('click','[data-action="filter-submit"]',function() {
                    yadcf.exFilterExternallyTriggered(filter);
                });

                table.parent().parent().on('click','[data-action="filter-reset"]',function() {
                    yadcf.exResetAllFilters(filter);
                });
            }
            else{
                yadcf.init(filter, filterData);
            }
        }

        table.parent().addClass('table-responsive');

        if(jQuery('.dataTables_filter input').length){
            jQuery('.dataTables_filter input').addClass('form-control').attr('placeholder',translations.quick_search);
        }
        if(jQuery('.dataTables_length select').length){
            jQuery('.dataTables_length select').addClass('form-control');
        }
        if(table.data('export')){
            jQuery('.dt-button').addClass('btn').addClass('btn-default').addClass('btn-sm');
        }

        return true;
    }
});