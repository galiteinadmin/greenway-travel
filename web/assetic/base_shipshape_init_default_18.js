/**
 * Prepare multiselect post
 * @param select
 * @returns {Array}
 */
/*function serealizeSelects (select)
 {
 var array = [];
 select.each(function(){ array.push(jQuery(this).val()) });
 return array[0];
 }*/

/**
 * Reset form
 * @param form
 * @returns {boolean}
 */
/*function resetFrom(form){
 form[0].reset();
 form.formValidation('resetForm', true);
 form.formValidation('disableSubmitButtons', false);
 return true;
 }*/

var selectList = [];

function addRemoveFromSelectArray(item,state){
    if(state){
        selectList.push(item);
    }
    else{
        selectList = jQuery.grep(selectList, function(value) {
            return value != item;
        });
    }
    return false;
}

/**
 * Select item  selectItem(Query(this));
 * @param element
 * @returns {boolean}
 */
function selectItem(element){
    if(element.children().hasClass('glyphicon-unchecked')){
        element.attr('title',element.data('title-2'));
        addRemoveFromSelectArray(element.data('id'),true);
    }
    else{
        addRemoveFromSelectArray(element.data('id'),false);
        element.attr('title',element.data('title-1'));
    }
    element.children().toggleClass('glyphicon-check').toggleClass('glyphicon-unchecked');
    element.tooltip('fixTitle');
    element.tooltip('show');

    if(selectList.length > 0){
        jQuery('[data-action="mass-action-wrapper"]').show();
    }
    else{
        jQuery('[data-action="mass-action-wrapper"]').hide();
    }

    return false;
}

/**
 * Select2format
 * @param item
 * @returns {string}
 */
function select2FormatResult(item) {
    var markup = "<li data-id='"+item.id+"' class='sp-select-2-result'>"+item.text+"<li>";
    return markup;
}

jQuery(document).ready(function() {

    /**
     * Execute on all pages
     */
    jQuery('[data-toggle="tooltip"]').tooltip();

    /**
     * Multiselect
     */
    jQuery('[data-action="multi-select2"]').multiSelect();

    /**
     * Bootstrap checkbox
     */
    jQuery('[data-type="bchackbox"]').bootstrapSwitch();

    /**
     * Info popover
     */
    jQuery('[rel="popover"]').popover({ trigger: "hover" });

    /**
     * Mass action choose
     */
    jQuery('[data-action="mass-action-select"]').bind('click',function(){
        jQuery('[data-action="mass-action-select"]').removeClass('sp-selected');
        jQuery(this).addClass('sp-selected');
    });

    /**
     * Mass action submit
     */
    jQuery('[data-action="mass-action-submit"]').bind('click',function(){

        if(jQuery('.sp-selected[data-action="mass-action-select"]').length < 1){
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.select_action
            });
            return false;
        }

        var action = jQuery('.sp-selected[data-action="mass-action-select"]').data('action-id');

        jQuery.post(jQuery(this).data("url"), { ids: selectList, action: action }, function(result) {
            if(result.error == false){
                jQuery.growl.notice({
                    title: result.title,
                    message: result.message
                });

                jQuery('[data-action="mass-action-select"]').removeClass('sp-selected');
                jQuery('[data-action="mass-action-wrapper"]').hide();

                window[jQuery('[data-action="mass-action-submit"]').data("callback")]();
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");

        return false;
    });

    /**
     * Multi select
     */
    jQuery('.datatables').on('click','[data-action="toggle-select"]',function() {
        selectItem(jQuery(this));
    });

    /**
     * Stop propagation on dropdown toggle
     */
    jQuery('body').on('click', '.dropdown-toggle', function(e){
        e.stopPropagation();
        jQuery(this).dropdown("toggle");
    });

    jQuery('body').on('click', '.sp-datatable-left-button', function(e){
        e.stopPropagation();
    });

    if(jQuery('[data-action="form"]').length){
        jQuery('[data-action="form"]').initializeValidation();
    }

    /**
     * Standard grid actions
     */
    jQuery('.datatables').on('click', '[data-action="standard_grid_action"]', function(e){

        var table = jQuery(this).parents('.datatables');
        jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id') }, function(result) {
            if(result.error == false){
                jQuery.growl.notice({
                    title: result.title,
                    message: result.message
                });

                table.DataTable().ajax.reload(null, false);
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");
    });

    /**
     * Standard edit modal form
     */
    jQuery('.datatables').on('click', '[data-action="standard_edit_modal_form"]', function(e){
        e.stopPropagation();
        jQuery.post(jQuery(this).data("url"), { id: jQuery(this).data('id') }, function(result) {
            if(result.error == false){
                var clone = jQuery('#modal-container').clone(true, true).appendTo(jQuery('footer'));
                clone.html(result.html);
                var modal = clone.find('.modal');
                modal.modal('show');

                var form = modal.find('[data-action="form"]');
                form.initializeValidation();
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");
    });

    /**
     * Standard add modal form
     */
    jQuery('[data-action="standard_add_modal_form"]').bind('click', function(e){
        e.stopPropagation();
        jQuery.post(jQuery(this).data("url"), { }, function(result) {
            if(result.error == false){
                var clone = jQuery('#modal-container').clone(true, true).appendTo(jQuery('footer'));
                clone.html(result.html);
                var modal = clone.find('.modal');
                modal.modal('show');

                var form = modal.find('[data-action="form"]');
                form.initializeValidation();
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");
    });

    /**
     * Modal preview
     */
    jQuery('body').on('click','[data-action="modal_preview"]', function(e){
        jQuery.post(jQuery(this).data("url"), { id:jQuery(this).data('id') }, function(result) {
            if(result.error == false){
                var clone = jQuery('#modal-container').clone(true, true).appendTo(jQuery('footer'));
                clone.html(result.html);
                var modal = clone.find('.modal');
                modal.modal('show');
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        }, "json");
    });

});