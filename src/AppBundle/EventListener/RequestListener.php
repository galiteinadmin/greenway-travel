<?php

namespace AppBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class RequestListener implements EventSubscriberInterface
{
    private $tokenStorage;
    private $defaultLocale;

    public function __construct(TokenStorageInterface $tokenStorage, $defaultLocale = 'hr')
    {
        $this->tokenStorage = $tokenStorage;
        $this->defaultLocale = $defaultLocale;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($event->isMasterRequest()) {

            $request = $event->getRequest();

            if (!$request->hasPreviousSession() || !$request->getSession()->isStarted()) {
                $session = new Session();
                $session->start();
            }

            $token = $this->tokenStorage->getToken();

            if(!isset($token) || empty($token)){
                $request->setLocale($request->getSession()->get('_locale', $this->defaultLocale));
                return;
            }
            else{
                $user = $token->getUser();
                if(!isset($user) || empty($user) || $user == "anon."){
                    $request->setLocale($request->getSession()->get('_locale', $this->defaultLocale));
                    return;
                }
            }

            return;
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            // must be registered after the default Locale listener
            KernelEvents::REQUEST => array(array('onKernelRequest',7)),
        );
    }
}