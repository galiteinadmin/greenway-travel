/**
 * Charts
 */

function renderPriceHistoryChart(data_web_price,data_regular_price){
    var chart_data_web_price = [];
    var chart_data_regular_price = [];

    jQuery.each(data_web_price,function(index, data_array){
        chart_data_web_price[index] =
        {
            x: new Date(data_array.x),
            y: data_array.y,
            label: data_array.legendText
        }
    });

    jQuery.each(data_regular_price,function(index, data_array){
        chart_data_regular_price[index] =
        {
            x: new Date(data_array.x),
            y: data_array.y-15,
            label: data_array.legendText
        }
    });

    jQuery('#modal-container').find('#price-history-chart').CanvasJSChart({
        animationEnabled: true,
        exportEnabled: true,
        title: {
            //text: res.title,
            fontSize: 26,
            wrap: true,
            padding: 20
        },
        legend :{
            verticalAlign: "center",
            horizontalAlign: "right"
        },
        axisY:{
            includeZero: false
        },
        axisX: {
            valueFormatString: "MMM",
            interval:1,
            intervalType: "month"
        },
        data: [
            {
                type: "line",
                name: translations.web_price,
                showInLegend: true,
                toolTipContent: "{label} <br/> {y}",
                dataPoints: chart_data_web_price
            },
            {
                type: "line",
                name: translations.regular_price,
                showInLegend: true,
                toolTipContent: "{label} <br/> {y}",
                dataPoints: chart_data_regular_price
            }
        ],
    });
}

/**
 * Share of the number of x models per brand and partner
 * @param date_from
 * @param date_to
 * @returns {boolean}
 */
function renderShare1Chart(date_from,date_to){

    jQuery.post(jQuery("#chart-share1").data("post-url"), { date_from: date_from, date_to: date_to }, function(res) {
        if(res.error == false){

            var chart_data = [];

            jQuery.each(res.data,function(index, data_array){
                chart_data[index] =
                {
                    type: "stackedColumn100",
                    indexLabelPlacement: "inside",
                    indexLabelFontColor: "white",
                    indexLabelFontSize: 12,
                    percentFormatString: "#.#",
                    indexLabel: "#percent %",
                    name: data_array.name,
                    showInLegend: true,
                    dataPoints: data_array.data
                }
            });

            jQuery("#chart-share1").CanvasJSChart({
                animationEnabled: true,
                exportEnabled: true,
                title: {
                    text: res.title,
                    fontSize: 26,
                    wrap: true,
                    padding: 20
                },
                axisY: {
                    gridThickness: 1,
                    tickThickness: 1,
                    lineThickness: 1,
                    title: translations.percentage
                },
                axisX: {
                    lineThickness: 1
                },
                toolTip: {
                    //shared: true,
                    content: "{label} <br/>{name} : {y} "+translations.models
                },
                data: chart_data,
                legend: {
                    cursor: "pointer",
                    itemclick: function (e) {
                        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                            e.dataSeries.visible = false;
                        } else {
                            e.dataSeries.visible = true;
                        }

                        e.chart.render();
                    }
                }
            });
        }
        else{
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    }, "json");

    return false;
}

/**
 * Share of the number of x models per brand and partner
 * @param date_from
 * @param date_to
 * @returns {boolean}
 */
function renderBrandPriceRatioPerPartnerChart(date_from,date_to){

    jQuery.post(jQuery("#brand_price_ratio_per_partner_chart").data("post-url"), { date_from: date_from, date_to: date_to }, function(res) {
        if(res.error == false){

            var chart_data = [];

            jQuery.each(res.data,function(index, data_array){
                chart_data[index] =
                {
                    type: "stackedColumn100",
                    indexLabelPlacement: "inside",
                    indexLabelFontColor: "white",
                    indexLabelFontSize: 12,
                    percentFormatString: "#.#",
                    indexLabel: "#percent %",
                    name: data_array.name,
                    showInLegend: true,
                    dataPoints: data_array.data
                }
            });

            jQuery("#brand_price_ratio_per_partner_chart").CanvasJSChart({
                animationEnabled: true,
                exportEnabled: true,
                title: {
                    text: res.title,
                    fontSize: 26,
                    wrap: true,
                    padding: 20
                },
                axisY: {
                    gridThickness: 1,
                    tickThickness: 1,
                    lineThickness: 1,
                    title: translations.percentage
                },
                axisX: {
                    lineThickness: 1
                },
                toolTip: {
                    //shared: true,
                    content: "{label} <br/>{name} : {y} "+translations.models
                },
                data: chart_data,
                legend: {
                    cursor: "pointer",
                    itemclick: function (e) {
                        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                            e.dataSeries.visible = false;
                        } else {
                            e.dataSeries.visible = true;
                        }

                        e.chart.render();
                    }
                }
            });
        }
        else{
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    }, "json");

    return false;
}

/**
 * Total share in market
 * @param date_from
 * @param date_to
 * @returns {boolean}
 */
function renderShare2Chart(date_from,date_to){

    jQuery.post(jQuery("#chart-share2").data("post-url"), { date_from: date_from, date_to: date_to }, function(res) {
        if(res.error == false){

            var chart_data = res.data;

            jQuery("#chart-share2").CanvasJSChart({
                animationEnabled: true,
                exportEnabled: true,
                title: {
                    text: res.title,
                    fontSize: 26,
                    wrap: true,
                    padding: 20
                },
                legend :{
                    verticalAlign: "center",
                    horizontalAlign: "right"
                },
                data: [
                    {
                        type: "pie",
                        showInLegend: true,
                        toolTipContent: "{label} <br/> {y} "+translations.models,
                        percentFormatString: "#.#",
                        indexLabel: "#percent %",
                        dataPoints: chart_data
                    }
                ],
            });
        }
        else{
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    }, "json");

    return false;
}

/**
 * Products on promotion per partner
 * @param date_from
 * @param date_to
 * @returns {boolean}
 */
function renderShare3Chart(date_from,date_to){

    jQuery.post(jQuery("#chart-share3").data("post-url"), { date_from: date_from, date_to: date_to }, function(res) {
        if(res.error == false){


            var chart_data = [];

            jQuery.each(res.data,function(index, data_array){
                chart_data[index] =
                {
                    type: "stackedColumn100",
                    indexLabelPlacement: "inside",
                    indexLabelFontColor: "white",
                    indexLabelFontSize: 12,
                    percentFormatString: "#.#",
                    indexLabel: "#percent %",
                    name: data_array.name,
                    showInLegend: true,
                    dataPoints: data_array.data
                }
            });

            jQuery("#chart-share3").CanvasJSChart({
                animationEnabled: true,
                exportEnabled: true,
                title: {
                    text: res.title,
                    fontSize: 22,
                    wrap: true,
                    padding: 20
                },
                axisY: {
                    gridThickness: 1,
                    tickThickness: 1,
                    lineThickness: 1,
                    title: translations.percentage
                },
                axisX: {
                    lineThickness: 1
                },
                toolTip: {
                    //shared: true,
                    content: "{label} <br/>{name} : {y} "+translations.models
                },
                data: chart_data,
                legend: {
                    cursor: "pointer",
                    itemclick: function (e) {
                        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                            e.dataSeries.visible = false;
                        } else {
                            e.dataSeries.visible = true;
                        }

                        e.chart.render();
                    }
                }
            });

          /*  var chart_data = res.data;

            jQuery("#chart-share3").CanvasJSChart({
                animationEnabled: true,
                exportEnabled: true,
                title: {
                    text: res.title,
                    fontSize: 26,
                    wrap: true,
                    padding: 20
                },
                legend :{
                    verticalAlign: "center",
                    horizontalAlign: "right"
                },
                data: [
                    {
                        type: "pie",
                        showInLegend: true,
                        toolTipContent: "{label} <br/> {y} "+translations.models,
                        percentFormatString: "#.#",
                        indexLabel: "#percent %",
                        dataPoints: chart_data
                    }
                ],
            });*/
        }
        else{
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    }, "json");

    return false;
}

/**
 * Min price share of products per partner
 * @param date_from
 * @param date_to
 * @returns {boolean}
 */
function renderShare4Chart(date_from,date_to){

    jQuery.post(jQuery("#chart-share4").data("post-url"), { date_from: date_from, date_to: date_to }, function(res) {
        if(res.error == false){

            var chart_data = res.data;

            jQuery("#chart-share4").CanvasJSChart({
                animationEnabled: true,
                exportEnabled: true,
                title: {
                    text: res.title,
                    fontSize: 26,
                    wrap: true,
                    padding: 20
                },
                legend :{
                    verticalAlign: "center",
                    horizontalAlign: "right"
                },
                data: [
                    {
                        type: "pie",
                        showInLegend: true,
                        toolTipContent: "{label} <br/> {y} "+translations.models,
                        percentFormatString: "#.#",
                        indexLabel: "#percent %",
                        dataPoints: chart_data
                    }
                ],
            });
        }
        else{
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    }, "json");

    return false;
}

/**
 * Share of the number of %name% models on promotion per partner
 * @param date_from
 * @param date_to
 * @returns {boolean}
 */
function renderShare5Chart(date_from,date_to){

    jQuery.post(jQuery("#chart-share5").data("post-url"), { date_from: date_from, date_to: date_to }, function(res) {
        if(res.error == false){

            var chart_data = res.data;

            jQuery("#chart-share5").CanvasJSChart({
                animationEnabled: true,
                exportEnabled: true,
                title: {
                    text: res.title,
                    fontSize: 26,
                    wrap: true,
                    padding: 20
                },
                legend :{
                    verticalAlign: "center",
                    horizontalAlign: "right"
                },
                data: [
                    {
                        type: "pie",
                        showInLegend: true,
                        toolTipContent: "{label} <br/> {y} "+translations.models,
                        percentFormatString: "#.#",
                        indexLabel: "#percent %",
                        dataPoints: chart_data
                    }
                ],
            });
        }
        else{
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    }, "json");

    return false;
}

/**
 * Share of the number of %name% models on promotion per brand
 * @param date_from
 * @param date_to
 * @returns {boolean}
 */
function renderShare6Chart(date_from,date_to){

    jQuery.post(jQuery("#chart-share6").data("post-url"), { date_from: date_from, date_to: date_to }, function(res) {
        if(res.error == false){

            var chart_data = res.data;

            jQuery("#chart-share6").CanvasJSChart({
                animationEnabled: true,
                exportEnabled: true,
                title: {
                    text: res.title,
                    fontSize: 26,
                    wrap: true,
                    padding: 20
                },
                legend :{
                    verticalAlign: "center",
                    horizontalAlign: "right"
                },
                data: [
                    {
                        type: "pie",
                        showInLegend: true,
                        toolTipContent: "{label} <br/> {y} "+translations.models,
                        percentFormatString: "#.#",
                        indexLabel: "#percent %",
                        dataPoints: chart_data
                    }
                ],
            });
        }
        else{
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    }, "json");

    return false;
}

/**
 * Primjer
 * @param date_from
 * @param date_to
 * @returns {boolean}
 */
function renderShareCompetitiveAvg(date_from,date_to){

    jQuery.post(jQuery("#chart-competitive-avg").data("post-url"), { date_from: date_from, date_to: date_to }, function(res) {
        if(res.error == false){

            if(!res.show){
                return false;
            }

            var chart_data = res.data;
            var line_data = res.partner_avg_price;

            jQuery("#chart-competitive-avg").CanvasJSChart({
                animationEnabled: true,
                exportEnabled: true,
                title: {
                    text: res.title,
                    fontSize: 22,
                    wrap: true,
                    padding: 20
                },
                legend :{
                    verticalAlign: "center",
                    horizontalAlign: "right"
                },
                axisY:{
                    stripLines:[
                        {
                            showOnTop: true,
                            value:res.avg,
                        }
                    ]
                },
                data: [
                    {
                        type: "column",
                        showInLegend: false,
                        dataPoints: chart_data
                    },
                    {
                        type: "line",
                        showInLegend: false,
                        toolTipContent: "{label}: {y}",
                        dataPoints: line_data
                    }
                ],
            });
        }
        else{
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    }, "json");

    return false;
}

/**
 * @param date_from
 * @param date_to
 * @returns {boolean}
 */
function renderShareCompetitiveMin(date_from,date_to){

    jQuery.post(jQuery("#chart-competitive-min").data("post-url"), { date_from: date_from, date_to: date_to }, function(res) {
        if(res.error == false){

            if(!res.show){
                return false;
            }

            var chart_data = res.data;
            var line_data = res.partner_min_price;

            jQuery("#chart-competitive-min").CanvasJSChart({
                animationEnabled: true,
                exportEnabled: true,
                title: {
                    text: res.title,
                    fontSize: 22,
                    wrap: true,
                    padding: 20
                },
                legend :{
                    verticalAlign: "center",
                    horizontalAlign: "right"
                },
                axisY:{
                    stripLines:[
                        {
                            showOnTop: true,
                            value:res.min,
                        }
                    ]
                },
                data: [
                    {
                        type: "column",
                        showInLegend: false,
                        dataPoints: chart_data
                    },
                    {
                        type: "line",
                        showInLegend: false,
                        toolTipContent: "{label}: {y}",
                        dataPoints: line_data
                    }
                ],
            });
        }
        else{
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    }, "json");

    return false;
}



/**
 * @param date_from
 * @param date_to
 * @returns {boolean}
 */
function renderDistribution1Chart(date_from,date_to){

    jQuery.post(jQuery("#chart-distribution-1").data("post-url"), { date_from: date_from, date_to: date_to }, function(res) {
        if(res.error == false){

            var chart_data = res.data;

            jQuery("#chart-distribution-1").CanvasJSChart({
                animationEnabled: true,
                exportEnabled: true,
                title: {
                    text: res.title,
                    fontSize: 22,
                    wrap: true,
                    padding: 20
                },
                legend :{
                    verticalAlign: "center",
                    horizontalAlign: "right"
                },
                data: [
                    {
                        type: "column",
                        showInLegend: false,
                        dataPoints: chart_data
                    }
                ],
            });
        }
        else{
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    }, "json");

    return false;
}

/**
 * @param date_from
 * @param date_to
 * @returns {boolean}
 */
function renderPriceByMonth(year){

    jQuery.post(jQuery("#chart-price-by-month").data("post-url"), { year: year }, function(res) {
        if(res.error == false){

            var column_data = res.column_data;
            var line_data = res.line_data

            jQuery("#chart-price-by-month").CanvasJSChart({
                animationEnabled: true,
                exportEnabled: true,
                title: {
                    text: res.title,
                    fontSize: 22,
                    wrap: true,
                    padding: 20
                },
                axisY: {
                    title: translations.price,
                    includeZero: false
                },
                axisX: {
                    //interval: 1,
                    showOnTop: true
                },
                data: [
                    {
                        type: "column",
                        showInLegend: false,
                        toolTipContent: "{label}: {y}",
                        dataPoints: column_data
                    },
                    {
                        type: "line",
                        showInLegend: false,
                        toolTipContent: "{label}: {y}",
                        dataPoints: line_data
                    }
                ]
            });
        }
        else{
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    }, "json");

    return false;
}



/**
 * Products on promotion table
 * @returns {boolean}
 */
function topPromotion(){

    jQuery.post(jQuery('[data-action="promotion-wrapper"]').data('url'), function(result) {
        if(result.error == true){
            jQuery.growl.error({
                title: translations.error_message,
                message: result.message
            });
        }
        else if(result.error == false){
            if ( $.fn.DataTable.fnIsDataTable( jQuery('#promotion_table') ) ) {
                jQuery('#promotion_table').DataTable().destroy();
            }

            jQuery('#promotion_table').html(result.html);
            jQuery('#promotion_table').find('[data-toggle="tooltip"]').tooltip();

            /**
             * search, export
             */
            createDatatable(jQuery('#promotion_table'), 5, false, false, false, false, false, false, true, false);
        }
        else{
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    }, 'json');

    return true;
}

/**
 * Products on crashed promotion table
 * @returns {boolean}
 */
function crashedPromotion(){

    jQuery.post(jQuery('[data-action="crashed-promotion-wrapper"]').data('url'), function(result) {
        if(result.error == true){
            jQuery.growl.error({
                title: translations.error_message,
                message: result.message
            });
        }
        else if(result.error == false){
            if ( $.fn.DataTable.fnIsDataTable( jQuery('#crashed_promotion_table') ) ) {
                jQuery('#crashed_promotion_table').DataTable().destroy();
            }

            jQuery('#crashed_promotion_table').html(result.html);
            jQuery('#crashed_promotion_table').find('[data-toggle="tooltip"]').tooltip();

            /**
             * search, export
             */
            createDatatable(jQuery('#crashed_promotion_table'), 5, false, false, false, false, false, false, true, false);
        }
        else{
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    }, 'json');

    return true;
}

/**
 * Cheaper models in competitive groups
 * @returns {boolean}
 */
function cheaperModels(){

    jQuery.post(jQuery('[data-action="cheaper-wrapper"]').data('url'), function(result) {
        if(result.error == true){
            jQuery.growl.error({
                title: translations.error_message,
                message: result.message
            });
        }
        else if(result.error == false){
            if ( $.fn.DataTable.fnIsDataTable( jQuery('#cheaper_table') ) ) {
                jQuery('#cheaper_table').DataTable().destroy();
            }

            jQuery('#cheaper_table').html(result.html);
            jQuery('#cheaper_table').find('[data-toggle="tooltip"]').tooltip();

            /**
             * search, export
             */
            createDatatable(jQuery('#cheaper_table'), 5, false, true, false, false, false, 0, 'desc', false, false);
        }
        else{
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    }, 'json');

    return true;
}

/**
 * Exclusive models for partner
 * @returns {boolean}
 */
function exclusiveModels(){

    jQuery.post(jQuery('[data-action="exclusive-wrapper"]').data('url'), function(result) {
        if(result.error == true){
            jQuery.growl.error({
                title: translations.error_message,
                message: result.message
            });
        }
        else if(result.error == false){
            if ( $.fn.DataTable.fnIsDataTable( jQuery('#exclusive_table') ) ) {
                jQuery('#exclusive_table').DataTable().destroy();
            }

            jQuery('#exclusive_table').html(result.html);
            jQuery('#exclusive_table').find('[data-toggle="tooltip"]').tooltip();

            /**
             * search, export
             */
            createDatatable(jQuery('#exclusive_table'), 5, false, false, false, false, 0, 'desc', false, false);
        }
        else{
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    }, 'json');

    return true;
}

/**
 * Dashboard alerts
 * @returns {boolean}
 */
function getDashboardAlerts(){

    jQuery.post(jQuery('[data-action="alerts-wrapper"]').data('url'), function(result) {
        if(result.error == true){
            jQuery.growl.error({
                title: translations.error_message,
                message: result.message
            });
        }
        else if(result.error == false){
            if ( $.fn.DataTable.fnIsDataTable( jQuery('#alerts_table') ) ) {
                jQuery('#alerts_table').DataTable().destroy();
            }

            jQuery('#alerts_table').html(result.html);
            jQuery('#alerts_table').find('[data-toggle="tooltip"]').tooltip();

            /**
             * search, export
             */
            createDatatable(jQuery('#alerts_table'), 5, false, false, false, false, false, false, true, false);
        }
        else{
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    }, 'json');

    return true;
}

/**
 * Dashboard wrong descriptions
 * @returns {boolean}
 */
function getProductsWithWtrongDescription(){

    jQuery.post(jQuery('[data-action="wrong-descriptions-wrapper"]').data('url'), function(result) {
        if(result.error == true){
            jQuery.growl.error({
                title: translations.error_message,
                message: result.message
            });
        }
        else if(result.error == false){
            if ( $.fn.DataTable.fnIsDataTable( jQuery('#wrong_descriptions_table') ) ) {
                jQuery('#wrong_descriptions_table').DataTable().destroy();
            }

            jQuery('#wrong_descriptions_table').html(result.html);
            jQuery('#wrong_descriptions_table').find('[data-toggle="tooltip"]').tooltip();

            /**
             * search, export
             */
            createDatatable(jQuery('#wrong_descriptions_table'), 10, false, false, false, false, false, false, true, false);
        }
        else{
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    }, 'json');

    return true;
}

/**
 * Dashboard tasks
 * @returns {boolean}
 */
function getDashboardTasks(){

    jQuery.post(jQuery('[data-action="tasks-wrapper"]').data('url'), function(result) {
        if(result.error == true){
            jQuery.growl.error({
                title: translations.error_message,
                message: result.message
            });
        }
        else if(result.error == false){
            if ( $.fn.DataTable.fnIsDataTable( jQuery('#tasks_table') ) ) {
                jQuery('#tasks_table').DataTable().destroy();
            }

            jQuery('#tasks_table').html(result.html);
            jQuery('#tasks_table').find('[data-toggle="tooltip"]').tooltip();

            /**
             * search, export
             */
            createDatatable(jQuery('#tasks_table'),50,false,false,false,false,false,false,true,false);
        }
        else{
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    }, 'json');

    return true;
}

/**
 * render calendar
 * @param headertype
 */
function renderCalendar(headertype,dates) {

    var calendar = jQuery('[data-action="tasks-calendar-wrapper"]').fullCalendar({
        header: headertype,
        selectable: false,
        selectHelper: true,
        /*select: function(start, end, allDay) {
         var title = prompt('Event Title:');
         if (title) {
         calendar.fullCalendar('renderEvent',
         {
         title: title,
         start: start,
         end: end,
         allDay: allDay
         },
         true // make the event "stick"
         );
         }
         calendar.fullCalendar('unselect');
         },*/
        editable: false,
        events: dates,
        buttonText: {
            prev: '<i class="fa fa-angle-left"></i>',
            next: '<i class="fa fa-angle-right"></i>',
            prevYear: '<i class="fa fa-angle-double-left"></i>',  // <<
            nextYear: '<i class="fa fa-angle-double-right"></i>',  // >>
            today:    'Today',
            month:    'Month',
            week:     'Week',
            day:      'Day'
        }
    });
}

/**
 * Dashboard tasks
 * @returns {boolean}
 */
function getDashboardTasksCalendar(){

    jQuery.post(jQuery('[data-action="tasks-calendar-wrapper"]').data('url'), function(result) {
        if(result.error == true){
            jQuery.growl.error({
                title: translations.error_message,
                message: result.message
            });
        }
        else if(result.error == false){

            /**
             * CALENDAR
             */
            renderCalendar({left: 'title',right: 'prev,next'},result.dates);

            enquire.register("screen and (min-width: 1200px)", {
                match : function() {
                    jQuery('[data-action="tasks-calendar-wrapper"]').removeData('fullCalendar').empty();
                    renderCalendar({left: 'prev,next',center: 'title',right: 'month,basicWeek,basicDay'},result.dates);
                },
                unmatch : function() {
                    jQuery('[data-action="tasks-calendar-wrapper"]').removeData('fullCalendar').empty();
                    renderCalendar({left: 'title',right: 'prev,next'},result.dates);
                }
            });

            //jQuery('#tasks_table').find('[data-toggle="tooltip"]').tooltip();
        }
        else{
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    }, 'json');

    return true;
}

/**
 * Get competitive groups
 */
function getCompetitiveGroups(){
    jQuery.ajax({
        type: "GET",
        url: jQuery('[data-action="competitive-group-change"]').data("url"),
        success: function(data, dataType)
        {
            if(data.error == false){
                jQuery('[data-action="competitive-group-change"]').html(data.html);
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown)
        {
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    });
}

/**
 * Get brands
 */
function getBrands(){
    jQuery.ajax({
        type: "GET",
        url: jQuery('[data-action="brand-change"]').data("url"),
        success: function(data, dataType)
        {
            if(data.error == false){
                jQuery('[data-action="brand-change"]').html(data.html);
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown)
        {
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    });
}

/**
 * Get partners
 */
function getPartners(){
    jQuery.ajax({
        type: "GET",
        url: jQuery('[data-action="partner-change"]').data("url"),
        success: function(data, dataType)
        {
            if(data.error == false){
                jQuery('[data-action="partner-change"]').html(data.html);
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown)
        {
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    });
}

/**
 * Get num of
 */
function getNumOf(){
    jQuery.ajax({
        type: "GET",
        url: jQuery('[data-action="num-of"]').data("url"),
        success: function(data, dataType)
        {
            if(data.error == false){
                jQuery('[data-action="num-of"]').html(data.html);
            }
            else{
                jQuery.growl.error({
                    title: translations.error_message,
                    message: result.message
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown)
        {
            jQuery.growl.error({
                title: translations.error_message,
                message: translations.there_has_been_an_error_please_try_again
            });
        }
    });
}

/**
 * Chart reloading
 */
function loadDashboardCharts(){
    var start_date = jQuery('#daterangepicker2').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var end_date = jQuery('#daterangepicker2').data('daterangepicker').endDate.format('YYYY-MM-DD');
    var start_year = jQuery('#daterangepicker2').data('daterangepicker').startDate.format('YYYY');

    getNumOf();
    getCompetitiveGroups();
    getBrands();
    getPartners();
    renderShare1Chart(start_date,end_date);
    renderShare2Chart(start_date,end_date);
    renderShare3Chart(start_date,end_date);
    renderShare4Chart(start_date,end_date);
    renderShare5Chart(start_date,end_date);
    renderShare6Chart(start_date,end_date);
    renderPriceByMonth(start_year);
    renderBrandPriceRatioPerPartnerChart(start_date,end_date);
    renderShareCompetitiveMin(start_date,end_date);
    renderShareCompetitiveAvg(start_date,end_date);
    renderDistribution1Chart(start_date,end_date);
    topPromotion();
    crashedPromotion();
    cheaperModels();
    exclusiveModels();
}

function loadModelsCharts(){
    var start_date = jQuery('#daterangepicker2').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var end_date = jQuery('#daterangepicker2').data('daterangepicker').endDate.format('YYYY-MM-DD');

    renderShare1Chart(start_date,end_date);
    renderShare2Chart(start_date,end_date);
    exclusiveModels();
}

function loadPricingCharts(){
    var start_date = jQuery('#daterangepicker2').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var end_date = jQuery('#daterangepicker2').data('daterangepicker').endDate.format('YYYY-MM-DD');
    var start_year = jQuery('#daterangepicker2').data('daterangepicker').startDate.format('YYYY');

    getBrands();
    getPartners();
    renderPriceByMonth(start_year);
    renderShare6Chart(start_date,end_date);
    renderBrandPriceRatioPerPartnerChart(start_date,end_date);
}

function loadPromotionCharts(){
    var start_date = jQuery('#daterangepicker2').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var end_date = jQuery('#daterangepicker2').data('daterangepicker').endDate.format('YYYY-MM-DD');

    renderShare3Chart(start_date,end_date);
    renderShare4Chart(start_date,end_date);
    renderShare5Chart(start_date,end_date);
    topPromotion();
    crashedPromotion();
}

function loadCompetitiveGroupsCharts(){
    var start_date = jQuery('#daterangepicker2').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var end_date = jQuery('#daterangepicker2').data('daterangepicker').endDate.format('YYYY-MM-DD');

    getCompetitiveGroups();
    renderShareCompetitiveMin(start_date,end_date);
    renderShareCompetitiveAvg(start_date,end_date);
    cheaperModels();
}

function loadDistributionCharts(){
    var start_date = jQuery('#daterangepicker2').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var end_date = jQuery('#daterangepicker2').data('daterangepicker').endDate.format('YYYY-MM-DD');

    renderDistribution1Chart(start_date,end_date);
}

/**
 * Choose which page to reload
 */
function reloadPage(){
    /**
     * Potencijalno moze biti tu
     */
    /*var start_date = jQuery('#daterangepicker2').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var end_date = jQuery('#daterangepicker2').data('daterangepicker').endDate.format('YYYY-MM-DD');*/

    getProductsWithWtrongDescription();

    if(jQuery('#page-dashboard').length > 0){
        loadDashboardCharts();
    }
    else if(jQuery('#page-analytics-models').length > 0){
        loadModelsCharts();
    }
    else if(jQuery('#page-analytics-pricing').length > 0){
        loadPricingCharts();
    }
    else if(jQuery('#page-analytics-promotion').length > 0){
        loadPromotionCharts();
    }
    else if(jQuery('#page-analytics-competitive-groups').length > 0){
        loadCompetitiveGroupsCharts();
    }
    else if(jQuery('#page-analytics-distribution').length > 0){
        loadDistributionCharts();
    }
}

jQuery(document).ready(function() {

    //reloadPage();

    if(jQuery('#page-dashboard').length > 0) {

        //getDashboardAlerts();
        //getDashboardTasks();
        //getDashboardTasksCalendar();

        /**
         * Download jednog grafa
         */
        /*jQuery('[data-action="download-graf"]').bind('click',function(e){
            e.preventDefault();
            var type = jQuery(this).data('download-type');
            jQuery(this).parents('.sp-button-group-holder').find('.canvasjs-chart-toolbar > div > div')[type].click();
            return false;
        });*/
    }
});