jQuery.fn.extend({
    standardReload: function(){
        //var table = jQuery('[data-table="'+jQuery(this).data('custom-callback')+'"]').parents('.dataTables_wrapper').find('.datatables');
        var table = jQuery('#table');
        table.DataTable().ajax.reload(null, false);
        return false;
    },
    redirect: function(){
        var redirect_url = jQuery(this).data('redirect-url');
        window.location.replace(redirect_url);
        return false;
    },
    custom: function(){
        var custom_function = jQuery(this).data('custom-callback');
        console.log(custom_function);
        window[custom_function](jQuery(this));
        return false;
    },
    initializeValidation: function(){

        var form = jQuery(this);

        form.find('textarea.autosize').autosize({append: "\n"});
        form.find('[data-action="multi-select2"]').multiSelect();

        /**
         * Date single
         */
        form.find('[data-type="datesingle"]:not([readonly])').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY'
            }
        });

        /**
         * Datetime single
         */
        form.find('[data-type="datetimesingle"]:not([readonly])').daterangepicker({
            autoApply: true,
            singleDatePicker: true,
            showDropdowns: true,
            timePicker: true,
            timePickerIncrement: 15,
            timePicker24Hour: true,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY H:mm'
            }
        });

        /**
         * Date range
         */
        form.find('[data-type="daterange"]:not([readonly])').daterangepicker({
            autoApply: false,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY'
            }
        });

        /**
         * Date range time
         */
        form.find('[data-type="daterangetime"]:not([readonly])').daterangepicker({
            autoApply: false,
            timePicker: true,
            timePickerIncrement: 30,
            timePicker24Hour: true,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY H:mm'
            }
        });

        if(form.find('[data-action="date"]:not([readonly])').data('clear')){
            form.find('[data-action="date"]').val('');
        }
        form.find('[data-action="date"]:not([readonly])').on('cancel.daterangepicker', function(ev, picker) {
            jQuery(this).val('');
        });

        /**
         * Validate form
         */
        form.on('init.field.fv', function(e, data) {
            // data.fv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent    = data.element.parents('.form-group'),
                $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                options    = data.fv.getOptions(),                      // Entire options
                validators = data.fv.getOptions(data.field).validators; // The field validators

            if (validators.notEmpty && options.icon && options.icon.required) {
                // The field uses notEmpty validator
                // Add required icon
                $icon.addClass(options.icon.required).show();
            }
        });

        form.formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'], /*':disabled' , ':hidden' , ':not(:visible)'*/
            icon: {
                required: 'glyphicon glyphicon-asterisk',
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            live: 'enabled',
            message: translations.please_fill_in_the_field_correctly,
            button: {
                selector: 'button[type="submit"]'
            },
            trigger: null
        }).on('status.field.fv', function(e, data) {
            // Remove the required icon when the field updates its status
            var $parent    = data.element.parents('.form-group'),
                $icon      = $parent.find('.form-control-feedback[data-fv-icon-for="' + data.field + '"]'),
                options    = data.fv.getOptions(),                      // Entire options
                validators = data.fv.getOptions(data.field).validators; // The field validators

            if (validators.notEmpty && options.icon && options.icon.required) {
                $icon.removeClass(options.icon.required).addClass('glyphicon');
            }
        }).on('success.field.fv', function(e, data) {
            // e, data parameters are the same as in err.field.fv event handler
            // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
            // - The submit button is clicked
            // - The form is invalid
            data.fv.disableSubmitButtons(false);
        }).on('success.form.fv', function(e, data) {
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = jQuery(e.target);

            // Get the BootstrapValidator instance
            $form.formValidation('disableSubmitButtons', false);
            var fv = $form.data('formValidation');


            jQuery.post($form.attr('action'), $form.serialize(), function(result) {
                if(result.error == true){
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: result.message
                    });
                }
                else if(result.error == false){
                    jQuery.growl.notice({
                        title: result.title,
                        message: result.message
                    });
                    $form.parents('.modal').modal('hide');

                    if($form.data('callback')){
                        var functions = $form.data('callback');
                        jQuery.each(functions, function(key,f) {
                            $form[f]();
                        });
                    }
                }
                else{
                    jQuery.growl.error({
                        title: translations.error_message,
                        message: translations.there_has_been_an_error_please_try_again
                    });
                }
            }, 'json');
        });

        return false;
    }
});