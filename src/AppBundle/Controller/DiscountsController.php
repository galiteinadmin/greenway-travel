<?php

namespace AppBundle\Controller;

use BusinessBundle\DataTable\DataTablePager;
use BusinessBundle\Entity\Discounts;
use BusinessBundle\Entity\GlobalParameters;
use BusinessBundle\Services\CarGroupContext;
use BusinessBundle\Services\DiscountsContext;
use BusinessBundle\Services\LocationsContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use BusinessBundle\Interfaces\BLL;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @Route(service="discounts_controller")
 */
class DiscountsController extends Controller
{

    private $twigEngine;
    private $translator;
    private $discounts_context;
    private $cars_context;

    public function __construct(DiscountsContext $discounts_context, CarGroupContext $cars_context, TwigEngine $twigEngine, TranslatorInterface $translator)
    {
        $this->twigEngine = $twigEngine;
        $this->translator = $translator;
        $this->discounts_context = $discounts_context;
        $this->cars_context = $cars_context;
    }

    /**
     * @Route("/discounts", name="discounts_index")
     */
    public function indexAction(Request $request)
    {
        /*$entities = $this->printer_brand_context->getAll();
        $brands_filter = Array();

        if($entities){
            foreach($entities as $key => $entity){
                $brands_filter[$key]["value"] = "{$entity->getId()}";
                $brands_filter[$key]["label"] = $entity->getName();
            }
            $brands_filter = json_encode($brands_filter);
        }*/

        return new Response($this->twigEngine->render('AppBundle:Discounts:index.html.twig', Array()));
    }

    /**
     * @Route("/discounts/list", name="get_discounts_list")
     * @Method("POST")
     */
    public function getList(Request $request)
    {
        $pager = new DataTablePager();
        $pager->setFromPost(json_decode($_POST["data"]));

        $entities = $this->discounts_context->getItemsWithPaging($pager);

        $html = $this->twigEngine->render('AppBundle:Discounts:list.html.twig', array('entities' => $entities));
        $num_of_items = $this->discounts_context->countAllItems();

        $ret = Array();
        $ret["draw"] = $pager->getDraw();
        $ret["recordsTotal"] = $num_of_items;
        $ret["recordsFiltered"] = $num_of_items;
        $ret["data"] = array();
        $ret["html"] = $html;

        return new JsonResponse($ret);
    }

    /**
     * @Route("/discounts/create", name="discounts_create_form")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $cars = $this->cars_context->getAll();

        $html = $this->twigEngine->render('AppBundle:Discounts:form.html.twig', array('entity' => null, 'cars' => $cars, 'title' => $this->translator->trans('Create discount')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }

    /**
     * @Route("/discounts/update/", name="discounts_update_form")
     * @Method("POST")
     */
    public function updateAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Discount id is not correct')));
        }

        $entity = $this->discounts_context->getById($p["id"]);

        if(!isset($entity) || empty($entity)){
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Discount does not exist')));
        }

        $cars = $this->cars_context->getAll();

        $html = $this->twigEngine->render('AppBundle:Discounts:form.html.twig', array('entity' => $entity, 'cars' => $cars, 'title' => $this->translator->trans('Edit discount')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }

    /**
     * @Route("/discounts/preview/", name="discounts_preview")
     * @Method("POST")
     */
    public function previewAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Discount id is not correct')));
        }

        $entity = $this->discounts_context->getById($p["id"]);

        $html = $this->twigEngine->render('AppBundle:Discounts:preview.html.twig', array('entity' => $entity, 'title' => $this->translator->trans('View discount')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }

    /**
     * @Route("/discounts/save", name="discounts_save")
     * @Method("POST")
     */
    public function saveAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["percent"]) || empty($p["percent"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Percent cannot be empty')));
        }
        if (!isset($p["dates"]) || empty($p["dates"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Dates cannot be empty')));
        }
        if (!isset($p["car"]) || empty($p["car"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Name cannot be empty')));
        }

        $dates = explode(" - ",$p["dates"]);
        $date_from = \DateTime::createFromFormat('d/m/Y H:i:s', $dates[0]." 00:00:00");
        $date_to = \DateTime::createFromFormat('d/m/Y H:i:s', $dates[1]." 00:00:00");
        $car = $this->cars_context->getById($p["car"]);

        /**
         * INSERT
         */
        if(!isset($p["id"]) || empty($p["id"])){

            $entity = new Discounts();
            $entity->setPercent($p["percent"]);
            $entity->setFrom($date_from);
            $entity->setTo($date_to);
            $entity->setCar($car);

            try {
                $this->discounts_context->save($entity);
            }
            catch(\Exception $e){
                dump($e->getMessage());
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
            }

            return new JsonResponse(array('error' => false, 'title' => $this->translator->trans('Create discount'), 'message' => $this->translator->trans('Discount has been created')));
        }
        /**
         * UPDATE
         */
        else{
            if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Discount id is not correct')));
            }

            $entity = $this->discounts_context->getById($p["id"]);

            if(!isset($entity) || empty($entity)){
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Discount does not exist')));
            }
            $entity->setPercent($p["percent"]);
            $entity->setFrom($date_from);
            $entity->setTo($date_to);
            $entity->setCar($car);

            try {
                $this->discounts_context->save($entity);
            }
            catch(\Exception $e){
                var_dump($e->getMessage());
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
            }

            return new JsonResponse(array('error' => false, 'title' => $this->translator->trans('Update discount'), 'message' => $this->translator->trans('Discount has been updated')));
        }

        return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
    }

    /**
     * @Route("/discounts/delete", name="discounts_delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Id is not correct')));
        }

        $entity = $this->discounts_context->getById($p["id"]);

        if(!isset($entity) || empty($entity)){
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Entity does not exist')));
        }

        try {
            $this->discounts_context->delete($entity);
        }
        catch(\Exception $e){
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
        }

        return new JsonResponse(array('error' => false, 'title' => $this->translator->trans('Delete'), 'message' => $this->translator->trans('Entity has been deleted')));
    }
}

