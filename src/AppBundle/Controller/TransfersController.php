<?php

namespace AppBundle\Controller;

use BusinessBundle\DataTable\DataTablePager;
use BusinessBundle\Entity\GlobalParameters;
use BusinessBundle\Entity\Transfers;
use BusinessBundle\Services\CarContext;
use BusinessBundle\Services\LocationsTransContext;
use BusinessBundle\Services\TransfersContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use BusinessBundle\Interfaces\BLL;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @Route(service="transfers_controller")
 */
class TransfersController extends Controller
{

    private $twigEngine;
    private $translator;
    private $transfers_context;
    private $locations_context;
    private $cars_context;

    public function __construct(TransfersContext $transfers_context, LocationsTransContext $locations_context, CarContext $cars_context, TwigEngine $twigEngine, TranslatorInterface $translator)
    {
        $this->transfers_context = $transfers_context;
        $this->twigEngine = $twigEngine;
        $this->translator = $translator;
        $this->locations_context = $locations_context;
        $this->cars_context = $cars_context;
    }

    /**
     * @Route("/transfers", name="transfers_index")
     */
    public function indexAction(Request $request)
    {
        /*$entities = $this->printer_brand_context->getAll();
        $brands_filter = Array();

        if($entities){
            foreach($entities as $key => $entity){
                $brands_filter[$key]["value"] = "{$entity->getId()}";
                $brands_filter[$key]["label"] = $entity->getName();
            }
            $brands_filter = json_encode($brands_filter);
        }*/

        return new Response($this->twigEngine->render('AppBundle:Transfers:index.html.twig', Array()));
    }

    /**
     * @Route("/transfers/list", name="get_transfers_list")
     * @Method("POST")
     */
    public function getTransfersList(Request $request)
    {
        $pager = new DataTablePager();
        $pager->setFromPost(json_decode($_POST["data"]));

        $entities = $this->transfers_context->getItemsWithPaging($pager);

        $html = $this->twigEngine->render('AppBundle:Transfers:list.html.twig', array('entities' => $entities));
        $num_of_items = $this->transfers_context->countAllItems();

        $ret = Array();
        $ret["draw"] = $pager->getDraw();
        $ret["recordsTotal"] = $num_of_items;
        $ret["recordsFiltered"] = $num_of_items;
        $ret["data"] = array();
        $ret["html"] = $html;

        return new JsonResponse($ret);
    }

    /**
     * @Route("/transfers/list/new", name="get_transfers_list_new")
     * @Method("POST")
     */
    public function getTransfersListNew(Request $request)
    {
        $pager = new DataTablePager();
        $pager->setFromPost(json_decode($_POST["data"]));

        $entities = $this->transfers_context->getItemsWithPagingNew($pager);

        $html = $this->twigEngine->render('AppBundle:Transfers:list.html.twig', array('entities' => $entities));
        $num_of_items = $this->transfers_context->countAllItemsNew();

        $ret = Array();
        $ret["draw"] = $pager->getDraw();
        $ret["recordsTotal"] = $num_of_items;
        $ret["recordsFiltered"] = $num_of_items;
        $ret["data"] = array();
        $ret["html"] = $html;

        return new JsonResponse($ret);
    }

    /**
     * @Route("transfers/create", name="transfers_create_form")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $locations = $this->locations_context->getAll();
        $cars = $this->cars_context->getAll();

        $html = $this->twigEngine->render('AppBundle:Transfers:form.html.twig', array('entity' => null, 'locations' => $locations, 'cars' => $cars, 'title' => $this->translator->trans('Create transfer')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }

    /**
     * @Route("transfers/update/", name="transfers_update_form")
     * @Method("POST")
     */
    public function updateAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Transfer id is not correct')));
        }

        $entity = $this->transfers_context->getById($p["id"]);

        if(!isset($entity) || empty($entity)){
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Transfer does not exist')));
        }

        $locations = $this->locations_context->getAll();
        $cars = $this->cars_context->getAll();

        $html = $this->twigEngine->render('AppBundle:Transfers:form.html.twig', array('entity' => $entity, 'locations' => $locations, 'cars' => $cars, 'title' => $this->translator->trans('Edit transfer')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }

    /**
     * @Route("transfers/preview/", name="transfers_preview")
     * @Method("POST")
     */
    public function previewAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Transfer id is not correct')));
        }

        $entity = $this->transfers_context->getById($p["id"]);

        $html = $this->twigEngine->render('AppBundle:Transfers:preview.html.twig', array('entity' => $entity, 'title' => $this->translator->trans('View Transfer')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }

    /**
     * @Route("/transfers/save", name="transfers_save")
     * @Method("POST")
     */
    public function saveAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["status"]) || empty($p["status"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Status cannot be empty')));
        }
        if (!isset($p["name"]) || empty($p["name"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Name cannot be empty')));
        }
        if (!isset($p["car"]) || empty($p["car"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Name cannot be empty')));
        }
        if (!isset($p["pickup_location"]) || empty($p["pickup_location"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Pickup location cannot be empty')));
        }
        if (!isset($p["pickup_time"]) || empty($p["pickup_time"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Pickup time cannot be empty')));
        }
        if (!isset($p["total"]) || empty($p["total"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Total cannot be empty')));
        }

        $date_from = \DateTime::createFromFormat('d/m/Y H:i', $p["pickup_time"]);
        $date_return = \DateTime::createFromFormat('d/m/Y H:i', $p["return_time"]);

        $car = $this->cars_context->getById($p["car"]);
        $pickup_location = $this->locations_context->getById($p["pickup_location"]);

        $return_location = null;
        if(!empty($p["return_location"])){
            $return_location = $this->locations_context->getById($p["return_location"]);
        }

        $now = new \DateTime();

        /**
         * INSERT
         */
        if(!isset($p["id"]) || empty($p["id"])){

            $entity = new Transfers();
            //$entity->setBookingId($p["booking_id"]);
            //$entity->setTitle($p["title"]);
            $entity->setPhone($p["phone"]);
            $entity->setEmail($p["email"]);
            $entity->setName($p["name"]);
            //$entity->setCity($p["city"]);
            //$entity->setCompany($p["company"]);
            //$entity->setAddress($p["address"]);
            //$entity->setState($p["state"]);
            //$entity->setZip($p["zip"]);
            //$entity->setCountry($p["country"]);
            $entity->setNotes($p["notes"]);
            $entity->setPickupId($pickup_location);
            $entity->setFrom($date_from);
            $entity->setReturnId($return_location);
            $entity->setCar($car);
            $entity->setTotalPrice($p["total"]);
            $entity->setExtraPrice(0); //$p["extra_price"]
            $entity->setCreated($now);
            $entity->setPassengers($p["passengers"]);
            $entity->setLuggage($p["luggage"]);
            $entity->setReturn($date_return);
            $entity->setDropoffInformation($p["dropoff_information"]);
            $entity->setFlightInformation($p["flight_information"]);

            try {
                $this->transfers_context->save($entity);
            }
            catch(\Exception $e){
                dump($e->getMessage());
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
            }

            return new JsonResponse(array('error' => false, 'title' => $this->translator->trans('Create transfer'), 'message' => $this->translator->trans('Transfer has been created')));

        }
        /**
         * UPDATE
         */
        else{
            if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Transfer id is not correct')));
            }

            $entity = $this->transfers_context->getById($p["id"]);

            if(!isset($entity) || empty($entity)){
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Transfer does not exist')));
            }
            //$entity->setTitle($p["title"]);
            $entity->setPhone($p["phone"]);
            $entity->setEmail($p["email"]);
            $entity->setName($p["name"]);
            //$entity->setCity($p["city"]);
            //$entity->setCompany($p["company"]);
            //$entity->setAddress($p["address"]);
            //$entity->setState($p["state"]);
            //$entity->setZip($p["zip"]);
            //$entity->setCountry($p["country"]);
            $entity->setNotes($p["notes"]);
            $entity->setPickupId($pickup_location);
            $entity->setFrom($date_from);
            $entity->setReturnId($return_location);
            $entity->setCar($car);
            $entity->setTotalPrice($p["total"]);
            //$entity->setExtraPrice($p["extra_price"]);
            $entity->setCreated($now);
            $entity->setPassengers($p["passengers"]);
            $entity->setLuggage($p["luggage"]);
            $entity->setReturn($date_return);
            $entity->setDropoffInformation($p["dropoff_information"]);
            $entity->setFlightInformation($p["flight_information"]);

            try {
                $this->transfers_context->save($entity);
            }
            catch(\Exception $e){
                var_dump($e->getMessage());
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
            }

            return new JsonResponse(array('error' => false, 'title' => $this->translator->trans('Update transfer'), 'message' => $this->translator->trans('Transfer has been updated')));
        }

        return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
    }

    /**
     * @Route("/transfers/delete", name="transfers_delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request)
    {

        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Id is not correct')));
        }

        $entity = $this->transfers_context->getById($p["id"]);

        if(!isset($entity) || empty($entity)){
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Entity does not exist')));
        }

        try {
            $this->transfers_context->delete($entity);
        }
        catch(\Exception $e){
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
        }

        return new JsonResponse(array('error' => false, 'title' => $this->translator->trans('Delete'), 'message' => $this->translator->trans('Entity has been deleted')));
    }
}

