<?php

namespace AppBundle\Controller;

use BusinessBundle\DataTable\DataTablePager;
use BusinessBundle\Entity\CarGroup;
use BusinessBundle\Entity\GlobalParameters;
use BusinessBundle\Services\CarGroupContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use BusinessBundle\Interfaces\BLL;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @Route(service="car_group_controller")
 */
class CarGroupController extends Controller
{

    private $twigEngine;
    private $translator;
    private $car_group_context;

    public function __construct(CarGroupContext $car_group_context, TwigEngine $twigEngine, TranslatorInterface $translator)
    {
        $this->twigEngine = $twigEngine;
        $this->translator = $translator;
        $this->car_group_context = $car_group_context;
    }

    /**
     * @Route("/car_group", name="car_group_index")
     */
    public function indexAction(Request $request)
    {
        /*$entities = $this->printer_brand_context->getAll();
        $brands_filter = Array();

        if($entities){
            foreach($entities as $key => $entity){
                $brands_filter[$key]["value"] = "{$entity->getId()}";
                $brands_filter[$key]["label"] = $entity->getName();
            }
            $brands_filter = json_encode($brands_filter);
        }*/

        return new Response($this->twigEngine->render('AppBundle:CarGroup:index.html.twig', Array()));
    }

    /**
     * @Route("/car_group/list", name="get_car_group_list")
     * @Method("POST")
     */
    public function getList(Request $request)
    {
        $pager = new DataTablePager();
        $pager->setFromPost(json_decode($_POST["data"]));

        $entities = $this->car_group_context->getItemsWithPaging($pager);

        $html = $this->twigEngine->render('AppBundle:CarGroup:list.html.twig', array('entities' => $entities));
        $num_of_items = $this->car_group_context->countAllItems();

        $ret = Array();
        $ret["draw"] = $pager->getDraw();
        $ret["recordsTotal"] = $num_of_items;
        $ret["recordsFiltered"] = $num_of_items;
        $ret["data"] = array();
        $ret["html"] = $html;

        return new JsonResponse($ret);
    }

    /**
     * @Route("/car_group/create", name="car_group_create_form")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $html = $this->twigEngine->render('AppBundle:CarGroup:form.html.twig', array('entity' => null, 'title' => $this->translator->trans('Create car group')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }

    /**
     * @Route("/car_group/update/", name="car_group_update_form")
     * @Method("POST")
     */
    public function updateAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Car group id is not correct')));
        }

        $entity = $this->car_group_context->getById($p["id"]);

        if(!isset($entity) || empty($entity)){
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Car group does not exist')));
        }

        $html = $this->twigEngine->render('AppBundle:CarGroup:form.html.twig', array('entity' => $entity, 'title' => $this->translator->trans('Edit car group')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }

    /**
     * @Route("/car_group/preview/", name="car_group_preview")
     * @Method("POST")
     */
    public function previewAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Car group id is not correct')));
        }

        $entity = $this->car_group_context->getById($p["id"]);

        $html = $this->twigEngine->render('AppBundle:CarGroup:preview.html.twig', array('entity' => $entity, 'title' => $this->translator->trans('View car group')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }

    /**
     * @Route("/car_group/save", name="car_group_save")
     * @Method("POST")
     */
    public function saveAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["name"]) || empty($p["name"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Name cannot be empty')));
        }
        if (!isset($p["qty"]) || empty($p["qty"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Qty cannot be empty')));
        }
        if (!isset($p["car_type"]) || empty($p["car_type"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Car type cannot be empty')));
        }
        if (!isset($p["price1"]) || empty($p["price1"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Price1 cannot be empty')));
        }
        if (!isset($p["price2"]) || empty($p["price2"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Price2 cannot be empty')));
        }
        if (!isset($p["price3"]) || empty($p["price3"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Price3 cannot be empty')));
        }
        if (!isset($p["price4"]) || empty($p["price4"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Price4 cannot be empty')));
        }

        /**
         * INSERT
         */
        if(!isset($p["id"]) || empty($p["id"])){

            $entity = new CarGroup();
            $entity->setName($p["name"]);
            $entity->setCarType($p["car_type"]);
            $entity->setQty($p["qty"]);
            $entity->setPrice1($p["price1"]);
            $entity->setPrice2($p["price2"]);
            $entity->setPrice3($p["price3"]);
            $entity->setPrice4($p["price4"]);
            $entity->setShow($p["show"]);
            $entity->setOrder($p["order"]);
            $entity->setPersons($p["persons"]);
            $entity->setLuggage($p["luggage"]);
            $entity->setShift($p["shift"]);
            $entity->setDoors($p["doors"]);
            $entity->setImage("");

            try {
                $this->car_group_context->save($entity);
            }
            catch(\Exception $e){
                dump($e->getMessage());
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
            }

            return new JsonResponse(array('error' => false, 'title' => $this->translator->trans('Create car group'), 'message' => $this->translator->trans('Car group has been created')));
        }
        /**
         * UPDATE
         */
        else{
            if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Car group id is not correct')));
            }

            $entity = $this->car_group_context->getById($p["id"]);

            if(!isset($entity) || empty($entity)){
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Car group does not exist')));
            }
            $entity->setName($p["name"]);
            $entity->setCarType($p["car_type"]);
            $entity->setQty($p["qty"]);
            $entity->setPrice1($p["price1"]);
            $entity->setPrice2($p["price2"]);
            $entity->setPrice3($p["price3"]);
            $entity->setPrice4($p["price4"]);
            $entity->setShow($p["show"]);
            $entity->setOrder($p["order"]);
            $entity->setPersons($p["persons"]);
            $entity->setLuggage($p["luggage"]);
            $entity->setShift($p["shift"]);
            $entity->setDoors($p["doors"]);

            try {
                $this->car_group_context->save($entity);
            }
            catch(\Exception $e){
                var_dump($e->getMessage());
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
            }

            return new JsonResponse(array('error' => false, 'title' => $this->translator->trans('Update car group'), 'message' => $this->translator->trans('Car group has been updated')));
        }

        return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
    }

    /**
     * @Route("/car_group/delete", name="car_group_delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Id is not correct')));
        }

        $entity = $this->car_group_context->getById($p["id"]);

        if(!isset($entity) || empty($entity)){
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Entity does not exist')));
        }

        try {
            $this->car_group_context->delete($entity);
        }
        catch(\Exception $e){
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
        }

        return new JsonResponse(array('error' => false, 'title' => $this->translator->trans('Delete'), 'message' => $this->translator->trans('Entity has been deleted')));
    }
}

