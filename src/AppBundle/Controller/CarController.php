<?php

namespace AppBundle\Controller;

use BusinessBundle\DataTable\DataTablePager;
use BusinessBundle\Entity\Car;
use BusinessBundle\Entity\GlobalParameters;
use BusinessBundle\Services\CarContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use BusinessBundle\Interfaces\BLL;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @Route(service="car_controller")
 */
class CarController extends Controller
{

    private $twigEngine;
    private $translator;
    private $car_context;

    public function __construct(CarContext $car_context, TwigEngine $twigEngine, TranslatorInterface $translator)
    {
        $this->twigEngine = $twigEngine;
        $this->translator = $translator;
        $this->car_context = $car_context;
    }

    /**
     * @Route("/car", name="car_index")
     */
    public function indexAction(Request $request)
    {
        /*$entities = $this->printer_brand_context->getAll();
        $brands_filter = Array();

        if($entities){
            foreach($entities as $key => $entity){
                $brands_filter[$key]["value"] = "{$entity->getId()}";
                $brands_filter[$key]["label"] = $entity->getName();
            }
            $brands_filter = json_encode($brands_filter);
        }*/

        return new Response($this->twigEngine->render('AppBundle:Car:index.html.twig', Array()));
    }

    /**
     * @Route("/car/list", name="get_car_list")
     * @Method("POST")
     */
    public function getList(Request $request)
    {
        $pager = new DataTablePager();
        $pager->setFromPost(json_decode($_POST["data"]));

        $entities = $this->car_context->getItemsWithPaging($pager);

        $html = $this->twigEngine->render('AppBundle:Car:list.html.twig', array('entities' => $entities));
        $num_of_items = $this->car_context->countAllItems();

        $ret = Array();
        $ret["draw"] = $pager->getDraw();
        $ret["recordsTotal"] = $num_of_items;
        $ret["recordsFiltered"] = $num_of_items;
        $ret["data"] = array();
        $ret["html"] = $html;

        return new JsonResponse($ret);
    }

    /**
     * @Route("/car/create", name="car_create_form")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $html = $this->twigEngine->render('AppBundle:Car:form.html.twig', array('entity' => null, 'title' => $this->translator->trans('Create car')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }

    /**
     * @Route("/car/update/", name="car_update_form")
     * @Method("POST")
     */
    public function updateAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Car id is not correct')));
        }

        $entity = $this->car_context->getById($p["id"]);

        if(!isset($entity) || empty($entity)){
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Car does not exist')));
        }

        $html = $this->twigEngine->render('AppBundle:Car:form.html.twig', array('entity' => $entity, 'title' => $this->translator->trans('Edit car')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }

    /**
     * @Route("/car/preview/", name="car_preview")
     * @Method("POST")
     */
    public function previewAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Car id is not correct')));
        }

        $entity = $this->car_context->getById($p["id"]);

        $html = $this->twigEngine->render('AppBundle:Car:preview.html.twig', array('entity' => $entity, 'title' => $this->translator->trans('View car')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }

    /**
     * @Route("/car/save", name="car_save")
     * @Method("POST")
     */
    public function saveAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["name"]) || empty($p["name"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Name cannot be empty')));
        }
        if (!isset($p["passangers"]) || empty($p["passangers"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Passangers cannot be empty')));
        }
        if (!isset($p["luggage"]) || empty($p["luggage"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Luggage type cannot be empty')));
        }

        /**
         * INSERT
         */
        if(!isset($p["id"]) || empty($p["id"])){

            $entity = new Car();
            $entity->setName($p["name"]);
            $entity->setPassangers($p["passangers"]);
            $entity->setLuggage($p["luggage"]);
            $entity->setShow($p["show"]);
            $entity->setOrder($p["order"]);
            $entity->setImage("");

            try {
                $this->car_context->save($entity);
            }
            catch(\Exception $e){
                dump($e->getMessage());
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
            }

            return new JsonResponse(array('error' => false, 'title' => $this->translator->trans('Create car'), 'message' => $this->translator->trans('Car has been created')));
        }
        /**
         * UPDATE
         */
        else{
            if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Car id is not correct')));
            }

            $entity = $this->car_context->getById($p["id"]);

            if(!isset($entity) || empty($entity)){
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Car does not exist')));
            }
            $entity->setName($p["name"]);
            $entity->setPassangers($p["passangers"]);
            $entity->setLuggage($p["luggage"]);
            $entity->setShow($p["show"]);
            $entity->setOrder($p["order"]);

            try {
                $this->car_context->save($entity);
            }
            catch(\Exception $e){
                var_dump($e->getMessage());
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
            }

            return new JsonResponse(array('error' => false, 'title' => $this->translator->trans('Update car'), 'message' => $this->translator->trans('Car has been updated')));
        }

        return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
    }

    /**
     * @Route("/car/delete", name="car_delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Id is not correct')));
        }

        $entity = $this->car_context->getById($p["id"]);

        if(!isset($entity) || empty($entity)){
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Entity does not exist')));
        }

        try {
            $this->car_context->delete($entity);
        }
        catch(\Exception $e){
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
        }

        return new JsonResponse(array('error' => false, 'title' => $this->translator->trans('Delete'), 'message' => $this->translator->trans('Entity has been deleted')));
    }
}

