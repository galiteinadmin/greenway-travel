<?php

namespace AppBundle\Controller;

use BusinessBundle\DataTable\DataTablePager;
use BusinessBundle\Entity\LocationsTrans;
use BusinessBundle\Entity\LocationTransPrices;
use BusinessBundle\Entity\GlobalParameters;
use BusinessBundle\Services\CarContext;
use BusinessBundle\Services\LocationTransPricesContext;
use BusinessBundle\Services\LocationsTransContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use BusinessBundle\Interfaces\BLL;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @Route(service="location_trans_prices_controller")
 */
class LocationTransPricesController extends Controller
{

    private $twigEngine;
    private $translator;
    private $location_trans_prices_context;
    private $locationsContext;
    private $carContext;

    public function __construct(LocationTransPricesContext $location_trans_prices_context, LocationsTransContext $locationsContext, CarContext $carContext, TwigEngine $twigEngine, TranslatorInterface $translator)
    {
        $this->twigEngine = $twigEngine;
        $this->translator = $translator;
        $this->location_trans_prices_context = $location_trans_prices_context;
        $this->locationsContext = $locationsContext;
        $this->carContext = $carContext;
    }

    /**
     * @Route("/location_trans_prices", name="location_trans_prices_index")
     */
    public function indexAction(Request $request)
    {
        $locations = $this->locationsContext->getAll();
        //$entities = $this->location_trans_prices_context->getAll();
        $cars = $this->carContext->getAll();

        $locationtransprices = Array();
        foreach($cars as $car){
            $locationtransprices[$car->getId()] = $this->location_trans_prices_context->getAll(Array("car" => $car));
        }

        return new Response($this->twigEngine->render('AppBundle:LocationTransPrices:index.html.twig', Array('locations' => $locations, 'locationtransprices' => $locationtransprices, 'cars' => $cars)));
    }

    /**
     * @Route("/location_trans_prices/list", name="get_location_trans_prices_list")
     * @Method("POST")
     */
    /*public function getList(Request $request)
    {
        $pager = new DataTablePager();
        $pager->setFromPost(json_decode($_POST["data"]));

        $entities = $this->location_trans_prices_context->getItemsWithPaging($pager);

        $html = $this->twigEngine->render('AppBundle:LocationTransPrices:list.html.twig', array('entities' => $entities));
        $num_of_items = $this->location_trans_prices_context->countAllItems();

        $ret = Array();
        $ret["draw"] = $pager->getDraw();
        $ret["recordsTotal"] = $num_of_items;
        $ret["recordsFiltered"] = $num_of_items;
        $ret["data"] = array();
        $ret["html"] = $html;

        return new JsonResponse($ret);
    }*/

    /**
     * @Route("/location_trans_prices/create", name="location_trans_prices_create_form")
     * @Method("POST")
     */
    /*public function createAction(Request $request)
    {
        $locations = $this->locationsContext->getAll();

        $html = $this->twigEngine->render('AppBundle:LocationTransPrices:form.html.twig', array('entity' => null, 'locations'=>$locations,'title' => $this->translator->trans('Create location rent price')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }*/

    /**
     * @Route("/location_trans_prices/update/", name="location_trans_prices_update_form")
     * @Method("POST")
     */
    /*public function updateAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Location rent price id is not correct')));
        }

        $locations = $this->locationsContext->getAll();

        $entity = $this->location_trans_prices_context->getById($p["id"]);

        if(!isset($entity) || empty($entity)){
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Location rent price does not exist')));
        }

        $html = $this->twigEngine->render('AppBundle:LocationTransPrices:form.html.twig', array('entity' => $entity, 'locations'=>$locations, 'title' => $this->translator->trans('Edit location rent price')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }*/

    /**
     * @Route("/location_trans_prices/preview/", name="location_trans_prices_preview")
     * @Method("POST")
     */
    /*public function previewAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Location rent price id is not correct')));
        }

        $entity = $this->location_trans_prices_context->getById($p["id"]);

        $html = $this->twigEngine->render('AppBundle:LocationTransPrices:preview.html.twig', array('entity' => $entity, 'title' => $this->translator->trans('View location rent price')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }*/

    /**
     * @Route("/location_trans_prices/save", name="location_trans_prices_save")
     * @Method("POST")
     */
    public function saveAction(Request $request)
    {
        $p = $_POST;

        $car = $this->carContext->getById($p["car"]);

        foreach($p["location"] as $key => $locations){
            $location_from = $this->locationsContext->getById($key);

            $entities = $this->location_trans_prices_context->getAll(Array("locationFrom" => $key, "car" => $car), Array());
            foreach($entities as $ek => $entity){
                if(isset($locations[$entity->getLocationTo()->getId()])){
                    $entity->setPrice($locations[$entity->getLocationTo()->getId()]);
                    $this->location_trans_prices_context->save($entity);
                    unset($locations[$entity->getLocationTo()->getId()]);
                }
                else{
                    $this->location_trans_prices_context->delete($entity);
                }
            }
            if(!empty($locations)){
                foreach($locations as $loc_id => $loc){
                    $location_to = $this->locationsContext->getById($loc_id);

                    $entity = new LocationTransPrices();
                    $entity->setLocationFrom($location_from);
                    $entity->setLocationTo($location_to);
                    $entity->setPrice($loc);
                    $entity->setCar($car);
                    $entity->setShow(1);

                    $this->location_trans_prices_context->save($entity);
                }
            }
        }

        return new JsonResponse(array('error' => false, 'title' => $this->translator->trans('Location trans price'), 'message' => $this->translator->trans('Prices are saved')));
    }
}

