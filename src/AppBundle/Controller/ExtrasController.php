<?php

namespace AppBundle\Controller;

use BusinessBundle\DataTable\DataTablePager;
use BusinessBundle\Entity\Extras;
use BusinessBundle\Entity\GlobalParameters;
use BusinessBundle\Services\ExtrasContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use BusinessBundle\Interfaces\BLL;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @Route(service="extras_controller")
 */
class ExtrasController extends Controller
{

    private $twigEngine;
    private $translator;
    private $extras_context;

    public function __construct(ExtrasContext $extras_context, TwigEngine $twigEngine, TranslatorInterface $translator)
    {
        $this->twigEngine = $twigEngine;
        $this->translator = $translator;
        $this->extras_context = $extras_context;
    }

    /**
     * @Route("/extras", name="extras_index")
     */
    public function indexAction(Request $request)
    {
        /*$entities = $this->printer_brand_context->getAll();
        $brands_filter = Array();

        if($entities){
            foreach($entities as $key => $entity){
                $brands_filter[$key]["value"] = "{$entity->getId()}";
                $brands_filter[$key]["label"] = $entity->getName();
            }
            $brands_filter = json_encode($brands_filter);
        }*/

        return new Response($this->twigEngine->render('AppBundle:Extras:index.html.twig', Array()));
    }

    /**
     * @Route("/extras/list", name="get_extras_list")
     * @Method("POST")
     */
    public function getList(Request $request)
    {
        $pager = new DataTablePager();
        $pager->setFromPost(json_decode($_POST["data"]));

        $entities = $this->extras_context->getItemsWithPaging($pager);

        $html = $this->twigEngine->render('AppBundle:Extras:list.html.twig', array('entities' => $entities));
        $num_of_items = $this->extras_context->countAllItems();

        $ret = Array();
        $ret["draw"] = $pager->getDraw();
        $ret["recordsTotal"] = $num_of_items;
        $ret["recordsFiltered"] = $num_of_items;
        $ret["data"] = array();
        $ret["html"] = $html;

        return new JsonResponse($ret);
    }

    /**
     * @Route("/extras/create", name="extras_create_form")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $html = $this->twigEngine->render('AppBundle:Extras:form.html.twig', array('entity' => null, 'title' => $this->translator->trans('Create extras')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }

    /**
     * @Route("/extras/update/", name="extras_update_form")
     * @Method("POST")
     */
    public function updateAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Extras id is not correct')));
        }

        $entity = $this->extras_context->getById($p["id"]);

        if(!isset($entity) || empty($entity)){
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Extras does not exist')));
        }

        $html = $this->twigEngine->render('AppBundle:Extras:form.html.twig', array('entity' => $entity, 'title' => $this->translator->trans('Edit extras')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }

    /**
     * @Route("/extras/preview/", name="extras_preview")
     * @Method("POST")
     */
    public function previewAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Extras id is not correct')));
        }

        $entity = $this->extras_context->getById($p["id"]);

        $html = $this->twigEngine->render('AppBundle:Extras:preview.html.twig', array('entity' => $entity, 'title' => $this->translator->trans('View extras')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }

    /**
     * @Route("/extras/save", name="extras_save")
     * @Method("POST")
     */
    public function saveAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["name"]) || empty($p["name"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Name cannot be empty')));
        }
        if (!isset($p["price1"]) || empty($p["price1"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Price cannot be empty')));
        }
        if (!isset($p["description"]) || empty($p["description"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Description cannot be empty')));
        }
        if (!isset($p["price_type"]) || empty($p["price_type"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Price type cannot be empty')));
        }

        /**
         * INSERT
         */
        if(!isset($p["id"]) || empty($p["id"])){

            $entity = new Extras();
            $entity->setName($p["name"]);
            $entity->setPrice1($p["price1"]);
            $entity->setPriceType($p["price_type"]);
            $entity->setDescription($p["description"]);
            $entity->setShow($p["show"]);
            $entity->setOrder($p["order"]);
            $entity->setImage("");

            try {
                $this->extras_context->save($entity);
            }
            catch(\Exception $e){
                dump($e->getMessage());
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
            }

            return new JsonResponse(array('error' => false, 'title' => $this->translator->trans('Create extras'), 'message' => $this->translator->trans('Extras has been created')));
        }
        /**
         * UPDATE
         */
        else{
            if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Extras id is not correct')));
            }

            $entity = $this->extras_context->getById($p["id"]);

            if(!isset($entity) || empty($entity)){
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Extras does not exist')));
            }
            $entity->setName($p["name"]);
            $entity->setPrice1($p["price1"]);
            $entity->setPriceType($p["price_type"]);
            $entity->setDescription($p["description"]);
            $entity->setShow($p["show"]);
            $entity->setOrder($p["order"]);

            try {
                $this->extras_context->save($entity);
            }
            catch(\Exception $e){
                var_dump($e->getMessage());
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
            }

            return new JsonResponse(array('error' => false, 'title' => $this->translator->trans('Update extras'), 'message' => $this->translator->trans('Extras has been updated')));
        }

        return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
    }

    /**
     * @Route("/extras/delete", name="extras_delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Id is not correct')));
        }

        $entity = $this->extras_context->getById($p["id"]);

        if(!isset($entity) || empty($entity)){
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Entity does not exist')));
        }

        try {
            $this->extras_context->delete($entity);
        }
        catch(\Exception $e){
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
        }

        return new JsonResponse(array('error' => false, 'title' => $this->translator->trans('Delete'), 'message' => $this->translator->trans('Entity has been deleted')));
    }
}

