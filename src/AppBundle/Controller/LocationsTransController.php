<?php

namespace AppBundle\Controller;

use BusinessBundle\DataTable\DataTablePager;
use BusinessBundle\Entity\LocationsTrans;
use BusinessBundle\Entity\GlobalParameters;
use BusinessBundle\Services\LocationsTransContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use BusinessBundle\Interfaces\BLL;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @Route(service="locations_trans_controller")
 */
class LocationsTransController extends Controller
{

    private $twigEngine;
    private $translator;
    private $locations_trans_context;

    public function __construct(LocationsTransContext $locations_trans_context, TwigEngine $twigEngine, TranslatorInterface $translator)
    {
        $this->twigEngine = $twigEngine;
        $this->translator = $translator;
        $this->locations_trans_context = $locations_trans_context;
    }

    /**
     * @Route("/locations_trans", name="locations_trans_index")
     */
    public function indexAction(Request $request)
    {
        /*$entities = $this->printer_brand_context->getAll();
        $brands_filter = Array();

        if($entities){
            foreach($entities as $key => $entity){
                $brands_filter[$key]["value"] = "{$entity->getId()}";
                $brands_filter[$key]["label"] = $entity->getName();
            }
            $brands_filter = json_encode($brands_filter);
        }*/

        return new Response($this->twigEngine->render('AppBundle:LocationsTrans:index.html.twig', Array()));
    }

    /**
     * @Route("/locations_trans/list", name="get_locations_trans_list")
     * @Method("POST")
     */
    public function getList(Request $request)
    {
        $pager = new DataTablePager();
        $pager->setFromPost(json_decode($_POST["data"]));

        $entities = $this->locations_trans_context->getItemsWithPaging($pager);

        $html = $this->twigEngine->render('AppBundle:LocationsTrans:list.html.twig', array('entities' => $entities));
        $num_of_items = $this->locations_trans_context->countAllItems();

        $ret = Array();
        $ret["draw"] = $pager->getDraw();
        $ret["recordsTotal"] = $num_of_items;
        $ret["recordsFiltered"] = $num_of_items;
        $ret["data"] = array();
        $ret["html"] = $html;

        return new JsonResponse($ret);
    }

    /**
     * @Route("/locations_trans/create", name="locations_trans_create_form")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $html = $this->twigEngine->render('AppBundle:LocationsTrans:form.html.twig', array('entity' => null, 'title' => $this->translator->trans('Create location')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }

    /**
     * @Route("/locations_trans/update/", name="locations_trans_update_form")
     * @Method("POST")
     */
    public function updateAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Location id is not correct')));
        }

        $entity = $this->locations_trans_context->getById($p["id"]);

        if(!isset($entity) || empty($entity)){
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Location does not exist')));
        }

        $html = $this->twigEngine->render('AppBundle:LocationsTrans:form.html.twig', array('entity' => $entity, 'title' => $this->translator->trans('Edit location')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }

    /**
     * @Route("/locations_trans/preview/", name="locations_trans_preview")
     * @Method("POST")
     */
    public function previewAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Location id is not correct')));
        }

        $entity = $this->locations_trans_context->getById($p["id"]);

        $html = $this->twigEngine->render('AppBundle:LocationsTrans:preview.html.twig', array('entity' => $entity, 'title' => $this->translator->trans('View location')));
        return new JsonResponse(array('error' => false, 'html' => $html));
    }

    /**
     * @Route("/locations_trans/save", name="locations_trans_save")
     * @Method("POST")
     */
    public function saveAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["name"]) || empty($p["name"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Name cannot be empty')));
        }

        /**
         * INSERT
         */
        if(!isset($p["id"]) || empty($p["id"])){

            $entity = new LocationsTrans();
            $entity->setName($p["name"]);
            $entity->setShow($p["show"]);

            try {
                $this->locations_trans_context->save($entity);
            }
            catch(\Exception $e){
                dump($e->getMessage());
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
            }

            return new JsonResponse(array('error' => false, 'title' => $this->translator->trans('Create location'), 'message' => $this->translator->trans('Location has been created')));
        }
        /**
         * UPDATE
         */
        else{
            if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Location id is not correct')));
            }

            $entity = $this->locations_trans_context->getById($p["id"]);

            if(!isset($entity) || empty($entity)){
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Location does not exist')));
            }
            $entity->setName($p["name"]);
            $entity->setShow($p["show"]);

            try {
                $this->locations_trans_context->save($entity);
            }
            catch(\Exception $e){
                var_dump($e->getMessage());
                return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
            }

            return new JsonResponse(array('error' => false, 'title' => $this->translator->trans('Update location'), 'message' => $this->translator->trans('Location has been updated')));
        }

        return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
    }

    /**
     * @Route("/locations_trans/delete", name="locations_trans_delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request)
    {
        $p = $_POST;

        if (!isset($p["id"]) || empty($p["id"]) || !preg_match('/^[0-9]*$/', $p["id"])) {
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Id is not correct')));
        }

        $entity = $this->locations_trans_context->getById($p["id"]);

        if(!isset($entity) || empty($entity)){
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('Entity does not exist')));
        }

        try {
            $this->locations_trans_context->delete($entity);
        }
        catch(\Exception $e){
            return new JsonResponse(array('error' => true, 'message' => $this->translator->trans('There has been an error please try again')));
        }

        return new JsonResponse(array('error' => false, 'title' => $this->translator->trans('Delete'), 'message' => $this->translator->trans('Entity has been deleted')));
    }
}

