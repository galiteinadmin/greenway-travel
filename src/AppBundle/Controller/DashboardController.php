<?php

namespace AppBundle\Controller;

use BusinessBundle\Entity\Bookings;
use BusinessBundle\Entity\GlobalParameters;
use BusinessBundle\Services\BookingsContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use BusinessBundle\Interfaces\BLL;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @Route(service="dashboard_controller")
 */
class DashboardController extends Controller
{

    private $twigEngine;
    private $translator;
    private $tokenStorage;
    private $bookings_context;

    public function __construct(BookingsContext $bookings_context, TwigEngine $twigEngine, TranslatorInterface $translator, TokenStorageInterface $tokenStorage)
    {
        $this->bookings_context = $bookings_context;
        $this->twigEngine = $twigEngine;
        $this->translator = $translator;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @Route("/", name="dashboard_index")
     */
    public function indexAction(Request $request)
    {
        /*$entities = $this->printer_brand_context->getAll();
        $brands_filter = Array();

        if($entities){
            foreach($entities as $key => $entity){
                $brands_filter[$key]["value"] = "{$entity->getId()}";
                $brands_filter[$key]["label"] = $entity->getName();
            }
            $brands_filter = json_encode($brands_filter);
        }*/

        return new Response($this->twigEngine->render('AppBundle:Dashboard:index.html.twig', Array()));
    }
}

