<?php

namespace BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bookings
 *
 * @ORM\Table(name="sp_car_group")
 * @ORM\Entity
 */
class CarGroup
{

    public function __construct()
    {
    }

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CarGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="qty", type="string", length=255, nullable=false)
     */
    private $qty;

    /**
     * Set qty
     *
     * @param string $qty
     *
     * @return CarGroup
     */
    public function setQty($qty)
    {
        $this->qty = $qty;

        return $this;
    }

    /**
     * Get qty
     *
     * @return string
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="carType", type="string", length=255, nullable=false)
     */
    private $carType;

    /**
     * Set qty
     *
     * @param string $carType
     *
     * @return CarGroup
     */
    public function setCarType($carType)
    {
        $this->carType = $carType;

        return $this;
    }

    /**
     * Get carType
     *
     * @return string
     */
    public function getCarType()
    {
        return $this->carType;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="order", type="string", length=255, nullable=false)
     */
    private $order;

    /**
     * Set order
     *
     * @param string $order
     *
     * @return CarGroup
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return string
     */
    public function getOrder()
    {
        return $this->order;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=false)
     */
    private $image;

    /**
     * Set image
     *
     * @param string $image
     *
     * @return CarGroup
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="show", type="string", length=255, nullable=false)
     */
    private $show;

    /**
     * Set show
     *
     * @param string $show
     *
     * @return CarGroup
     */
    public function setShow($show)
    {
        $this->show = $show;

        return $this;
    }

    /**
     * Get show
     *
     * @return string
     */
    public function getShow()
    {
        return $this->show;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="price1", type="decimal", nullable=false)
     */
    private $price1;

    /**
     * Set price1
     *
     * @param string $price1
     *
     * @return Toner
     */
    public function setPrice1($price1)
    {
        $this->price1 = $price1;

        return $this;
    }

    /**
     * Get price1
     *
     * @return string
     */
    public function getPrice1()
    {
        return $this->price1;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="price2", type="decimal", nullable=false)
     */
    private $price2;

    /**
     * Set price2
     *
     * @param string $price2
     *
     * @return Toner
     */
    public function setPrice2($price2)
    {
        $this->price2 = $price2;

        return $this;
    }

    /**
     * Get price2
     *
     * @return string
     */
    public function getPrice2()
    {
        return $this->price2;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="price3", type="decimal", nullable=false)
     */
    private $price3;

    /**
     * Set price3
     *
     * @param string $price3
     *
     * @return Toner
     */
    public function setPrice3($price3)
    {
        $this->price3 = $price3;

        return $this;
    }

    /**
     * Get price3
     *
     * @return string
     */
    public function getPrice3()
    {
        return $this->price3;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="price4", type="decimal", nullable=false)
     */
    private $price4;

    /**
     * Set price4
     *
     * @param string $price4
     *
     * @return Toner
     */
    public function setPrice4($price4)
    {
        $this->price4 = $price4;

        return $this;
    }

    /**
     * Get price4
     *
     * @return string
     */
    public function getPrice4()
    {
        return $this->price4;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="persons", type="string", length=255, nullable=false)
     */
    private $persons;

    /**
     * Set persons
     *
     * @param string $persons
     *
     * @return CarGroup
     */
    public function setPersons($persons)
    {
        $this->persons = $persons;

        return $this;
    }

    /**
     * Get persons
     *
     * @return string
     */
    public function getPersons()
    {
        return $this->persons;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="doors", type="string", length=255, nullable=false)
     */
    private $doors;

    /**
     * Set doors
     *
     * @param string $doors
     *
     * @return CarGroup
     */
    public function setDoors($doors)
    {
        $this->doors = $doors;

        return $this;
    }

    /**
     * Get doors
     *
     * @return string
     */
    public function getDoors()
    {
        return $this->doors;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="luggage", type="string", length=255, nullable=false)
     */
    private $luggage;

    /**
     * Set luggage
     *
     * @param string $luggage
     *
     * @return CarGroup
     */
    public function setLuggage($luggage)
    {
        $this->luggage = $luggage;

        return $this;
    }

    /**
     * Get luggage
     *
     * @return string
     */
    public function getLuggage()
    {
        return $this->luggage;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="shift", type="string", length=255, nullable=false)
     */
    private $shift;

    /**
     * Set shift
     *
     * @param string $shift
     *
     * @return CarGroup
     */
    public function setShift($shift)
    {
        $this->shift = $shift;

        return $this;
    }

    /**
     * Get shift
     *
     * @return string
     */
    public function getShift()
    {
        return $this->shift;
    }


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
