<?php

namespace BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bookings
 *
 * @ORM\Table(name="sp_car")
 * @ORM\Entity
 */
class Car
{

    public function __construct()
    {
    }

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Car
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="passangers", type="string", length=255, nullable=false)
     */
    private $passangers;

    /**
     * Set passangers
     *
     * @param string $passangers
     *
     * @return Car
     */
    public function setPassangers($passangers)
    {
        $this->passangers = $passangers;

        return $this;
    }

    /**
     * Get passangers
     *
     * @return string
     */
    public function getPassangers()
    {
        return $this->passangers;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="luggage", type="string", length=255, nullable=false)
     */
    private $luggage;

    /**
     * Set luggage
     *
     * @param string $luggage
     *
     * @return Car
     */
    public function setLuggage($luggage)
    {
        $this->luggage = $luggage;

        return $this;
    }

    /**
     * Get luggage
     *
     * @return string
     */
    public function getLuggage()
    {
        return $this->luggage;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="order", type="string", length=255, nullable=false)
     */
    private $order;

    /**
     * Set order
     *
     * @param string $order
     *
     * @return Car
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return string
     */
    public function getOrder()
    {
        return $this->order;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=false)
     */
    private $image;

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Car
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="show", type="string", length=255, nullable=false)
     */
    private $show;

    /**
     * Set show
     *
     * @param string $show
     *
     * @return Car
     */
    public function setShow($show)
    {
        $this->show = $show;

        return $this;
    }

    /**
     * Get show
     *
     * @return string
     */
    public function getShow()
    {
        return $this->show;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
