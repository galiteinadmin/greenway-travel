<?php

namespace BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Bookings
 *
 * @ORM\Table(name="sp_transfer")
 * @ORM\Entity
 */
class Transfers
{

    public function __construct()
    {
    }

    /**
     * @var string
     *
     * @ORM\Column(name="booking_id", type="string", length=255, nullable=false)
     */
    private $bookingId;

    /**
     * Set bookingId
     *
     * @param string $bookingId
     *
     * @return Bookings
     */
    public function setBookingId($bookingId)
    {
        $this->bookingId = $bookingId;

        return $this;
    }

    /**
     * Get bookingId
     *
     * @return string
     */
    public function getBookingId()
    {
        return $this->bookingId;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="passengers", type="string", length=255, nullable=false)
     */
    private $passengers;

    /**
     * Set passengers
     *
     * @param string $passengers
     *
     * @return Toner
     */
    public function setPassengers($passengers)
    {
        $this->passengers = $passengers;

        return $this;
    }

    /**
     * Get passengers
     *
     * @return string
     */
    public function getPassengers()
    {
        return $this->passengers;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="luggage", type="string", length=255, nullable=false)
     */
    private $luggage;

    /**
     * Set luggage
     *
     * @param string $luggage
     *
     * @return Toner
     */
    public function setLuggage($luggage)
    {
        $this->luggage = $luggage;

        return $this;
    }

    /**
     * Get luggage
     *
     * @return string
     */
    public function getLuggage()
    {
        return $this->luggage;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="extras_list", type="string", length=255, nullable=false)
     */
    private $extrasList;

    /**
     * Set extrasList
     *
     * @param string $extrasList
     *
     * @return Bookings
     */
    public function setExtrasList($extrasList)
    {
        $this->extrasList = $extrasList;

        return $this;
    }

    /**
     * Get extrasList
     *
     * @return string
     */
    public function getExtrasList()
    {
        return $this->extrasList;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="extra_price", type="decimal", nullable=false)
     */
    private $extraPrice;

    /**
     * Set extraPrice
     *
     * @param string $extraPrice
     *
     * @return Bookings
     */
    public function setExtraPrice($extraPrice)
    {
        $this->extraPrice = $extraPrice;

        return $this;
    }

    /**
     * Get extraPrice
     *
     * @return string
     */
    public function getExtraPrice()
    {
        return $this->extraPrice;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="total_price", type="decimal", nullable=false)
     */
    private $totalPrice;

    /**
     * Set totalPrice
     *
     * @param string $totalPrice
     *
     * @return Bookings
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return string
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Bookings
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="from", type="datetime", nullable=false)
     */
    private $from;

    /**
     * Set from
     *
     * @param \DateTime $from
     * @return Bookings
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return \DateTime
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="return", type="datetime", nullable=true)
     */
    private $return;

    /**
     * Set return
     *
     * @param \DateTime $return
     * @return Bookings
     */
    public function setReturn($return)
    {
        $this->return = $return;

        return $this;
    }

    /**
     * Get return
     *
     * @return \DateTime
     */
    public function getReturn()
    {
        return $this->return;
    }


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Bookings
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="c_title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Bookings
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="c_name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Bookings
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="c_phone", type="string", length=255, nullable=false)
     */
    private $phone;

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Bookings
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="c_email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Bookings
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="c_company", type="string", length=255, nullable=false)
     */
    private $company;

    /**
     * Set company
     *
     * @param string $company
     *
     * @return Bookings
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="c_address", type="string", length=255, nullable=false)
     */
    private $address;

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Bookings
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="c_city", type="string", length=255, nullable=false)
     */
    private $city;

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Bookings
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="c_state", type="string", length=255, nullable=false)
     */
    private $state;

    /**
     * Set state
     *
     * @param string $state
     *
     * @return Bookings
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="c_zip", type="string", length=255, nullable=false)
     */
    private $zip;

    /**
     * Set zip
     *
     * @param string $zip
     *
     * @return Bookings
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="c_country", type="string", length=255, nullable=false)
     */
    private $country;

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Bookings
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="string", length=255, nullable=false)
     */
    private $notes;

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return Bookings
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="flight_information", type="string", length=255, nullable=false)
     */
    private $flightInformation;

    /**
     * Set flightInformation
     *
     * @param string $flightInformation
     *
     * @return Bookings
     */
    public function setFlightInformation($flightInformation)
    {
        $this->flightInformation = $flightInformation;

        return $this;
    }

    /**
     * Get flightInformation
     *
     * @return string
     */
    public function getFlightInformation()
    {
        return $this->flightInformation;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="dropoff_information", type="string", length=255, nullable=false)
     */
    private $dropoffInformation;

    /**
     * Set dropoffInformation
     *
     * @param string $dropoffInformation
     *
     * @return Bookings
     */
    public function setDropoffInformation($dropoffInformation)
    {
        $this->dropoffInformation = $dropoffInformation;

        return $this;
    }

    /**
     * Get dropoffInformation
     *
     * @return string
     */
    public function getDropoffInformation()
    {
        return $this->dropoffInformation;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="BusinessBundle\Entity\LocationsTrans", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pickup_location_id_fk", referencedColumnName="id")
     * })
     */
    private $pickupId;

    /**
     * Set pickupId
     *
     * @param \BusinessBundle\Entity\Locations $pickupId
     * @return Users
     */
    public function setPickupId($pickupId)
    {
        $this->pickupId = $pickupId;

        return $this;
    }

    /**
     * Get pickupId
     * @return \BusinessBundle\Entity\LocationsTrans
     */
    public function getPickupId()
    {
        return $this->pickupId;
    }

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="BusinessBundle\Entity\LocationsTrans", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="return_location_id_fk", referencedColumnName="id")
     * })
     */
    private $returnId;

    /**
     * Set returnId
     *
     * @param \BusinessBundle\Entity\LocationsTrans $returnId
     * @return Users
     */
    public function setReturnId($returnId)
    {
        $this->returnId = $returnId;

        return $this;
    }

    /**
     * Get returnId
     * @return \BusinessBundle\Entity\Locations
     */
    public function getReturnId()
    {
        return $this->returnId;
    }

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="BusinessBundle\Entity\Car", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="car_id_fk", referencedColumnName="id")
     * })
     */
    private $car;

    /**
     * Set car
     *
     * @param \BusinessBundle\Entity\Car $car
     * @return Users
     */
    public function setCar($car)
    {
        $this->car = $car;

        return $this;
    }

    /**
     * Get car
     * @return \BusinessBundle\Entity\Car
     */
    public function getCar()
    {
        return $this->car;
    }
}
