<?php
/**
 * Created by PhpStorm.
 * User: Borislav
 * Date: 5.10.2016.
 * Time: 13:14
 */

namespace BusinessBundle\Entity;


class SearchFilter
{

    private $field;
    private $operation;
    private $value;


    public function setField($field)
    {
        $this->field = $field;
        return $this;
    }
    public function getField()
    {
        return $this->field;
    }

    public function setOperation($operation)
    {
        $this->operation = $operation;
        return $this;
    }
    public function getOperation()
    {
        return $this->operation;
    }

    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
    public function getValue()
    {
        return $this->value;
    }
}