<?php

namespace BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LocationRentPrices
 *
 * @ORM\Table(name="sp_rent_location_prices")
 * @ORM\Entity
 */
class LocationRentPrices
{

    public function __construct()
    {
    }

    /**
     * @var string
     *
     * @ORM\Column(name="shw", type="string", length=255, nullable=false)
     */
    private $show;

    /**
     * Set show
     *
     * @param string $show
     *
     * @return CarGroup
     */
    public function setShow($show)
    {
        $this->show = $show;

        return $this;
    }

    /**
     * Get show
     *
     * @return string
     */
    public function getShow()
    {
        return $this->show;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", nullable=false)
     */
    private $price;

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Toner
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="BusinessBundle\Entity\Locations", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="location_from_id_fk", referencedColumnName="id")
     * })
     */
    private $locationFrom;

    /**
     * Set locationFrom
     *
     * @param \BusinessBundle\Entity\Locations $locationFrom
     * @return Users
     */
    public function setLocationFrom($locationFrom)
    {
        $this->locationFrom = $locationFrom;

        return $this;
    }

    /**
     * Get from
     * @return \BusinessBundle\Entity\Locations
     */
    public function getLocationFrom()
    {
        return $this->locationFrom;
    }

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="BusinessBundle\Entity\Locations", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="location_to_id_fk", referencedColumnName="id")
     * })
     */
    private $locationTo;

    /**
     * Set to
     *
     * @param \BusinessBundle\Entity\Locations $locationTo
     * @return Users
     */
    public function setLocationTo($locationTo)
    {
        $this->locationTo = $locationTo;

        return $this;
    }

    /**
     * Get to
     * @return \BusinessBundle\Entity\Locations
     */
    public function getLocationTo()
    {
        return $this->locationTo;
    }
}
