<?php

namespace BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Bookings
 *
 * @ORM\Table(name="transfer_reservation_fleets")
 * @ORM\Entity
 */
class Fleet
{

    public function __construct()
    {
    }

    /**
     * @var string
     *
     * @ORM\Column(name="passangers", type="string", length=255, nullable=false)
     */
    private $passangers;

    /**
     * Set passangers
     *
     * @param string $passangers
     *
     * @return Cars
     */
    public function setPassangers($passangers)
    {
        $this->passangers = $passangers;

        return $this;
    }

    /**
     * Get passangers
     *
     * @return string
     */
    public function getPassangers()
    {
        return $this->passangers;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="luggage", type="string", length=255, nullable=false)
     */
    private $luggage;

    /**
     * Set luggage
     *
     * @param string $luggage
     *
     * @return Cars
     */
    public function setLuggage($luggage)
    {
        $this->luggage = $luggage;

        return $this;
    }

    /**
     * Get luggage
     *
     * @return string
     */
    public function getLuggage()
    {
        return $this->luggage;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
