<?php

namespace BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Bookings
 *
 * @ORM\Table(name="sp_rent_aditional_prices")
 * @ORM\Entity
 */
class Discounts
{

    public function __construct()
    {
    }

    /**
     * @var string
     *
     * @ORM\Column(name="percent", type="string", length=255, nullable=false)
     */
    private $percent;

    /**
     * Set percent
     *
     * @param string $percent
     *
     * @return Bookings
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get percent
     *
     * @return string
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="from", type="datetime", nullable=false)
     */
    private $from;

    /**
     * Set from
     *
     * @param \DateTime $from
     * @return Bookings
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return \DateTime
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="to", type="datetime", nullable=false)
     */
    private $to;

    /**
     * Set to
     *
     * @param \DateTime $to
     * @return Bookings
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get to
     *
     * @return \DateTime
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="BusinessBundle\Entity\CarGroup", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="car_group_id_fk", referencedColumnName="id")
     * })
     */
    private $car;

    /**
     * Set car
     *
     * @param \BusinessBundle\Entity\CarGroup $car
     * @return Users
     */
    public function setCar($car)
    {
        $this->car = $car;

        return $this;
    }

    /**
     * Get car
     * @return \BusinessBundle\Entity\CarGroup
     */
    public function getCar()
    {
        return $this->car;
    }
}
