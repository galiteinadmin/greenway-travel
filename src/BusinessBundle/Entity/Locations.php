<?php

namespace BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Bookings
 *
 * @ORM\Table(name="sp_rent_locations")
 * @ORM\Entity
 */
class Locations
{

    public function __construct()
    {
    }

    /**
     * @var string
     *
     * @ORM\Column(name="show", type="string", length=255, nullable=false)
     */
    private $show;

    /**
     * Set show
     *
     * @param string $show
     *
     * @return Locations
     */
    public function setShow($show)
    {
        $this->show = $show;

        return $this;
    }

    /**
     * Get show
     *
     * @return string
     */
    public function getShow()
    {
        return $this->show;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Locations
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_information", type="integer", length=1, nullable=false)
     */
    private $deliveryInformation;

    /**
     * Set deliveryInformation
     *
     * @param string $deliveryInformation
     *
     * @return CarGroup
     */
    public function setDeliveryInformation($deliveryInformation)
    {
        $this->deliveryInformation = $deliveryInformation;

        return $this;
    }

    /**
     * Get deliveryInformation
     *
     * @return string
     */
    public function getDeliveryInformation()
    {
        return $this->deliveryInformation;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
