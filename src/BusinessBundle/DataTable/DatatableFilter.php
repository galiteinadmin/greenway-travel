<?php
/**
 * Created by PhpStorm.
 * User: Borislav
 * Date: 25.10.2016.
 * Time: 12:28
 */

namespace BusinessBundle\DataTable;


class DataTableFilter
{

    private $field;
    private $type;
    private $value;


    public function setField($field)
    {
        $this->field = $field;
    }

    public function getField()
    {
        return $this->field ;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type ;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value ;
    }
}