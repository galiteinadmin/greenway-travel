<?php

namespace BusinessBundle\DataTable;

use BusinessBundle\Entity\SearchFilter;
use CoreBusinessBundle\Helpers\StringHelper;
use Sensio\Bundle\FrameworkExtraBundle;


/**
 * Created by PhpStorm.
 * User: Borislav
 * Date: 14.10.2016.
 * Time: 14:50
 */
class DataTablePager
{
    private $start;
    private $lenght;
    private $draw;
    private $columnOrder;
    private $sortOrder;
    private $search;
    private $filters;

    public function getStart()
    {
        return $this->start;
    }

    public function setStart($start)
    {
        $this->start = $start;
    }

    public function getDraw()
    {
        return $this->draw;
    }

    public function setDraw($draw)
    {
        $this->draw = $draw;
    }

    public function getLenght()
    {
        return $this->lenght;
    }

    public function setLenght($lenght)
    {
        $this->lenght = $lenght;
    }

    public function getColumnOrder()
    {
        return $this->columnOrder;
    }

    public function setColumnOrder($columnOrder)
    {
        $this->columnOrder = $columnOrder;
    }

    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    }

    public function getSearch()
    {
        return $this->search;
    }

    public function setSearch($search)
    {
        $this->search = $search;
    }

    public function getFilters()
    {
        return $this->filters;
    }

    public function setFilters($filters)
    {
        $this->filters = $filters;
    }

    public function setFromPost($post)
    {
        $sort_column_id = 0;
        if (isset($post->order->value[0]->column)) {
            $sort_column_id = $post->order->value[0]->column;
        }

        $this->setColumnOrder($post->columns->value["{$sort_column_id}"]->data);

        $this->setSortOrder("asc");
        if (isset($post->order->value[0]->dir)) {
            $this->setSortOrder($post->order->value[0]->dir);
        }

        $this->setSearch($post->search->value->value);
        $this->setStart($post->start->value);
        $this->setLenght($post->length->value);
        $this->setDraw($post->draw->value);

        $filters = Array();
        foreach ($post->columns->value as $field) {
            if (!empty($field->search->value) || $field->search->value === "0") {
                $filter=new SearchFilter();
                $filter->setField($field->data);
                $filter->setOperation($field->search_type);

                if($field->search_type == "in" || $field->search_type == "ni"){
                    $field->search->value = explode("|",$field->search->value);
                    $field->search->value = implode("','",$field->search->value);
                    $field->search->value = "'" . $field->search->value . "'";
                }
                $filter->setValue($field->search->value);

                $filters[]=$filter;
            }
        }

        $this->setFilters($filters);

    }

    public function getOrderBy(){
        if ($this->sortOrder == "asc")
            $orderBy = 'ORDER BY ISNULL(' . $this->columnOrder . '),' . $this->columnOrder . ' ' . $this->sortOrder;
        else
            $orderBy = 'ORDER BY ' . $this->columnOrder . ' ' . $this->sortOrder;

        return $orderBy;
    }

    public function getLimit(){
        return '  LIMIT ' . $this->start . ',' . $this->lenght;
    }
}