<?php
/**
 * Created by PhpStorm.
 * User: Borislav
 * Date: 21.6.2016.
 * Time: 15:17
 */

namespace BusinessBundle\Helpers;


class StringConverter
{
    public static function GetPriceFromString($input)
    {
        $price = null;

        $input = trim($input);
        if (preg_match('/^[0-9.,]+$/', $input) && !empty($input)) {
            $input = str_replace(",", "", $input);
            $price = number_format(str_replace(",", ".", $input), 4, '.', '');
        }
        return $price;
    }
}