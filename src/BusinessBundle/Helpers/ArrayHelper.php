<?php
/**
 * Created by PhpStorm.
 * User: Borislav
 * Date: 3.7.2016.
 * Time: 16:49
 */

namespace BusinessBundle\Helpers;


class ArrayHelper
{
    public static function findObjectByProperty($array, $index, $value)
    {
        foreach ($array as $arrayInf) {
            if ($arrayInf->{$index} == $value) {
                return $arrayInf;
            }
        }
        return null;
    }

    public static function findByProperty($array, $index, $value)
    {

        if (count($array) > 0) {
            foreach ($array as $arrayInf) {
                if ($arrayInf[$index] == $value) {
                    return $arrayInf;
                }
            }
        }
        return null;
    }
}
