<?php
/**
 * Created by PhpStorm.
 * User: Borislav
 * Date: 5.10.2016.
 * Time: 13:20
 */

namespace BusinessBundle\Helpers;


use BusinessBundle\Entity\SearchFilter;

class SearchFilterHelper
{
    /**@var SearchFilter $rule */
    public static function mapOperations($rule, $prefix)
    {
        $fieldPrefix = $prefix;
        $fieldName = $rule->getField();
        $fieldData = $rule->getValue();

        switch ($rule->getOperation()) {
            case "eq":
                $fieldOperation = $fieldPrefix . $fieldName . " = '" . $fieldData . "'";
                break;
            case "ne":
                $fieldOperation = $fieldPrefix . $fieldName . " != '" . $fieldData . "'";
                break;
            case "lt":
                $fieldOperation = $fieldPrefix . $fieldName . " < '" . $fieldData . "'";
                break;
            case "gt":
                $fieldOperation = $fieldPrefix . $fieldName . " > '" . $fieldData . "'";
                break;
            case "le":
                $fieldOperation = $fieldPrefix . $fieldName . " <= '" . $fieldData . "'";
                break;
            case "ge":
                $fieldOperation = $fieldPrefix . $fieldName . " >= '" . $fieldData . "'";
                break;
            case "nu":
                $fieldOperation = $fieldPrefix . $fieldName . " = ''";
                break;
            case "nn":
                $fieldOperation = $fieldPrefix . $fieldName . " != ''";
                break;
            case "in":
                $fieldOperation = $fieldPrefix . $fieldName . " IN (" . $fieldData . ")";
                break;
            case "ni":
                $fieldOperation = $fieldPrefix . $fieldName . " NOT IN (" . $fieldData . ")";
                break;
            case "tr":
                $fieldOperation = $fieldPrefix . $fieldName . " = '1'";
                break;
            case "bw":
                $fieldOperation = $fieldPrefix . $fieldName . " LIKE '" . $fieldData . "%'";
                break;
            case "bn":
                $fieldOperation = $fieldPrefix . $fieldName . " NOT LIKE '" . $fieldData . "%'";
                break;
            case "ew":
                $fieldOperation = $fieldPrefix . $fieldName . " LIKE '%" . $fieldData . "'";
                break;
            case "en":
                $fieldOperation = $fieldPrefix . $fieldName . " NOT LIKE '%" . $fieldData . "'";
                break;
            case "cn":
                $fieldOperation = $fieldPrefix . $fieldName . " LIKE '%" . $fieldData . "%'";
                break;
            case "nc":
                $fieldOperation = $fieldPrefix . $fieldName . " NOT LIKE '%" . $fieldData . "%'";
                break;
            case "information eq":
                $fieldOperation = $fieldPrefix . $fieldName . " = '" . $fieldData . "'";
                break;
            default:
                $fieldOperation = "";
                break;
        }
        return $fieldOperation;
    }
}