<?php

/**
 * Created by PhpStorm.
 * User: Borislav
 * Date: 12.5.2016.
 * Time: 13:04
 */
namespace BusinessBundle\Interfaces\DAL;

use BusinessBundle\DataTable\DataTablePager;

interface IBaseDataAccess
{
    public function getAll();

    public function getById($id);

    public function save($item);

    public function validate($item);

    public function delete($item);

    public function getItemsWithPaging(DataTablePager $pager);

    public function countAllItems();
}