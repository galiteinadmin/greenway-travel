<?php
/**
 * Created by PhpStorm.
 * User: Borislav
 * Date: 11.5.2016.
 * Time: 10:56
 */

namespace BusinessBundle\Interfaces\BLL;


use BusinessBundle\DataTable\DataTablePager;

interface IContext
{
    public function getAll();

    public function getById($id);

    public function save($item);

    public function validate($item);

    public function delete($item);

    public function getItemsWithPaging(DataTablePager $pager);

    public function countAllItems();
}