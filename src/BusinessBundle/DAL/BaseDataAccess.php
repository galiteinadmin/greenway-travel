<?php

/**
 * Created by PhpStorm.
 * User: Borislav
 * Date: 12.5.2016.
 * Time: 13:00
 */
namespace BusinessBundle\DAL;

use BusinessBundle\DataTable\DataTablePager;
use BusinessBundle\Helpers\SearchFilterHelper;
use BusinessBundle\Interfaces\DAL\IBaseDataAccess;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Validator\RecursiveValidator;

class BaseDataAccess implements IBaseDataAccess
{
    protected $repository;
    protected $entityManager;
    protected $validator;


    public function setEntityRepository(EntityRepository $repository)
    {
        $this->repository = $repository;
    }

    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function setValidator(RecursiveValidator $validator)
    {
        $this->validator = $validator;


    }

    public function getAll($filter = Array(),$order = Array())
    {
        $items = null;

        if(!empty($filter) || !empty($order)){
            $items = $this->repository->findBy($filter,$order);
        }
        else{
            $items = $this->repository->findAll();
        }

        return $items;
    }

    public function getById($id)
    {
        $item = $this->repository->findOneById($id);
        return $item;
    }

    public function save($item)
    {
        $this->entityManager->persist($item);
        $this->entityManager->flush();
    }

    public function validate($item)
    {
        return $this->validator->validate($item);
    }

    public function delete($item)
    {
        $this->entityManager->remove($item);
        $this->entityManager->flush();
    }

    public function getItemsWithPaging(DataTablePager $pager)
    {
        //dump('xfgvffg');
        $queryBuilder = $this->repository->createQueryBuilder('i');
        $queryBuilder->setFirstResult($pager->getStart());
        $queryBuilder->setMaxResults($pager->getLenght());
        $queryBuilder->orderBy('i.' . $pager->getColumnOrder(), $pager->getSortOrder());

        if ($pager->getFilters()) {
            $conditions = array();

            /**@var SearchFilter $filter */
            foreach ($pager->getFilters() as $filter) {
                if (strpos($filter->getField(), ".") !== false) {
                    $tableName = explode(".", $filter->getField())[0];
                    $queryBuilder->join('i.' . $tableName, $tableName);
                    array_push($conditions, SearchFilterHelper::mapOperations($filter,""));
                }else
                {
                    array_push($conditions, SearchFilterHelper::mapOperations($filter,"i."));
                }

                $orX = $queryBuilder->expr()->andX();
                foreach ($conditions as $condition) {
                    $orX->add($condition);
                }

                $queryBuilder->add('where', $orX);
            }
        }

        $query = $queryBuilder->getQuery();

        $items = $query->getResult();

        return $items;
    }

    public function countAllItems()
    {
        $query = $this->repository->createQueryBuilder('i')
            ->select('count(i.id)')
            ->getQuery();

        $count = $query->getSingleScalarResult();
       // dump($count);die;
        return $count;
    }

    public function mapPagerToQueryBuilder(\Doctrine\ORM\QueryBuilder $queryBuilder, DataTablePager $pager)
    {
        $queryBuilder->setFirstResult($pager->getStart());
        $queryBuilder->setMaxResults($pager->getLenght());

        $joins = array();


        if (strpos($pager->getColumnOrder(), ".") !== false) {
            $tableName = explode(".", $pager->getColumnOrder())[0];
            if (!in_array($tableName, $joins))
                $joins[] = $tableName;

            $queryBuilder->orderBy($pager->getColumnOrder(), $pager->getSortOrder());
        } else {
            $queryBuilder->orderBy('i.' . $pager->getColumnOrder(), $pager->getSortOrder());
        }

        if ($pager->getFilters()) {
            $conditions = array();

            /**@var SearchFilter $filter */
            foreach ($pager->getFilters() as $filter) {
                if (strpos($filter->getField(), ".") !== false) {
                    $tableName = explode(".", $filter->getField())[0];
                    if (!in_array($tableName, $joins))
                        $joins[] = $tableName;
                    array_push($conditions, SearchFilterHelper::mapOperations($filter, ""));
                } else {
                    array_push($conditions, SearchFilterHelper::mapOperations($filter, "i."));
                }

                $orX = $queryBuilder->expr()->andX();
                foreach ($conditions as $condition) {
                    $orX->add($condition);
                }

                $queryBuilder->add('where', $orX);
            }
        }
        foreach ($joins as $join) {
            $queryBuilder->join('i.' . $join, $join);
        }

        return $queryBuilder;
    }

}