<?php
/**
 * Created by PhpStorm.
 * User: Borislav
 * Date: 17.6.2016.
 * Time: 10:25
 */

namespace BusinessBundle\DAL;

use BusinessBundle\DataTable\DataTablePager;
use BusinessBundle\Entity\Transfers;
use BusinessBundle\Helpers\SearchFilterHelper;
use BusinessBundle\Helpers\StringHelper;
use Doctrine\ORM\NoResultException;

class TransfersDAL extends BaseDataAccess
{
    public function getItemsWithPagingNew(DataTablePager $pager)
    {
        $queryBuilder = $this->repository->createQueryBuilder('i');

        $conditions = array();

        $now = date("Y-m-d",time());

        array_push($conditions, StringHelper::Format("i.from > '{0}'", $now));

        $queryBuilder = $this->mapPagerToQueryBuilder($queryBuilder, $pager);

        $query = $queryBuilder->getQuery();

        $items = $query->getResult();
        return $items;
    }

    public function countAllItemsNew()
    {

        $queryBuilder = $this->repository->createQueryBuilder('i')->select('count(i.id)');

        $conditions = array();

        $now = date("Y-m-d",time());
        array_push($conditions, StringHelper::Format("i.from > '{0}'", $now));

        if (!empty($conditions)) {
            $orX = $queryBuilder->expr()->andX();
            foreach ($conditions as $condition) {
                $orX->add($condition);
            }

            $queryBuilder->andWhere($orX);
        }


        $query = $queryBuilder->getQuery();

        $count = $query->getSingleScalarResult();
        return $count;
    }
}