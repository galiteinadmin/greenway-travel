<?php

namespace BusinessBundle\Type;

class PkDateTime extends \DateTime
{
    public function __toString()
    {
        return $this->format('U');
    }
}