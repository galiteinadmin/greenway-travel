<?php
/**
 * Created by PhpStorm.
 * User: Borislav
 * Date: 10.4.2016.
 * Time: 0:21
 */

namespace BusinessBundle\Services;
use BusinessBundle\DataTable\DataTablePager;
use BusinessBundle\Interfaces\BLL\IContext;
use BusinessBundle\Interfaces\DAL\IBaseDataAccess;

class BaseContext implements IContext
{
    protected $dataAccess;

    public function __construct(IBaseDataAccess $dataAccess)
    {
        $this->dataAccess = $dataAccess;
    }

    public function getAll($filter = Array(),$order = Array())
    {
        return $this->dataAccess->getAll($filter,$order);
    }

    public function getById($id)
    {

        return $this->dataAccess->getById($id);
    }

    public function save($item)
    {
        $this->dataAccess->save($item);
    }

    public function validate($item)
    {
        return $this->dataAccess->validate($item);
    }

    public function delete($item)
    {
        $this->dataAccess->delete($item);
    }

    public function getItemsWithPaging(DataTablePager $pager)
    {
        return $this->dataAccess->getItemsWithPaging($pager);
    }

    public function countAllItems()
    {
        return $this->dataAccess->countAllItems();
    }


}