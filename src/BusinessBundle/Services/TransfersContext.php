<?php
/**
 * Created by PhpStorm.
 * User: Borislav
 * Date: 17.6.2016.
 * Time: 10:25
 */

namespace BusinessBundle\Services;


use BusinessBundle\DataTable\DataTablePager;

class TransfersContext extends BaseContext
{
    public function getItemsWithPagingNew(DataTablePager $pager)
    {
        return $this->dataAccess->getItemsWithPagingNew($pager);
    }

    public function countAllItemsNew()
    {
        return $this->dataAccess->countAllItemsNew();
    }
}